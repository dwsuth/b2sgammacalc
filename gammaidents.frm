
*id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,p4?)*g_(1,p2?)*g_(1,mu?)=DAVE1*g_(1,mu,p2,p3,p4,p2,mu);
*id g_(1,mu?)*g_(1,p3?)*g_(1,p2?)*g_(1,p4?)*g_(1,p2?)*g_(1,mu?)=DAVE2*g_(1,mu,p3,p2,p4,p2,mu);
*id g_(1,mu?)*g_(1,p3?)*g_(1,p4?)*g_(1,p2?)*g_(1,p2?)*g_(1,mu?)=DAVE3*g_(1,mu,p3,p4,p2,p2,mu);
*id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,p2?)*g_(1,p4?)*g_(1,mu?)=DAVE4*g_(1,mu,p2,p3,p2,p4,mu);
*id g_(1,mu?)*g_(1,p2?)*g_(1,p2?)*g_(1,p3?)*g_(1,p4?)*g_(1,mu?)=DAVE5*g_(1,mu,p2,p2,p3,p4,mu);
*id g_(1,mu?)*g_(1,p3?)*g_(1,p2?)*g_(1,p2?)*g_(1,p4?)*g_(1,mu?)=DAVE6*g_(1,mu,p3,p2,p2,p4,mu);
**************
****identity: \gamma^\mu \gamma_\mu = D \times ident.
***************

repeat;
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu)*g_(1);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1)*g_(1);
endrepeat;


***step two: if two adjacent-but-one gamma matrices are contracted by the same momentum or same index, commute and then replace
repeat;
id g_(1,p1?)*g_(1,p2?)*g_(1,p1?)=(2*d_(p1,p2)-g_(1,p2,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,p2?)*g_(1,mu?)=(2*d_(mu,p2)-g_(1,p2,mu))*g_(1,mu);
id g_(1,mu?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,nu)-g_(1,nu,mu))*g_(1,mu);
id g_(1,p1?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,nu)-g_(1,nu,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu)*g_(1);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1)*g_(1);
endrepeat;

***step three: if two adjacent-but-two gamma matrices are contracted by the same momentum or same index, commute and replace, then apply adjacent-but-one rules
repeat;
id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,p3,p1)-2*d_(p1,p3)*g_(1,p2,p1)+g_(1,p2,p3,p1,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,p3?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,p3,p1)-2*d_(p1,p3)*g_(1,nu,p1)+g_(1,nu,p3,p1,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,nu,p1)-2*d_(p1,nu)*g_(1,p2,p1)+g_(1,p2,nu,p1,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,rho?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,rho,p1)-2*d_(p1,rho)*g_(1,nu,p1)+g_(1,nu,rho,p1,p1));
id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,p3,mu)-2*d_(mu,p3)*g_(1,p2,mu)+g_(1,p2,p3,mu,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,p3,mu)-2*d_(mu,p3)*g_(1,nu,mu)+g_(1,nu,p3,mu,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,nu,mu)-2*d_(mu,nu)*g_(1,p2,mu)+g_(1,p2,nu,mu,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,rho?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,rho,mu)-2*d_(mu,rho)*g_(1,nu,mu)+g_(1,nu,rho,mu,mu));
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu)*g_(1);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1)*g_(1);
id g_(1,p1?)*g_(1,p2?)*g_(1,p1?)=(2*d_(p1,p2)-g_(1,p2)*g_(1,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,p2?)*g_(1,mu?)=(2*d_(mu,p2)-g_(1,p2)*g_(1,mu))*g_(1,mu);
id g_(1,mu?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,nu)-g_(1,nu)*g_(1,mu))*g_(1,mu);
id g_(1,p1?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,nu)-g_(1,nu)*g_(1,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu)*g_(1);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1)*g_(1);
endrepeat;


*id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,p4?)*g_(1,p1?)*g_(1,p2?)=DAVEDAVEDAVE*g_(1,p1,p2,p3,p4,p1,p2);
*id g_(1,p1?)*g_(1,mu?)*g_(1,p3?)*g_(1,p4?)*g_(1,p1?)*g_(1,mu?)=DAVEDAVEDAVE*g_(1,p1,mu,p3,p4,p1,mu);
*id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,p4?)*g_(1,mu?)*g_(1,p2?)=DAVEDAVEDAVE*g_(1,mu,p2,p3,p4,mu,p2);
*id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,p4?)*g_(1,mu?)*g_(1,nu?)=DAVEDAVEDAVE*g_(1,mu,nu,p3,p4,mu,nu);
***step four: adjacent-but-three rules
***TODO
repeat;
id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,p4?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,p3,p4,p1)-g_(1,p2,p1,p3,p4,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,p3?)*g_(1,p4?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,p3,p4,p1)-g_(1,nu,p1,p3,p4,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,rho?)*g_(1,p4?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,rho,p4,p1)-g_(1,nu,p1,rho,p4,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,rho?)*g_(1,sigma?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,rho,sigma,p1)-g_(1,nu,p1,rho,sigma,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,rho?)*g_(1,sigma?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,rho,sigma,p1)-g_(1,p2,p1,rho,sigma,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,p3?)*g_(1,sigma?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,p3,sigma,p1)-g_(1,nu,p1,p3,sigma,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,sigma?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,p3,sigma,p1)-g_(1,p2,p1,p3,sigma,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,rho?)*g_(1,p4?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,rho,p4,p1)-g_(1,p2,p1,rho,p4,p1));

id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,p4?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,p3,p4,mu)-g_(1,p2,mu,p3,p4,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,p4?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,p3,p4,mu)-g_(1,nu,mu,p3,p4,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,rho?)*g_(1,p4?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,rho,p4,mu)-g_(1,nu,mu,rho,p4,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,rho?)*g_(1,sigma?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,rho,sigma,mu)-g_(1,nu,mu,rho,sigma,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,rho?)*g_(1,sigma?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,rho,sigma,mu)-g_(1,p2,mu,rho,sigma,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,sigma?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,p3,sigma,mu)-g_(1,nu,mu,p3,sigma,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,sigma?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,p3,sigma,mu)-g_(1,p2,mu,p3,sigma,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,rho?)*g_(1,p4?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,rho,p4,mu)-g_(1,p2,mu,rho,p4,mu));

id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,p3,p1)-2*d_(p1,p3)*g_(1,p2,p1)+g_(1,p2,p3,p1,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,p3?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,p3,p1)-2*d_(p1,p3)*g_(1,nu,p1)+g_(1,nu,p3,p1,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,nu,p1)-2*d_(p1,nu)*g_(1,p2,p1)+g_(1,p2,nu,p1,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,rho?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,rho,p1)-2*d_(p1,rho)*g_(1,nu,p1)+g_(1,nu,rho,p1,p1));
id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,p3,mu)-2*d_(mu,p3)*g_(1,p2,mu)+g_(1,p2,p3,mu,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,p3,mu)-2*d_(mu,p3)*g_(1,nu,mu)+g_(1,nu,p3,mu,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,nu,mu)-2*d_(mu,nu)*g_(1,p2,mu)+g_(1,p2,nu,mu,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,rho?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,rho,mu)-2*d_(mu,rho)*g_(1,nu,mu)+g_(1,nu,rho,mu,mu));
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu)*g_(1);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1)*g_(1);
id g_(1,p1?)*g_(1,p2?)*g_(1,p1?)=(2*d_(p1,p2)-g_(1,p2)*g_(1,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,p2?)*g_(1,mu?)=(2*d_(mu,p2)-g_(1,p2)*g_(1,mu))*g_(1,mu);
id g_(1,mu?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,nu)-g_(1,nu)*g_(1,mu))*g_(1,mu);
id g_(1,p1?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,nu)-g_(1,nu)*g_(1,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu)*g_(1);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1)*g_(1);
endrepeat;

