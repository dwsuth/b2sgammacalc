*id DAVEdeltag1overg1=-COUPcosW/COUPsinW*DAVEVC;
*id DAVEdeltasW2=-COUPsinW*COUPcosW*DAVEVC;
*id DAVEVC=d6dum*shiftdum*COUPv^2*COUPcHWB;

id VtbarbW(p1?,p2?,p3?,mu?) = -i_ * COUPVtb * COUPg * g_(1,mu,7_)/2 * sqrt_(2)/2
- i_ *  COUPVtb * d6dum*COUPcphiq3 * COUPg * sqrt_(2)/2 * COUPv^2 * g_(1,mu,7_)/2
*+ i_ *d6dum *  shiftdum * COUPVtb * COUPg * deltaGF/2 * g_(1,mu,7_)/2
-i_*d6dum*(g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2)*p3(gamma4)*COUPVtb*COUPcuW*sqrt_(2)*mt
-i_*d6dum*(g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2)*p3(gamma4)*COUPVtb*COUPcdW*sqrt_(2)*mb
-i_*d6dum*COUPg*sqrt_(2)/2*COUPcud*g_(1,mu,6_)/2*COUPVtb*mt*mb
+ newshiftdum * d6dum * -i_ * COUPVtb * sqrt_(2)/2*deltag2new * g_(1,mu,7_)/2
;
id VubarbW(p1?,p2?,p3?,mu?) = -i_ * COUPVtb * COUPg * g_(1,mu,7_)/2 * sqrt_(2)/2
- i_ *  COUPVtb * d6dum*COUPcphiq3 * COUPg * sqrt_(2)/2 * COUPv^2 * g_(1,mu,7_)/2
*+ i_ *d6dum *  shiftdum * COUPVtb * COUPg * deltaGF/2 * g_(1,mu,7_)/2
*-i_*d6dum*(g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2)*p3(gamma4)*COUPVtb*COUPcuW*sqrt_(2)*mt
-i_*d6dum*(g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2)*p3(gamma4)*COUPVtb*COUPcdW*sqrt_(2)*mb
*-i_*d6dum*COUPg*sqrt_(2)/2*COUPcud*g_(1,mu,6_)/2*COUPVtb*mt*mb
+ newshiftdum * d6dum * -i_ * COUPVtb * sqrt_(2)/2*deltag2new * g_(1,mu,7_)/2
;
id VtbarbW2(p1?,p2?,p3?,mu?) = -i_ * COUPVtb * COUPg * g_(2,mu,7_)/2 * sqrt_(2)/2
- i_ *  COUPVtb * d6dum*COUPcphiq3 * COUPg * sqrt_(2)/2 * COUPv^2 * g_(2,mu,7_)/2
*+ i_ *d6dum *  shiftdum * COUPVtb * COUPg * deltaGF/2 * g_(2,mu,7_)/2
+ newshiftdum * d6dum * -i_ * COUPVtb * sqrt_(2)/2*deltag2new * g_(2,mu,7_)/2
;
*****WARNING the following two items should be equal, but with g_(1, and g_(2, respectively**********
id VsbartW(p1?,p2?,p3?,mu?) = -i_ * COUPVts * COUPg * g_(1,mu,7_)/2 * sqrt_(2)/2
- i_ *  COUPVts * d6dum*COUPcphiq3 * COUPg * sqrt_(2)/2 * COUPv^2 * g_(1,mu,7_)/2
*+ i_ * d6dum * shiftdum * COUPVts * COUPg * deltaGF/2 * g_(1,mu,7_)/2
-i_*d6dum*(g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2)*p3(gamma4)*COUPVts*COUPcuW*sqrt_(2)*mt
*-i_*d6dum*(g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2)*p3(gamma4)*COUPVts*COUPcdW*sqrt_(2)*mb
- i_ * COUPVts * d6dum * deltag2new * sqrt_(2)/2*g_(1,mu,7_)/2 * newshiftdum
;
id VsbartW2(p1?,p2?,p3?,mu?) = -i_ * COUPVts * COUPg * g_(2,mu,7_)/2 * sqrt_(2)/2
- i_ *  COUPVts * d6dum*COUPcphiq3 * COUPg * sqrt_(2)/2 * COUPv^2 * g_(2,mu,7_)/2
*+ i_ * d6dum * shiftdum * COUPVts * COUPg * deltaGF/2 * g_(2,mu,7_)/2
-i_*d6dum*(g_(2,mu,gamma4,6_)/2-g_(2,gamma4,mu,6_)/2)*p3(gamma4)*COUPVts*COUPcuW*sqrt_(2)*mt
*-i_*d6dum*(g_(2,mu,gamma4,7_)/2-g_(2,gamma4,mu,7_)/2)*p3(gamma4)*COUPVts*COUPcdW*sqrt_(2)*mb
- i_ * COUPVts * d6dum * deltag2new * sqrt_(2)/2*g_(2,mu,7_)/2 * newshiftdum
;
id VsbaruW(p1?,p2?,p3?,mu?) = -i_ * COUPVts * COUPg * g_(1,mu,7_)/2 * sqrt_(2)/2
- i_ *  COUPVts * d6dum*COUPcphiq3 * COUPg * sqrt_(2)/2 * COUPv^2 * g_(1,mu,7_)/2
*+ i_ * d6dum * shiftdum * COUPVts * COUPg * deltaGF/2 * g_(1,mu,7_)/2
*-i_*d6dum*(g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2)*p3(gamma4)*COUPVts*COUPcdW*sqrt_(2)*mb
- i_ * COUPVts * d6dum * deltag2new * sqrt_(2)/2*g_(1,mu,7_)/2 * newshiftdum
;

id VtbarbWgam(p1?,p2?,p3?,mu?,nu?) =
+i_*d6dum*COUPe*(g_(1,mu,nu,7_)/2-g_(1,nu,mu,7_)/2)*COUPVtb*COUPcuW*sqrt_(2)*mt
+i_*d6dum*COUPe*(g_(1,mu,nu,6_)/2-g_(1,nu,mu,6_)/2)*COUPVtb*COUPcdW*sqrt_(2)*mb
;

id VsbartWgam(p1?,p2?,p3?,mu?,nu?) =
+i_*d6dum*COUPe*(g_(1,mu,nu,6_)/2-g_(1,nu,mu,6_)/2)*COUPVts*COUPcuW*sqrt_(2)*mt
*-i_*d6dum*COUPe*(g_(1,mu,nu,7_)/2-g_(1,nu,mu,7_)/2)*COUPVts*COUPcdW*sqrt_(2)*mb
;
id VtbarbWZ(p1?,p2?,p3?,p4?,mu?,nu?) =i_*d6dum*COUPg*COUPcosW*(g_(1,mu,nu,7_)/2-g_(1,nu,mu,7_)/2)*COUPVtb*COUPcuW*sqrt_(2)*mt
-i_*d6dum*COUPg*COUPcosW*(g_(1,mu,nu,6_)/2-g_(1,nu,mu,6_)/2)*COUPVtb*COUPcdW*sqrt_(2)*mb
;

id VsbartWZ(p1?,p2?,p3?,p4?,mu?,nu?) =-i_*d6dum*COUPg*COUPcosW*(g_(1,mu,nu,6_)/2-g_(1,nu,mu,6_)/2)*COUPVts*COUPcuW*sqrt_(2)*mt
;
if ( count(d6dum,1) > 1 ) discard;
.sort
id Vwpwmgam(p1?,p2?,p3?,alpha?,beta?,gamma?) = i_ * COUPe * ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)))
*+i_ *  shiftdum * COUPe/2 *(deltag1gamma+deltakappagamma)* ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)))
-i_ * 6*  COUPsinW * d6dum*COUPcW * (p2(gamma)*p3(alpha)*p1(beta)-p1(gamma)*p2(alpha)*p3(beta)+d_(gamma, alpha)*(p3(beta)*p1(gamma4)*p2(gamma4)-p1(beta)*p2(gamma4)*p3(gamma4))+d_(alpha,beta)*(p1(gamma)*p2(gamma4)*p3(gamma4)-p2(gamma)*p3(gamma4)*p1(gamma4))+d_(gamma,beta)*(p2(alpha)*p3(gamma4)*p1(gamma4)-p3(alpha)*p1(gamma4)*p2(gamma4)))
*-i_ * COUPg * COUPcosW * COUPv^2 * d6dum*COUPcHWB * ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) ) + d_(gamma,alpha) * ( - p1(beta)))
+i_ * COUPg * COUPcosW * POLESVC * d6dum * (d_(gamma,alpha)*(COUPcosW^2*p3(beta)+COUPsinW^2*p1(beta))+d_(alpha,beta)*(COUPsinW^2*p2(gamma)-COUPsinW^2*p1(gamma))+d_(beta,gamma)*(-COUPsinW^2*p2(alpha)-COUPcosW^2*p3(alpha)))
+ i_ * deltaenew * newshiftdum * d6dum * ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)))
;

id Vtbartgam(p1?,p2?,p3?,mu?) = - i_ * 2/3* COUPe * g_(1,mu)
*-i_ * 2/3 * COUPe *  shiftdum * deltaalphaover2alpha * g_(1,mu)
+i_ * 2/3 * COUPe * COUPsinW * COUPcosW * POLESVC * d6dum * g_(1,mu)
*+i_*d6dum*COUPv/sqrt_(2)*(g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2)*p3(gamma4)*(-COUPcuW*COUPsinW-COUPcosW*COUPcuB)
*+i_*d6dum*COUPv/sqrt_(2)*(g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2)*p3(gamma4)*(-COUPcuW*COUPcosW-COUPcosW*COUPcuB)
+i_*d6dum*(g_(1,7_,mu,gamma4)/2-g_(1,7_,gamma4,mu)/2)*p3(gamma4)*(-COUPcuW*COUPsinW-COUPcosW*COUPcuB)*mt
+i_*d6dum*(g_(1,6_,mu,gamma4)/2-g_(1,6_,gamma4,mu)/2)*p3(gamma4)*(-COUPcuW*COUPsinW-COUPcosW*COUPcuB)*mt
-i_ * newshiftdum * d6dum * 2/3 * deltaenew * g_(1,mu)
;

id Vtbartglue(p1?,p2?,p3?,mu?) = - i_ * COUPe * g_(1,mu)
+i_*d6dum*(g_(1,7_,mu,gamma4)/2-g_(1,7_,gamma4,mu)/2)*p3(gamma4)*(-COUPcuG*COUPe)*mt
+i_*d6dum*(g_(1,6_,mu,gamma4)/2-g_(1,6_,gamma4,mu)/2)*p3(gamma4)*(-COUPcuG*COUPe)*mt
;

id Vbbarbglue(p1?,p2?,p3?,mu?) = - i_ * COUPe * g_(1,mu)
+i_*d6dum*(g_(1,7_,mu,gamma4)/2-g_(1,7_,gamma4,mu)/2)*p3(gamma4)*(-COUPcdG*COUPe)*mb
+i_*d6dum*(g_(1,6_,mu,gamma4)/2-g_(1,6_,gamma4,mu)/2)*p3(gamma4)*(-COUPcdG*COUPe)*mb
;
id Vsbarsgam(p1?,p2?,p3?,mu?) = i_ * 1/3* COUPe * g_(1,mu)
*+i_ * 1/3 * COUPe *  shiftdum * deltaalphaover2alpha * g_(1,mu)
- i_ * 1/3 * COUPe * COUPsinW * COUPcosW * POLESVC * d6dum * g_(1,mu)
+i_ * newshiftdum * d6dum * 1/3 * deltaenew * g_(1,mu)
;
id Vbbarbgam(p1?,p2?,p3?,mu?) = i_ * 1/3* COUPe * g_(1,mu)
*+i_ * 1/3 * COUPe *  shiftdum * deltaalphaover2alpha * g_(1,mu)
- i_ * 1/3 * COUPe * COUPsinW * COUPcosW * POLESVC * d6dum * g_(1,mu)
+i_*d6dum*(g_(1,7_,mu,gamma4)/2-g_(1,7_,gamma4,mu)/2)*p3(gamma4)*(COUPcdW*COUPsinW-COUPcosW*COUPcdB)*mb
+i_*d6dum*(g_(1,6_,mu,gamma4)/2-g_(1,6_,gamma4,mu)/2)*p3(gamma4)*(COUPcdW*COUPsinW-COUPcosW*COUPcdB)*mb
+i_ * newshiftdum * d6dum * 1/3 * deltaenew * g_(1,mu)
;
id Vlbarlgam(p1?,p2?,p3?,mu?) = i_ * COUPe * g_(2,mu)
*+i_  * COUPe *  shiftdum * deltaalphaover2alpha * g_(2,mu)
-i_ * COUPe * COUPsinW * COUPcosW * POLESVC * d6dum * g_(2,mu)
+i_ * newshiftdum * d6dum * deltaenew * g_(2,mu)
;

if ( count(d6dum,1) > 1 ) discard;
.sort
id VGpGmlbarl(p3?,p4?,p1?,p2?) =
 + i_ * g_(2,gamma1,7_)/2 * (p3(gamma1)-p4(gamma1)) * d6dum*COUPcphil1
 - i_ * g_(2,gamma1,7_)/2 * (p3(gamma1)-p4(gamma1)) * d6dum*COUPcphil3
 + i_ * g_(2,gamma1,6_)/2 * (p3(gamma1)-p4(gamma1)) * d6dum*COUPcphie;

id VGpGmnubarnu(p3?,p4?,p1?,p2?) =
 + i_ * g_(2,gamma1,7_)/2 * (p3(gamma1)-p4(gamma1)) * d6dum*COUPcphil1
 + i_ * g_(2,gamma1,7_)/2 * (p3(gamma1)-p4(gamma1)) * d6dum*COUPcphil3;


id VsbartG(p1?,p2?,p3?) = -i_ * sqrt_(2)/COUPv * COUPVts * (- mt * g_(1,6_)/2)
- i_ *  COUPVts * d6dum*COUPcphiq3 * sqrt_(2) * COUPv * g_(1,6_,gamma4)/2 * p3(gamma4)
*+ i_ * d6dum * shiftdum * COUPVts * deltaGF/ COUPv * (-mt * g_(1,6_)/2)
+ i_ * d6dum * newshiftdum * COUPVts * sqrt_(2)*deltavnew/COUPv^2 * (-mt * g_(1,6_)/2)
;
id VsbartG2(p1?,p2?,p3?) = -i_ * sqrt_(2)/COUPv * COUPVts * (- mt * g_(2,6_)/2)
- i_ *  COUPVts * d6dum*COUPcphiq3 * sqrt_(2) * COUPv * g_(2,6_,gamma4)/2 * p3(gamma4)
*+ i_ * d6dum * shiftdum * COUPVts * deltaGF/ COUPv * (-mt * g_(1,6_)/2)
+ i_ * d6dum * newshiftdum * COUPVts * sqrt_(2)*deltavnew/COUPv^2 * (-mt * g_(2,6_)/2)
;
id VsbaruG(p1?,p2?,p3?) =- i_ *  COUPVts * d6dum*COUPcphiq3 * sqrt_(2) * COUPv * g_(1,6_,gamma4)/2 * p3(gamma4)
*+ i_ * d6dum * shiftdum * COUPVts * deltaGF/ COUPv * (-mt * g_(1,6_)/2)
*+ i_ * d6dum * newshiftdum * COUPVts * sqrt_(2)*deltavnew/COUPv^2 * (-mt * g_(2,6_)/2)
;
***TODO this rule is bullshit
id VlbarnuG(p1?,p2?,p3?) = 
- i_ *  d6dum*COUPcphil3 * sqrt_(2) * COUPv * ( g_(2,6_,gamma4)/2 * p3(gamma4))
;

id VnubarlG(p1?,p2?,p3?) =
+ i_ * d6dum*COUPcphil3 * sqrt_(2) * COUPv * ( g_(2,6_,gamma4)/2 * p3(gamma4));


id VtbarbG(p1?,p2?,p3?) = -i_ * sqrt_(2)/COUPv * COUPVtb * (- mt * g_(1,7_)/2 + mb*g_(1,6_)/2)
+ i_ *  COUPVtb * d6dum*COUPcphiq3 * sqrt_(2) * COUPv * g_(1,6_,gamma4)/2 * p3(gamma4)
+i_ * d6dum * COUPVtb * COUPcud*sqrt_(2) * g_(1,7_,gamma4)/2 * p3(gamma4)*mb*mt/COUPv
*+ i_ *  shiftdum* d6dum * COUPVtb * deltaGF/ COUPv * (- mt * g_(1,7_)/2 + mb*g_(1,6_)/2)
+ i_ * d6dum * newshiftdum * COUPVtb * sqrt_(2)* deltavnew/COUPv^2 * (- mt * g_(1,7_)/2 + mb*g_(1,6_)/2)
;
id VubarbG(p1?,p2?,p3?) = -i_ * sqrt_(2)/COUPv * COUPVtb * (mb*g_(1,6_)/2)
+ i_ *  COUPVtb * d6dum*COUPcphiq3 * sqrt_(2) * COUPv * g_(1,6_,gamma4)/2 * p3(gamma4)
*+ i_ *  shiftdum* d6dum * COUPVtb * deltaGF/ COUPv * (- mt * g_(1,7_)/2 + mb*g_(1,6_)/2)
+ i_ * d6dum * newshiftdum * COUPVtb * sqrt_(2)* deltavnew/COUPv^2 * (mb*g_(1,6_)/2)
;
id VtbarbG2(p1?,p2?,p3?) = -i_ * sqrt_(2)/COUPv * COUPVtb * (- mt * g_(2,7_)/2 + mb*g_(2,6_)/2)
+ i_ *  COUPVtb * d6dum*COUPcphiq3 * sqrt_(2) * COUPv * g_(2,6_,gamma4)/2 * p3(gamma4)
+i_ * d6dum * COUPVtb * COUPcud*sqrt_(2) * g_(2,7_,gamma4)/2 * p3(gamma4)*mb*mt/COUPv
*+ i_ *  shiftdum* d6dum * COUPVtb * deltaGF/ COUPv * (- mt * g_(2,7_)/2 + mb*g_(2,6_)/2)
+ i_ * d6dum * newshiftdum * COUPVtb * sqrt_(2)* deltavnew/COUPv^2 * (- mt * g_(2,7_)/2 + mb*g_(2,6_)/2)
;

id VGpwmgam(p1?,p2?,p3?,beta?,gamma?) = i_ * COUPg * COUPv * COUPe/2 * d_(beta,gamma)
*+ i_ *  shiftdum * COUPg * mw * COUPsinW * deltaalphaover2alpha * d_(beta, gamma)
- i_ *  2 * COUPcosW * d6dum*POLESVC/COUPv * (d_(beta,gamma)*p2(gamma4)*p3(gamma4)-p2(gamma)*p3(beta))
*-i_/2 * COUPcosW * d6dum * POLESVC/COUPv * (d_(beta,gamma)*(COUPsinW^2*(4*p2(gamma4)*p3(gamma4)+COUPg^2*COUPv^2)+4*COUPcosW^2*p2(gamma4)*p3(gamma4))-4*(COUPsinW^2+COUPcosW^2)*p2(gamma)*p3(beta))
-i_/2 * d6dum * POLESVC * COUPv * COUPe * COUPcosW^2 * COUPgp * d_(beta,gamma)
+ i_ * deltaWGA * d6dum * newshiftdum * d_(beta,gamma)
;

id VwpGmgam(p1?,p2?,p3?,beta?,gamma?) = i_ * COUPg * COUPv * COUPe/2 * d_(beta,gamma)
*+ i_ *  shiftdum * COUPg * mw * COUPsinW * deltaalphaover2alpha * d_(beta, gamma)
*- i_ *  2 * COUPv * COUPcosW * d6dum*COUPcHWB * (d_(beta,gamma)*p1(gamma4)*p3(gamma4)-p1(gamma)*p3(beta))
*-i_/2 * COUPcosW * d6dum * POLESVC/COUPv * (d_(beta,gamma)*(COUPsinW^2*(4*p1(gamma4)*p3(gamma4)+COUPg^2*COUPv^2)+4*COUPcosW^2*p1(gamma4)*p3(gamma4))-4*(COUPsinW^2+COUPcosW^2)*p1(gamma)*p3(beta))
- i_ *  2 * COUPcosW * d6dum*POLESVC/COUPv * (d_(beta,gamma)*p1(gamma4)*p3(gamma4)-p1(gamma)*p3(beta))
-i_/2 * d6dum * POLESVC * COUPv * COUPe * COUPcosW^2 * COUPgp * d_(beta,gamma)
+ i_ * deltaWGA * d6dum * newshiftdum * d_(beta,gamma)
;

id VGpGmgam(p1?,p2?,p3?,gamma?) = - i_ * COUPe * (p1(gamma)-p2(gamma))
*- i_ *  shiftdum * COUPe * deltaalphaover2alpha * (p1(gamma)-p2(gamma))
+ i_ * d6dum * COUPg * COUPsinW^2 * COUPcosW * POLESVC * (p1(gamma)-p2(gamma))
- i_ * deltaenew * newshiftdum * d6dum * (p1(gamma)-p2(gamma))
;


****TODO this is bullshit
id VwpGmZ(p1?,p2?,p3?,beta?,gamma?) = -i_ * COUPgp * COUPg * COUPsinW * COUPv/2 * d_(beta,gamma)
*+ i_ *  2 * COUPv * COUPsinW * d6dum*COUPcHWB * (d_(beta,gamma)*p3(gamma4)*p1(gamma4)-p1(gamma)*p3(beta))
-i_ *  COUPg * mw * COUPv^2 * 1/2 * 1/COUPcosW * d6dum*COUPcHD * d_(beta,gamma)
*-i_ *  shiftdum * COUPg * mw * 1/COUPcosW *(COUPsinW^2*deltaalphaover2alpha -COUPv^2/4*d6dum*COUPcHD) * d_(beta,gamma)
*;
*-i_ * shiftdum * d6dum *COUPgp * COUPsinW * mw * (COUPv^2/2 * COUPg/COUPgp * d6dum*COUPcHWB + deltag1/COUPgp + deltag2/COUPg + deltasW2/2/COUPsinW^2 + deltaGF/sqrt_(2) )* d_(beta,gamma)
*-i_ * COUPgp * COUPg * COUPsinW * COUPv/2 * (DAVEdeltag1overg1 + DAVEdeltasW2/2/COUPsinW^2 + DAVEVC*COUPcosW/2/COUPsinW) * d_(beta,gamma)
- i_/2 * COUPsinW * d6dum * POLESVC/COUPv * (d_(beta, gamma) * (-4*(COUPcosW^2+COUPsinW^2)*p1(gamma4)*p3(gamma4)+COUPg^2*COUPcosW^2*COUPv^2)+4*(COUPsinW^2+COUPcosW^2)*p1(gamma)*p3(beta))
-i_ * newshiftdum * d6dum * deltaWGZ * d_(beta,gamma);
***SOPHIE'S TENTATIVE CATEGORISATION OF HWB FOLLOWS
*+ i_ *  2 * COUPv * COUPsinW * d6dum*COUPsophiecHWB * (d_(beta,gamma)*p3(gamma4)*p1(gamma4)-p1(gamma)*p3(beta))
*- i_ * d_(beta,gamma)* fielddum * 1/4 * COUPgp * COUPg * COUPv^3 * COUPcosW * d6dum * COUPsophiecHWB
*- i_ * shiftdum *d_(beta,gamma)* (1/2 * sophiedeltag1 * COUPg * COUPv * COUPsinW + 1/2 * sophiedeltag2 * COUPgp * COUPv * COUPsinW + 1/2 * COUPgp * COUPg * 1/2 * 1/COUPv * sophiedeltaGF/COUPGF * COUPsinW)
*-i_ * fielddum * d_(beta,gamma)*  1/4 * COUPgp * COUPg * COUPv * sophiedeltas2 / COUPsinW;
*;

****TODO this is bullshit
****TODO remove minus one here and above
id VGpwmZ(p1?,p2?,p3?,beta?,gamma?) = -i_ * COUPgp * COUPg * COUPsinW * COUPv/2 * d_(beta,gamma)
*+ i_ *  2 * COUPv * COUPsinW * d6dum*COUPcHWB * (d_(beta,gamma)*p3(gamma4)*p2(gamma4)-p2(gamma)*p3(beta))
-i_ *  COUPg * mw * COUPv^2 * 1/2 * 1/COUPcosW * d6dum*COUPcHD * d_(beta,gamma)
*-i_ *  shiftdum * COUPg * mw * 1/COUPcosW *(COUPsinW^2*deltaalphaover2alpha -COUPv^2/4*d6dum*COUPcHD) * d_(beta,gamma)
*;
*-i_ * shiftdum * d6dum *COUPgp * COUPsinW * mw * (COUPv^2/2 * COUPg/COUPgp * d6dum*COUPcHWB + deltag1/COUPgp + deltag2/COUPg + deltasW2/2/COUPsinW^2 + deltaGF/sqrt_(2) )* d_(beta,gamma)
*-i_ * COUPgp * COUPg * COUPsinW * COUPv/2 * (DAVEdeltag1overg1 + DAVEdeltasW2/2/COUPsinW^2 + DAVEVC*COUPcosW/2/COUPsinW) * d_(beta,gamma)
****Dave commented the following out and replaced it with one with p2 instead of p1
*- i_/2 * COUPsinW * POLESVC/COUPv * (d_(beta, gamma) * (-4*(COUPcosW^2+COUPsinW^2)*p1(gamma4)*p3(gamma4)+COUPg^2*COUPcosW^2*COUPv^2)+4*(COUPsinW^2+COUPcosW^2)*p1(gamma)*p3(beta));
- i_/2 * COUPsinW * d6dum * POLESVC/COUPv * (d_(beta, gamma) * (-4*(COUPcosW^2+COUPsinW^2)*p2(gamma4)*p3(gamma4)+COUPg^2*COUPcosW^2*COUPv^2)+4*(COUPsinW^2+COUPcosW^2)*p2(gamma)*p3(beta))
-i_* deltaWGZ * d6dum * newshiftdum * d_(beta,gamma);
***SOPHIE'S TENTATIVE CATEGORISATION OF HWB FOLLOWS
*+ i_ *  2 * COUPv * COUPsinW * d6dum*COUPsophiecHWB * (d_(beta,gamma)*p3(gamma4)*p1(gamma4)-p1(gamma)*p3(beta))
*- i_ * fielddum * d_(beta,gamma) * 1/4 * COUPgp * COUPg * COUPv^3 * COUPcosW * d6dum * COUPsophiecHWB
*- i_ * shiftdum * d_(beta,gamma) * (1/2 * sophiedeltag1 * COUPg * COUPv * COUPsinW + 1/2 * sophiedeltag2 * COUPgp * COUPv * COUPsinW + 1/2 * COUPgp * COUPg * 1/2 * 1/COUPv * sophiedeltaGF/COUPGF * COUPsinW)
*-i_ * fielddum * d_(beta,gamma) * 1/4 * COUPgp * COUPg * COUPv * sophiedeltas2 / COUPsinW;
*;
if ( count(d6dum,1) > 1 ) discard;
.sort

id VGpGmZ(p1?,p2?,p3?,gamma?) = i_ * (COUPgp*COUPsinW - COUPg*COUPcosW)/2 * (p1(gamma)-p2(gamma))
+ i_ *  COUPg / (2*COUPcosW) * COUPv^2 * d6dum*COUPcHD/2 *(p1(gamma)-p2(gamma))
*+ i_ *  shiftdum * d6dum * COUPg/(2*COUPcosW)*(-COUPcosW^2*deltag2/COUPg+COUPcosW*COUPsinW*deltag1/COUPg+deltasW2)* (p1(gamma)-p2(gamma))
*+ i_/2 * (COUPg/2/COUPcosW * DAVEdeltasW2 + COUPgp/2/COUPsinW * DAVEdeltasW2 + COUPsinW*COUPgp*DAVEdeltag1overg1) * (p1(gamma)-p2(gamma))
- i_/2 * COUPe * d6dum * POLESVC * (COUPsinW^2-COUPcosW^2) * (p1(gamma)-p2(gamma))
+ i_ * newshiftdum * d6dum * deltaGGZ * (p1(gamma)-p2(gamma));
****SOPHIE'S TENTATIVE CATEGORISATION OF HWB FOLLOWS
*+ i_/2 * fielddum * (COUPgp * sophiedeltas2/COUPsinW )*(p1(gamma)-p2(gamma))
*+i_/2* shiftdum * (sophiedeltag1 * COUPsinW - sophiedeltag2 * COUPcosW)*(p1(gamma)-p2(gamma));
*;

id VwpwmZ(p1?,p2?,p3?,alpha?,beta?,gamma?) = i_ * COUPg * COUPcosW * ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)))
*+i_ *  shiftdum *d6dum * COUPg/2 * COUPcosW *(deltag1Z+deltakappaZ)* ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)))
-i_ *  COUPcosW * d6dum*COUPcW * (p2(gamma)*p3(alpha)*p1(beta)-p1(gamma)*p2(alpha)*p3(beta)+d_(gamma, alpha)*(p3(beta)*p1(gamma4)*p2(gamma4)-p1(beta)*p2(gamma4)*p3(gamma4))+d_(alpha,beta)*(p1(gamma)*p2(gamma4)*p3(gamma4)-p2(gamma)*p3(gamma4)*p1(gamma4))+d_(gamma,beta)*(p2(alpha)*p3(gamma4)*p1(gamma4)-p3(alpha)*p1(gamma4)*p2(gamma4)))
* Wills FR:
* -i_ * COUPg/2 * COUPsinW * COUPv^2 * d6dum*COUPcHWB *  (d_(alpha, beta)*(p1(gamma)+p2(gamma))-d_(alpha,gamma)*(p1(beta)+p3(beta))+d_(beta,gamma)*(p3(alpha)-p2(alpha)))
* Daves FR:
* +i_ * COUPg/2 * COUPsinW * COUPv^2 * d6dum*COUPcHWB * ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)));
* maybe Trotts FR:
+ i_ * COUPv^2 * COUPg * COUPsinW * d6dum * COUPcHWB * (d_(alpha,beta)*(p1(gamma)-p2(gamma))-d_(alpha,gamma)*p1(beta)+d_(beta,gamma)*p2(alpha))
*+ i_ * COUPg * COUPsinW * DAVEVC * (d_(alpha,beta)*(-p1(gamma)+p2(gamma))+d_(alpha,gamma)*p1(beta)-d_(beta,gamma)*p2(alpha))
+ i_ * COUPe * d6dum * POLESVC * (d_(alpha,beta) * COUPsinW^2 * (p1(gamma)-p2(gamma)) + d_(beta, gamma) * (COUPsinW^2*p2(alpha)+COUPcosW^2*p3(alpha))+d_(alpha,gamma)*(-COUPcosW^2*p3(beta)-COUPsinW^2*p1(beta)) )
+ i_ * deltaWWZ * d6dum * newshiftdum * ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)));
****SOPHIE'S TENTATIVE CATEGORISATION OF HWB FOLLOWS
*+ i_ * shiftdum * (COUPcosW * sophiedeltag2) * ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)))
*+ i_ * fielddum * ( - 1/2 * COUPg * sophiedeltas2/COUPcosW) * ( d_(alpha,beta) * (p1(gamma) - p2(gamma)) + d_(beta,gamma) * (p2(alpha) - p3(alpha)) + d_(gamma,alpha) * (p3(beta) - p1(beta)))
*+ i_ * 1/2 * COUPe * COUPv^2 * COUPsophiecHWB * fielddum * d6dum * (d_(alpha,beta) * (p1(gamma)-p2(gamma)) + d_(beta, gamma) * (-p3(alpha)+p2(alpha)) + d_(alpha,gamma) * (-p1(beta)+p3(beta)) )
*+ i_ * COUPe * COUPv^2 * COUPsophiecHWB * d6dum * (d_(beta,gamma) * p3(alpha) - d_(alpha,gamma) * p3(beta));
*;

if ( count(d6dum,1) > 1 ) discard;
.sort
id VtbartZ(p1?,p2?,p3?,mu?) = i_ * 1/6 * COUPsinW/COUPgp* ( (COUPgp^2-3*COUPg^2) * g_(1,mu,7_)/2 + 4*COUPgp^2*g_(1,mu,6_)/2)
 + i_ *  d6dum*COUPcphiq1 * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,7_)/2
 - i_ *  d6dum*COUPcphiq3 * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,7_)/2
 + i_ *  d6dum*COUPcphiu * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,6_)/2
+i_*d6dum*COUPv/sqrt_(2)*(g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2)*p3(gamma4)*(-COUPcuW*COUPcosW+COUPsinW*COUPcuB)
+i_*d6dum*COUPv/sqrt_(2)*(g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2)*p3(gamma4)*(-COUPcuW*COUPcosW+COUPsinW*COUPcuB)
* + i_ *  shiftdum * d6dum * COUPg/COUPcosW * ((-1/2+2/3*COUPsinW^2)*deltagZ+2/3*deltasW2) * g_(1,mu,7_)/2
* + i_ *  shiftdum * d6dum *COUPg/COUPcosW * ((2/3*COUPsinW^2)*deltagZ+2/3*deltasW2) * g_(1,mu,6_)/2
* + i_ * COUPv^2*d6dum*COUPcHWB/2 * 1/6 * COUPsinW/COUPgp* ( (COUPgp^2*COUPcosW/COUPsinW-3*COUPg^2*COUPsinW/COUPcosW) * g_(1,mu,7_)/2 + 4*COUPgp^2*COUPcosW/COUPsinW*g_(1,mu,6_)/2);
* - i_ * 2/3 * COUPe * DAVEVC * (g_(1,6_,mu)/2+g_(1,7_,mu)/2)
 - i_ * 1/6 * COUPe * d6dum * POLESVC * ((3*COUPsinW^2-COUPcosW^2)*g_(1,6_,mu)/2-4*COUPcosW^2*g_(1,7_,mu)/2)
****SOPHIE'S TENTATIVE CATEGORISATION OF HWB FOLLOWS
*+ i_ * g_(1,mu,6_)/2 * (1/3 * fielddum * d6dum * COUPv^2 * COUPe * COUPsophiecHWB + shiftdum *(2/3 * sophiedeltag1 * COUPsinW) + fielddum*(1/3 * COUPgp * sophiedeltas2/COUPsinW))
*+ i_ * g_(1,mu,7_)/2 * (-1/6 * fielddum * d6dum * COUPv^2 * COUPe * COUPsophiecHWB + shiftdum * (-1/2 * sophiedeltag2 *COUPcosW) +fielddum*(1/6 * sophiedeltag1 * COUPsinW + 1/3 * COUPg/COUPcosW * sophiedeltas2))
+ i_ * d6dum * newshiftdum * (deltattZL * g_(1,mu,7_)/2 + deltattZR * g_(1,mu,6_)/2)
;

id VsbarsZ(p1?,p2?,p3?,mu?) = i_ * 1/6 * COUPsinW/COUPgp* ( (COUPgp^2+3*COUPg^2) * g_(1,mu,7_)/2 - 2*COUPgp^2*g_(1,mu,6_)/2)
 + i_ *  d6dum*COUPcphiq1 * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,7_)/2
 + i_ *  d6dum*COUPcphiq3 * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,7_)/2
 + i_ *  d6dum*COUPcphid * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,6_)/2
* + i_ *  shiftdum * d6dum *COUPg/COUPcosW * ((1/2-1/3*COUPsinW^2)*deltagZ-1/3*deltasW2) * g_(1,mu,7_)/2
* + i_ *  shiftdum * d6dum * COUPg/COUPcosW * ((-1/3*COUPsinW^2)*deltagZ-1/3*deltasW2) * g_(1,mu,6_)/2
* + i_ * COUPv^2*d6dum*COUPcHWB/2 * 1/6 * COUPsinW/COUPgp* ( (COUPgp^2*COUPcosW/COUPsinW+3*COUPg^2*COUPsinW/COUPcosW) * g_(1,mu,7_)/2 - 2*COUPgp^2*COUPcosW/COUPsinW*g_(1,mu,6_)/2);
* + i_ * 1/3 * COUPe * DAVEVC * (g_(1,6_,mu)/2+g_(1,7_,mu)/2)
****Dave commented out the following line and replaced a sin with a cos
* + i_ * 1/6 * COUPe * POLESVC * ((3*COUPsinW^2+COUPcosW^2)*g_(1,6_,mu)/2-2*COUPsinW^2*g_(1,7_,mu)/2);
 + i_ * 1/6 * COUPe * d6dum * POLESVC * ((3*COUPsinW^2+COUPcosW^2)*g_(1,6_,mu)/2-2*COUPcosW^2*g_(1,7_,mu)/2)
****SOPHIE'S TENTATIVE CATEGORISATION OF HWB FOLLOWS
*+ i_ * g_(1,mu,6_)/2 * (-1/6* COUPv^2 * COUPsophiecHWB * fielddum * d6dum * COUPe + shiftdum * (-1/3 * sophiedeltag1 * COUPsinW)+fielddum*( -1/6 * COUPgp * sophiedeltas2/COUPsinW))
*+i_ * g_(1,mu,7_)/2 *(fielddum * d6dum * 1/3 * COUPv^2 * COUPe * COUPsophiecHWB + shiftdum * (1/2 * sophiedeltag2 * COUPcosW + 1/6 * sophiedeltag1 * COUPsinW)+fielddum*( -1/6 * COUPg * sophiedeltas2/COUPcosW)) 
+ i_ * newshiftdum * d6dum * (deltassZL * g_(1,mu,7_)/2 + deltassZR * g_(1,mu,6_)/2)
;


id VbbarbZ(p1?,p2?,p3?,mu?) = i_ * 1/6 * COUPsinW/COUPgp* ( (COUPgp^2+3*COUPg^2) * g_(1,mu,7_)/2 - 2*COUPgp^2*g_(1,mu,6_)/2)
 + i_ *  d6dum*COUPcphiq1 * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,7_)/2
 + i_ *  d6dum*COUPcphiq3 * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,7_)/2
 + i_ *  d6dum*COUPcphid * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(1,mu,6_)/2
+i_*d6dum*(g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2)*p3(gamma4)*(COUPcdW*COUPcosW+COUPsinW*COUPcdB)*mb
+i_*d6dum*(g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2)*p3(gamma4)*(COUPcdW*COUPcosW+COUPsinW*COUPcdB)*mb
* + i_ *  shiftdum * d6dum * COUPg/COUPcosW * ((1/2-1/3*COUPsinW^2)*deltagZ-1/3*deltasW2) * g_(1,mu,7_)/2
* + i_ *  shiftdum * d6dum *COUPg/COUPcosW * ((-1/3*COUPsinW^2)*deltagZ-1/3*deltasW2) * g_(1,mu,6_)/2
* + i_ * COUPv^2*d6dum*COUPcHWB/2 * 1/6 * COUPsinW/COUPgp* ( (COUPgp^2*COUPcosW/COUPsinW+3*COUPg^2*COUPsinW/COUPcosW) * g_(1,mu,7_)/2 - 2*COUPgp^2*COUPcosW/COUPsinW*g_(1,mu,6_)/2);
* + i_ * 1/3 * COUPe * DAVEVC * (g_(1,6_,mu)/2+g_(1,7_,mu)/2)
****Dave commented out the following line and replaced a sin with a cos
* + i_ * 1/6 * COUPe * POLESVC * ((3*COUPsinW^2+COUPcosW^2)*g_(1,6_,mu)/2-2*COUPsinW^2*g_(1,7_,mu)/2);
 + i_ * 1/6 * COUPe * d6dum * POLESVC * ((3*COUPsinW^2+COUPcosW^2)*g_(1,6_,mu)/2-2*COUPcosW^2*g_(1,7_,mu)/2)
*+ i_ * g_(1,mu,6_)/2 * (-1/6* COUPv^2 * COUPsophiecHWB * fielddum * d6dum * COUPe + shiftdum * (-1/3 * sophiedeltag1 * COUPsinW)+fielddum*( -1/6 * COUPgp * sophiedeltas2/COUPsinW))
*+i_ * g_(1,mu,7_)/2 *(fielddum * d6dum * 1/3 * COUPv^2 * COUPe * COUPsophiecHWB + shiftdum * (1/2 * sophiedeltag2 * COUPcosW + 1/6 * sophiedeltag1 * COUPsinW)+fielddum*( -1/6 * COUPg * sophiedeltas2/COUPcosW)) 
+ i_ * newshiftdum * d6dum * (deltabbZL * g_(1,mu,7_)/2 + deltabbZR * g_(1,mu,6_)/2)
;



id VlbarlZ(p1?,p2?,p3?,mu?) = -i_ * 1/2 * COUPsinW/COUPgp* ( (COUPgp^2-COUPg^2) * g_(2,mu,7_)/2 + 2*COUPgp^2*g_(2,mu,6_)/2)
 + i_ *  d6dum*COUPcphil1 * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(2,mu,7_)/2
 + i_ *  d6dum*COUPcphil3 * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(2,mu,7_)/2
 + i_ *  d6dum*COUPcphie * COUPv^2 * (COUPg^2 + COUPgp^2) * COUPcosW/COUPg/2 * g_(2,mu,6_)/2
* + i_ *  shiftdum * d6dum * COUPg/COUPcosW * ((1/2-COUPsinW^2)*deltagZ-deltasW2) * g_(2,mu,7_)/2
* + i_ *  shiftdum * d6dum * COUPg/COUPcosW * ((-COUPsinW^2)*deltagZ-deltasW2) * g_(2,mu,6_)/2
* -i_ * COUPv^2*d6dum*COUPcHWB/2 *1/2 * COUPsinW/COUPgp* ( (COUPgp^2*COUPcosW/COUPsinW-COUPg^2*COUPsinW/COUPcosW) * g_(2,mu,7_)/2 + 2*COUPgp^2*COUPcosW/COUPsinW*g_(2,mu,6_)/2);
* + i_ *  COUPe * DAVEVC * (g_(2,6_,mu)/2+g_(2,7_,mu)/2)
 + i_ * 1/2 * COUPe * d6dum * POLESVC * ((COUPsinW^2-COUPcosW^2)*g_(2,6_,mu)/2-2*COUPcosW^2*g_(2,7_,mu)/2)
****SOPHIE'S TENTATIVE CATEGORISATION OF HWB FOLLOWS
*+ i_ * g_(2,mu,6_)/2 * (-1/2 * COUPgp * COUPv^2 * COUPcosW * COUPsophiecHWB * fielddum * d6dum + shiftdum * (-sophiedeltag1 * COUPsinW)+fielddum*( - 1/2 * COUPgp * sophiedeltas2/COUPsinW))
*+ i_ * g_(2, mu, 7_)/2 * (shiftdum * (1/2 * sophiedeltag2 * COUPcosW - 1/2 * sophiedeltag1 * COUPsinW)+fielddum*(- 1/2 * COUPg * sophiedeltas2/COUPcosW));
+ i_ * newshiftdum *d6dum * (deltallZL * g_(2,mu,7_)/2 + deltallZR * g_(2,mu,6_)/2)
;

id VnubarnuZ(p1?,p2?,p3?,mu?)=i_ * 1/2 * COUPsinW/COUPgp * (COUPgp^2+COUPg^2)*(g_(2,mu,6_)/2-g_(2,mu,7_)/2)
+i_ * 1/2 * COUPsinW/COUPgp *(COUPg^2+COUPgp^2)* d6dum *COUPv^2 * (COUPcphil1*g_(2,mu,7_)/2-COUPcphil3*g_(2,mu,7_)/2)
+i_ * 1/2 * COUPe * d6dum * POLESVC * (g_(2,mu,6_)/2-g_(2,mu,7_)/2)
+i_ * newshiftdum * d6dum * deltanunuZ * (g_(2,mu,6_)/2 - g_(2,mu,7_)/2);

if ( count(d6dum,1) > 1 ) discard;
.sort


id VWpGmlbarl(p4?,p3?,p1?,p2?,alpha?) = 
-i_ * COUPg * COUPv *  d6dum*COUPcphil1 * g_(2,alpha,7_)/2
-i_ * COUPg * COUPv * d6dum*COUPcphie * g_(2,alpha,6_)/2;
id VGpWmlbarl(p4?,p3?,p1?,p2?,alpha?) = 
-i_ * COUPg * COUPv *  d6dum*COUPcphil1 * g_(2,alpha,7_)/2
-i_ * COUPg * COUPv *  d6dum*COUPcphie * g_(2,alpha,6_)/2;

id VWpGmnubarnu(p4?,p3?,p1?,p2?,alpha?) = 
-i_ * COUPg * COUPv *  d6dum*COUPcphil1 * g_(2,alpha,7_)/2;
id VGpWmnubarnu(p4?,p3?,p1?,p2?,alpha?) = 
-i_ * COUPg * COUPv *  d6dum*COUPcphil1 * g_(2,alpha,7_)/2;

id Vtbartlbarl(p4?,p3?,p2?,p1?) =
+ i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq1
- i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq3
+ i_ * g_(1,gamma1,6_)/2 * g_(2,gamma1,6_)/2 *  d6dum*COUPceu
+ i_ * g_(1,gamma1,6_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclu
+ i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,6_)/2 *  d6dum*COUPcqe;

id Vtbartnubarnu(p4?,p3?,p2?,p1?) =
+ i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq1
+ i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq3
+ i_ * g_(1,gamma1,6_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclu;

id Vsbarslbarl(p4?,p3?,p2?,p1?) = Vbbarblbarl(p4,p3,p2,p1);

id Vbbarblbarl(p4?,p3?,p2?,p1?) =
+ i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq1
+ i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq3
+ i_ * g_(1,gamma1,6_)/2 * g_(2,gamma1,6_)/2 *  d6dum*COUPced
+ i_ * g_(1,gamma1,6_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPcld
+ i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,6_)/2 *  d6dum*COUPcqe;

id Vsbarsnubarnu(p4?,p3?,p2?,p1?) = Vbbarbnubarnu(p4,p3,p2,p1);

id Vbbarbnubarnu(p4?,p3?,p2?,p1?) =
+ i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq1
- i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq3
+ i_ * g_(1,gamma1,6_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPcld;

id Vtbarblbarnu(p4?,p3?,p2?,p1?) =
+ 2 * COUPVtb * i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq3;

id Vsbartnubarl(p4?,p3?,p2?,p1?) =
+ 2 * COUPVts * i_ * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2 *  d6dum*COUPclq3;

id VlbarnuW(p1?,p2?,p3?,mu?) = -i_ * COUPg * g_(2,mu,7_)/2 * sqrt_(2)/2
-i_ *  COUPg * g_(2,mu,7_)/2 * sqrt_(2)/2 * COUPv^2 * d6dum*COUPcphil3
*+ i_ *  shiftdum * d6dum * COUPg * g_(2,mu,7_)/2 * deltaGF/2
- i_ * d6dum *newshiftdum * sqrt_(2)/2 * deltag2new * g_(2,mu,7_)/2

;

id VnubarlW(p1?,p2?,p3?,mu?) = -i_ * COUPg * g_(2,mu,7_)/2 * sqrt_(2)/2
-i_ *  COUPg * g_(2,mu,7_)/2 * sqrt_(2)/2 * COUPv^2 * d6dum*COUPcphil3
*+ i_ *  shiftdum * d6dum * COUPg * g_(2,mu,7_)/2 * deltaGF/2
- i_ * newshiftdum * d6dum * sqrt_(2)/2 * deltag2new * g_(2,mu,7_)/2
;

id VsbartGmZ(p1?,p2?,p3?,p4?,mu?) = i_ *  d6dum*COUPcphiq3 * COUPVts * sqrt_(2) * COUPgp * COUPsinW * COUPv * g_(1,mu,7_)/2
+ i_ * d6dum * COUPVts *(-COUPcosW*COUPcuW-COUPsinW*COUPcuB) * (g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2) * p4(gamma4)*sqrt_(2)*mt/COUPv
;
id VtbarbGpZ(p1?,p2?,p3?,p4?,mu?) = i_ *  d6dum*COUPcphiq3 * COUPVtb * sqrt_(2) * COUPgp * COUPsinW * COUPv * g_(1,mu,7_)/2
+ i_ * d6dum * COUPVtb *(-COUPcosW*COUPcuW-COUPsinW*COUPcuB) * (g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2) * p4(gamma4)*sqrt_(2)*mt/COUPv
+ i_ * d6dum * COUPVtb * (-COUPcosW*COUPcdW+COUPsinW*COUPcdB) * (g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2) * p4(gamma4)*sqrt_(2)*mb/COUPv
+i_ * d6dum * COUPVtb * COUPe*sqrt_(2)*mt*mb/COUPv*COUPcud*g_(1,mu,6_)/2
;
id VsbartGmgam(p1?,p2?,p3?,p4?,mu?) = i_ *  d6dum*COUPcphiq3 * COUPVts * sqrt_(2) * COUPgp * COUPsinW * COUPv * g_(1,mu,7_)/2
+ i_ * d6dum * COUPVts *(-COUPsinW*COUPcuW+COUPcosW*COUPcuB) * (g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2) * p4(gamma4)*sqrt_(2)*mt/COUPv
;
id VtbarbGpgam(p1?,p2?,p3?,p4?,mu?) = i_ *  d6dum*COUPcphiq3 * COUPVtb * sqrt_(2) * COUPgp * COUPsinW * COUPv * g_(1,mu,7_)/2
+ i_ * d6dum * COUPVtb *(-COUPsinW*COUPcuW+COUPcosW*COUPcuB) * (g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2) * p4(gamma4)*sqrt_(2)*mt/COUPv
+ i_ * d6dum * COUPVtb * (-COUPsinW*COUPcdW-COUPcosW*COUPcdB) * (g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2) * p4(gamma4)*sqrt_(2)*mb/COUPv
+i_ * d6dum * COUPVtb * COUPe*sqrt_(2)*mt*mb/COUPv*COUPcud*g_(1,mu,6_)/2
;
id VtbarbGpglue(p1?,p2?,p3?,p4?,mu?) = 
+ i_ * d6dum * COUPVtb *(COUPe*COUPcuG) * (g_(1,mu,gamma4,7_)/2-g_(1,gamma4,mu,7_)/2) * p4(gamma4)*sqrt_(2)*mt/COUPv
+ i_ * d6dum * COUPVtb * (-COUPe*COUPcdG) * (g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2) * p4(gamma4)*sqrt_(2)*mb/COUPv
;
id VsbartGmglue(p1?,p2?,p3?,p4?,mu?) = 
+ i_ * d6dum * COUPVts *(COUPe*COUPcuG) * (g_(1,mu,gamma4,6_)/2-g_(1,gamma4,mu,6_)/2) * p4(gamma4)*sqrt_(2)*mt/COUPv
;

id Vsbartnil(p1?,p2?,mu?)=i_ *COUPVts *g_(1,mu,7_)/2;
id Vtbarbnil(p1?,p2?,mu?)=(4*COUPcqq)*d6dum*COUPVtb*g_(1,mu,7_)/2;

id Vtbartsbarb(p1?,p2?,p3?,p4?) = i_ * d6dum * (-2) * COUPVtb * COUPVts *COUPcqq * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2;
*+i_*d6dum* COUPcquqd * g_(1,gamma1,6_)/2 * g_(2,gamma1,6_)/2;
id Vtbarbsbart(p1?,p2?,p3?,p4?) = i_ * d6dum * COUPVtb * COUPVts * COUPcqqprime * g_(1,gamma1,7_)/2 * g_(2,gamma1,7_)/2;
*+i_*d6dum* COUPcquqd * g_(1,gamma1,6_)/2 * g_(2,gamma1,6_)/2;

id PRt(p1?,mu?) = i_* (g_(1,mu) * p1(mu) + mt*g_(1));
id PRt2(p1?,mu?) = i_* (g_(2,mu) * p1(mu) + mt*g_(2));
id PRu(p1?,mu?) = i_* (g_(1,mu) * p1(mu));
id PRu2(p1?,mu?) = i_* (g_(2,mu) * p1(mu));
id PRb(p1?,mu?) = i_* (g_(1,mu) * p1(mu) + mb*g_(1));
id PRs(p1?,mu?) = i_* (g_(1,mu) * p1(mu));
id PRnu(p1?,mu?) = i_* (g_(2,mu) * p1(mu) );
id PRl(p1?,mu?) = i_* (g_(2,mu) * p1(mu) );
*id PRwplus(p1?,mu?,nu?) = -i_ * (d_(mu,nu) - (1-xi) * p1(mu) * p1(nu) * deng );
*id PRGplus(p1?) = i_;
id PRz(p1?,mu?,nu?) = -i_ * d_(mu,nu) * (1-d6dum*sophiedeltamz2/mz2);
***************************
*********
********
*WARNING!!!!!
********
********
*changing the Feynman rules for PRwplus and PRGplus
*will not change the propagators in bsgamma.frm
*************************
**************************
id PRwplus(p1?,mu?,nu?) = -i_ * (1+d6dum*deltamW2new*denw) * (d_(mu,nu) - (1-xi) * p1(mu) * p1(nu) * deng *(1+xi*d6dum*deltamW2new*deng) );
id PRGplus(p1?) = i_ * (1+xi*d6dum*deltamW2new*deng);
*id PRz(p1?,mu?,nu?) = -i_ * d_(mu,nu) * (1-d6dum*deltamZ2new/mz2);
if ( count(d6dum,1) > 1 ) discard;
.sort


id GF=COUPg^2/(4*sqrt_(2)*mw2);
id deltasW2=2*COUPsinW^2*(1-COUPsinW^2)*(deltag1/COUPgp-deltag2/COUPg)+COUPv^2*COUPsinW*COUPcosW*(1-2*COUPsinW^2)*COUPcHWB;
id deltaalphaover2alpha=-deltaGF/sqrt_(2)+deltamz2/mz2*mw2/(2*(mw2-mz2))-COUPcHWB/(sqrt_(2)*GF)*COUPcosW*COUPsinW;
id deltakappaZ=deltag1Z;
id deltakappagamma=deltag1gamma;
id deltag1=-COUPgp*(deltaGF/sqrt_(2)+deltamz2/(2*COUPsinW^2*mz2));
id deltag2=-COUPg*deltaGF/sqrt_(2);
id deltaGF=1/(sqrt_(2)*GF)*(sqrt_(2)*COUPcphil3-1/sqrt_(2)*COUPcll);
id deltag1Z=1/(4*sqrt_(2)*GF)*(COUPcHD-4*COUPcphil3+2*COUPcll);
id deltag1gamma=1/(4*sqrt_(2)*GF)*(COUPcHD*mw2/(mw2-mz2)-4*COUPcphil3+2*COUPcll);
id deltagZ=-1/(4*sqrt_(2)*GF)*(COUPcHD+4*COUPcphil3-2*COUPcll);
id deltamz2=mz2*(1/(2*sqrt_(2)*GF)*COUPcHD+sqrt_(2)/GF*COUPcosW*COUPsinW*COUPcHWB);

*id DAVEVC=0;
*id DAVEdeltag1overg1=0;
*id DAVEdeltasW2=0;

*id shiftdum=0; ******todo todo todo
id COUPcHWB=0;
id POLESVC=COUPv^2*COUPcHWB;
id deltassZL=deltabbZL;
id deltassZR=deltabbZR;
id deltaWGZ=(COUPgp^4*COUPv*deltag2new + COUPg*COUPgp^3*(COUPv*deltag1new + COUPgp*deltavnew) +COUPg^3*COUPgp*(2*COUPv*deltag1new + COUPgp*deltavnew))/(2*(COUPg^2 + COUPgp^2)^(3/2));
id deltaGGZ=(3*COUPg^2*COUPgp*deltag1new + COUPgp^3*deltag1new - COUPg^3*deltag2new - 3*COUPg*COUPgp^2*deltag2new)/(2*(COUPg^2 + COUPgp^2)^(3/2));
id deltaWWZ=(COUPg*(-(COUPg*COUPgp*deltag1new) + COUPg^2*deltag2new + 2*COUPgp^2*deltag2new))/(COUPg^2 + COUPgp^2)^(3/2);
id deltattZL=(5*COUPg^2*COUPgp*deltag1new + COUPgp^3*deltag1new - 3*COUPg^3*deltag2new - 7*COUPg*COUPgp^2*deltag2new)/(6*(COUPg^2 + COUPgp^2)^(3/2));
id deltattZR=(2*COUPgp*(2*COUPg^2*deltag1new + COUPgp^2*deltag1new - COUPg*COUPgp*deltag2new))/(3*(COUPg^2 + COUPgp^2)^(3/2));
id deltabbZL=(-(COUPg^2*COUPgp*deltag1new) + COUPgp^3*deltag1new + 3*COUPg^3*deltag2new + 5*COUPg*COUPgp^2*deltag2new)/(6*(COUPg^2 + COUPgp^2)^(3/2));
id deltabbZR=-(COUPgp*(2*COUPg^2*deltag1new + COUPgp^2*deltag1new - COUPg*COUPgp*deltag2new))/(3*(COUPg^2 + COUPgp^2)^(3/2));
id deltallZL=(-3*COUPg^2*COUPgp*deltag1new - COUPgp^3*deltag1new + COUPg^3*deltag2new + 3*COUPg*COUPgp^2*deltag2new)/(2*(COUPg^2 + COUPgp^2)^(3/2));
id deltallZR=(COUPgp*(-2*COUPg^2*deltag1new - COUPgp^2*deltag1new + COUPg*COUPgp*deltag2new))/(COUPg^2 + COUPgp^2)^(3/2);
id deltaenew=(deltag2new*COUPgp^3 + deltag1new*COUPg^3)/(COUPgp^2 + COUPg^2)^(3/2);
id deltaWGA=(COUPg*(COUPg^2*COUPgp*COUPv*deltag2new + 2*COUPgp^3*COUPv*deltag2new + COUPg*COUPgp^3*deltavnew + COUPg^3*(COUPv*deltag1new + COUPgp*deltavnew)))/(2*(COUPg^2 + COUPgp^2)^(3/2));
id deltanunuZ=1/2*(COUPsinW*deltag1new+COUPcosW*deltag2new); 


*id POLESVC=0;
if ( count(d6dum,1) > 1 ) discard;
.sort
****DEFINITIONS FOR SOPHIE'S TENTATIVE CATEGORIZATION OF HWB
*id COUPcHWB=0;
id COUPsophiecHWB=0;
id COUPGF=1/sqrt_(2)*1/COUPv^2;
id sophiedeltag1=0;
id sophiedeltag2=0;
*id sophiedeltas2=-COUPsinW*COUPcosW*COUPv^2*COUPcHWB*d6dum;
id sophiedeltas2=0;
id sophiedeltaGF=0;
id sophiedeltamz2 = 1/2*COUPv*deltavnew*newshiftdum*(COUPgp^2+COUPg^2)+1/2*COUPv^2*(COUPgp*deltag1new*newshiftdum+COUPg*deltag2new*newshiftdum)+1/8*COUPv^4*COUPcHD*(COUPgp^2+COUPg^2)+1/2*COUPv^4*COUPgp*COUPg*COUPcHWB;
*id sophiedeltamz2 = 1/8*COUPv^4*COUPcHD*(COUPgp^2+COUPg^2)+1/2*COUPv^4*COUPgp*COUPg*COUPcHWB;
*id deltamZ2new=1/2*COUPv*deltavnew*(COUPgp^2+COUPg^2)+1/2*COUPv^2*(COUPgp*deltag1new+COUPg*deltag2new)+1/8*COUPv^4*COUPcHD*d6dum*(COUPgp^2+COUPg^2)+1/2*COUPv^4*COUPgp*COUPg*COUPcHWB*d6dum;
if ( count(d6dum,1) > 1 ) discard;
.sort
id deltamW2new=1/2*newshiftdum*COUPg*COUPv*(COUPv*deltag2new+COUPg*deltavnew);
if ( count(d6dum,1) > 1 ) discard;
.sort
**********in the {mZ, mW, GF} scheme, the three basis input shifts are:
id deltag1new=COUPv^2*(COUPgp*(-COUPcphil3+1/2*COUPcll)-1/4*1/COUPgp*(COUPg^2+COUPgp^2)*COUPcHD-COUPg*COUPcHWB);
if ( count(d6dum,1) > 1 ) discard;
.sort
id deltavnew=COUPv^3*(-1/2*COUPcll+COUPcphil3);
if ( count(d6dum,1) > 1 ) discard;
.sort
id deltag2new=COUPg*COUPv^2*(-COUPcphil3+1/2*COUPcll);
if ( count(d6dum,1) > 1 ) discard;
.sort
********************************************************
**********in the {mZ, alpha, GF} scheme, the three basis input shifts are:
*id deltag1new=COUPgp/(2*(COUPcosW^2-COUPsinW^2))*(COUPsinW^2*(1/2*COUPv^2*COUPcHD+COUPe*COUPv^3*COUPcosW/mw*COUPcHWB)+2*COUPcosW^3*COUPsinW*COUPv^2*COUPcHWB);
if ( count(d6dum,1) > 1 ) discard;
.sort
*id deltavnew=COUPv^3*(-1/2*COUPcll+COUPcphil3);
if ( count(d6dum,1) > 1 ) discard;
.sort
*id deltag2new=-COUPg/(2*(COUPcosW^2-COUPsinW^2))*(COUPcosW^2*(1/2*COUPv^2*COUPcHD+COUPe*COUPv^3*COUPcosW/mw*COUPcHWB)+2*COUPcosW*COUPsinW^3*COUPv^2*COUPcHWB);
if ( count(d6dum,1) > 1 ) discard;
.sort
********************************************************

*id COUPcHWB=0;
*id COUPsophiecHWB=COUPcHWB;
*id COUPGF=1/sqrt_(2)*1/COUPv^2;
*id sophiedeltag1=-COUPcosW/COUPsinW*COUPgp*COUPv^2*COUPcHWB*d6dum;
*id sophiedeltag2=0;
**id sophiedeltas2=-COUPsinW*COUPcosW*COUPv^2*COUPcHWB*d6dum;
*id sophiedeltas2=COUPsinW^2*COUPv^2*COUPcosW/COUPsinW*(COUPcosW^2-COUPsinW^2)*COUPcHWB*d6dum;
*id sophiedeltaGF=0;
*id sophiedeltamz2 = 1/8*COUPv^4*COUPcHD*d6dum*(COUPgp^2+COUPg^2)+1/2*COUPv^4*COUPgp*COUPg*COUPcHWB*d6dum;
