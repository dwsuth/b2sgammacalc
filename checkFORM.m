(* ::Package:: *)

(* ::Input:: *)
(*$Assumptions=Element[g1,Reals]&&Element[g2,Reals];*)
(*FORMtoMatRules={oneoveroneminusx->1/(1-x),logx->Log[x],logxi->Log[xi],logxoverxi->Log[x/xi],logxxi->Log[x*xi],COUPe->g2 *COUPsinW,mw->mW,mw2->mW^2,COUPgp->g1,COUPg->g2,COUPcosW->g2/Sqrt[g1^2+g2^2],COUPsinW->g1/Sqrt[g1^2+g2^2],mz2->mW^2/COUPcosW^2,mt->Sqrt[\[Kappa]]*mW,COUPv->2*mW/g2,eps->-1/2/Log[mW],GF->Sqrt[2]*g2^2/8/mW^2};*)
(*Rules2={x->\[Kappa],xi->\[Xi],COUPsinW->g1/Sqrt[g1^2+g2^2],COUPcosW->g2/Sqrt[g1^2+g2^2]};*)
(*StringReplacementRules={"g_(1,6_,delta1)"->"2*quarkL","g_(1,6_,N1_?)"->"2*quarkL","g_(1,6_,alpha)"->"2*quarkL","g_(1,7_,delta1)"->"2*quarkR","g_(1,7_,N1_?)"->"2*quarkR","g_(1,7_,alpha)"->"2*quarkR","g_(2,6_,delta1)"->"2*lepL","g_(2,6_,N1_?)"->"2*lepL","g_(2,6_,alpha)"->"2*lepL","g_(2,7_,delta1)"->"2*lepR","g_(2,7_,N1_?)"->"2*lepR","g_(2,7_,alpha)"->"2*lepR","pi_"->"Pi","log(x)"->"logx","log(xi)"->"logxi","log(xi^-1*x)"->"logxoverxi","log(xi*x)"->"logxxi","g_(1,6_,epspol)"->"2*vectorshiz","g_(1,6_)"->"2*scalarshiz","g_(1,7_,epspol)"->"2*vectorshizR","g_(1,7_)"->"2*scalarshizR","p1.epspol"->"p1eps","p2.epspol"->"p2eps","sqrt_(2)"->"Sqrt[2]",";"->";\n","g_(2,6_,p1)"->"2*lepRp1","g_(2,7_,p1)"->"2*lepLp1"};*)
(*$Assumptions=\[Kappa]>1&&\[Xi]>0&&g1>0&&g2>0;*)


(* ::Input:: *)
(*(*only needed on sophies mac*)*)
(*If[$MachineName!="walnuts",SetDirectory["~/Work/b2sgammacalc"];]*)
(**)


(* ::Input:: *)
(*inputFORMfiles={"HRMEout","bubblegamAout","bubbleZAout","bubbleZBout","bsgammaout"};*)
(*resultstrs=Import/@inputFORMfiles;*)
(*sanitisedStrings=StringReplace[StringDelete[#,"\n"|"\r"|" "],StringReplacementRules]&/@resultstrs;*)
(*ToExpression/@sanitisedStrings*)


(* ::Input:: *)
(*diagnamesbox={"Fbox","Gbox1","Gbox2"};*)
(*diagnamesnew={"Ga","Gb","Gc","Gd","Ge","Gf","Gg","GhW","GhG","GiW","GiG","Gj","Gk", "GgWgam","GgWgam","GgW","GfW"};*)
(*diagnamesZ={"FaWZ","FaGZ","FbGZ","FbWZ","FcZ","FdZ","FeZ","FfZ","FgZ","FhZ"};*)
(*diagnamesgam={"FaWgam","FaGgam","FbGgam","FbWgam","Fcgam","Fdgam","Fegam","Ffgam","Fggam","Fhgam"};*)
(*diagnamesnu={"FaWZnu","FaGZnu","FbGZnu","FbWZnu","FcZnu","FdZnu","FeZnu","FfZnu","FgZnu","FhZnu","Fboxnu"};*)
(*FORMbox=(16*Pi^2*{Fbox,Gbox1,Gbox2}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMnew=(16*Pi^2*{Ga,Gb,Gc,Gd,Ge,Gf,Gg,GhW,GhG,GiW,GiG,Gj,Gk,GgWgam,GgWgam,GgW,GfW}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMZ=(16*Pi^2*{FaGZ,FaWZ,FbGZ,FbWZ,FcZ,FdZ,FeZ,FfZ,FgZ,FhZ}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMgam=(16*Pi^2*{FaWgam,FaGgam,0,0,Fcgam,Fdgam,Fegam,Ffgam,Fggam,Fhgam}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)
(*FORMnu=(16*Pi^2*{FaWZnu,FaGZnu,FbGZnu,FbWZnu,FcZnu,FdZnu,FeZnu,FfZnu,FgZnu,FhZnu,Fboxnu}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*{FORMboxd4FeynGauge,FORMnewd4FeynGauge,FORMZd4FeynGauge,FORMgamd4FeynGauge,FORMnud4FeynGauge}=({FORMbox,FORMnew,FORMZ,FORMgam,FORMnu}/.d6dum->0)//Simplify//Expand;*)
(**)


1/2((Coefficient[Coefficient[FORMgam,COUPcHWB],p1eps]-Coefficient[Coefficient[FORMgam,COUPcHWB],p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))/.{newshiftdum->0,d6dum->1}//Simplify





(* ::Input:: *)
(*wilsonCoeffs={COUPcphil1,COUPcphil3,COUPcphie,COUPcphiq1,COUPcphiu,COUPcphiq3,COUPcphid,COUPclq1,COUPclq3,COUPceu,COUPclu,COUPcqe,COUPcll,COUPcW,COUPcHWB,COUPcHD,COUPced,COUPcld,COUPcuW,COUPcdW,COUPcuB,COUPcdB,COUPcud};*)
(*{FORMboxd6FeynGauge,FORMnewd6FeynGauge,FORMZd6FeynGauge,FORMgamd6FeynGauge,FORMnud6FeynGauge}=(Coefficient[Total[#],d6dum]&/@{FORMbox,FORMnew,FORMZ,FORMgam,FORMnu})//Simplify//Expand*)


(Gbox1+Gbox2)/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand


(* ::Input:: *)
(*(*Sophie's dim 4 Z and box results are below*)*)
(*FAZGSophie=(g1^2 g2^2 \[Kappa])/(16 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(3 g2^4 \[Kappa])/(16 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^2)/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^3)/(48 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^3)/(16 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa] Log[mW])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa] Log[mW])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^2 Log[mW])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2 Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^3 Log[mW])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa]^3 Log[mW])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2 Log[\[Kappa]])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^3 Log[\[Kappa]])/(24 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa]^3 Log[\[Kappa]])/(8 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2);*)
(*FAZWSophie=(g1^2 g2^2)/(24 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+g2^4/(8 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^2)/(24 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa]^2)/(8 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 Log[mW])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa] Log[mW])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa] Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^2 Log[mW])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2 Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2 Log[\[Kappa]])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2);*)
(*FBZGSophie=(g1^2 g2^2 \[Kappa])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa]))+(g2^4 \[Kappa])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa]))-(g1^2 g2^2 \[Kappa]^2)/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa]))-(g2^4 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa]))-(g1^2 g2^2 \[Kappa] Log[mW])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa]))-(g2^4 \[Kappa] Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa]))+(g1^2 g2^2 \[Kappa]^2 Log[mW])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa]))+(g2^4 \[Kappa]^2 Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa]))+(g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa]))+(g2^4 \[Kappa]^2 Log[\[Kappa]])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa]));*)
(*FBZWSophie=0;*)
(*FCZSophie=-((g1^2 g2^2)/(24 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2))+g2^4/(8 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(13 g1^2 g2^2 \[Kappa]^2)/(24 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(3 g2^4 \[Kappa]^2)/(8 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 Log[mW])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa] Log[mW])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa] Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^2 Log[mW])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2 Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa] Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa] Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2 Log[\[Kappa]])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2);*)
(*FDZSophie=(g1^2 g2^2 \[Kappa])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^2)/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa]^3)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa] Log[mW])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(2 g1^2 g2^2 \[Kappa]^2 Log[mW])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^3 Log[mW])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2 Log[\[Kappa]])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^3 Log[\[Kappa]])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2);*)
(*FEZSophie=-(g2^4/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2))-(g2^4 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(5 g2^4 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(3 g2^4 Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(6 g2^4 \[Kappa] Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(3 g2^4 \[Kappa]^2 Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(3 g2^4 \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2);*)
(*FFZSophie=-((g1^2 g2^2 \[Kappa])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2))+(g1^2 g2^2 \[Kappa]^2)/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2);*)
(*FGZSophie=-((g1^2 g2^2 \[Kappa])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2))+(g1^2 g2^2 \[Kappa]^2)/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2);*)
(*FHZSophie=-((g1^2 g2^2 \[Kappa])/(16 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2))+(g2^4 \[Kappa])/(16 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(3 g1^2 g2^2 \[Kappa]^3)/(16 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(3 g2^4 \[Kappa]^3)/(16 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa] Log[mW])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa] Log[mW])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^2 Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^4 \[Kappa]^2 Log[mW])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^3 Log[mW])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa]^3 Log[mW])/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^3 Log[\[Kappa]])/(8 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(g2^4 \[Kappa]^3 Log[\[Kappa]])/(8 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2);*)
(*FBoxSophie=-(g2^4/(4 mW^2 (-1+\[Kappa])^2))+(g2^4 \[Kappa])/(4 mW^2 (-1+\[Kappa])^2)-(g2^4 \[Kappa] Log[\[Kappa]])/(4 mW^2 (-1+\[Kappa])^2);*)
(*dim4zllvertex=quarkL*1/2*COUPsinW/COUPgp*((COUPgp^2-COUPg^2)*lepL+2*COUPgp^2*lepR) /mz2;*)
(*SophieZ={FAZGSophie,FAZWSophie,FBZGSophie,FBZWSophie,FCZSophie,FDZSophie,FEZSophie,FFZSophie,FGZSophie,FHZSophie}*(dim4zllvertex/.FORMtoMatRules/.Rules2)//Simplify//Expand;*)
(*Sophiebox={FBoxSophie*quarkL*lepL//Expand};*)
(*(*Sophie's b to s gamma results*)*)
(*FCgammap1Sophie=-((5 g1 g2^3 mb)/(27 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4))+(g1 g2^3 mb \[Kappa])/(Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^2)/(Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(5 g1 g2^3 mb \[Kappa]^3)/(27 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(2 g1 g2^3 mb Log[\[Kappa]])/(9 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(2 g1 g2^3 mb \[Kappa] Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4);*)
(*FCgammap2Sophie=(7 g1 g2^3 mb)/(27 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^2)/(Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(20 g1 g2^3 mb \[Kappa]^3)/(27 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(2 g1 g2^3 mb Log[\[Kappa]])/(9 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(4 g1 g2^3 mb \[Kappa] Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(4 g1 g2^3 mb \[Kappa]^2 Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4);*)
(*FDgammap1Sophie=(11 g1 g2^3 mb \[Kappa])/(27 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(2 g1 g2^3 mb \[Kappa]^2)/(3 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(g1 g2^3 mb \[Kappa]^3)/(3 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(2 g1 g2^3 mb \[Kappa]^4)/(27 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(2 g1 g2^3 mb \[Kappa] Log[\[Kappa]])/(9 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4);*)
(*FDgammap2Sophie=-((19 g1 g2^3 mb \[Kappa])/(27 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4))+(3 g1 g2^3 mb \[Kappa]^2)/(2 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^3)/(Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(11 g1 g2^3 mb \[Kappa]^4)/(54 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(4 g1 g2^3 mb \[Kappa] Log[\[Kappa]])/(9 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(g1 g2^3 mb \[Kappa]^2 Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4);*)
(*FEgammap1Sophie=(g1 g2^3 mb)/(36 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa])/(4 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(17 g1 g2^3 mb \[Kappa]^3)/(36 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^3 Log[\[Kappa]])/(6 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4);*)
(*FEgammap2Sophie=(11 g1 g2^3 mb)/(18 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(3 g1 g2^3 mb \[Kappa])/(Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(9 g1 g2^3 mb \[Kappa]^2)/(2 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(19 g1 g2^3 mb \[Kappa]^3)/(9 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(4 g1 g2^3 mb \[Kappa]^3 Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4);*)
(*FFgammap1Sophie=0;*)
(*FFgammap2Sophie=-((g1 g2^3 mb)/(4 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^3))+(g1 g2^3 mb \[Kappa])/(Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^3)-(3 g1 g2^3 mb \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^3)+(g1 g2^3 mb \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^3);*)
(*FGgammap1Sophie=0;*)
(*FGgammap2Sophie=0;*)
(*FHgammap1Sophie=-((17 g1 g2^3 mb \[Kappa])/(72 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4))+(g1 g2^3 mb \[Kappa]^2)/(8 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(g1 g2^3 mb \[Kappa]^3)/(8 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^4)/(72 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(g1 g2^3 mb \[Kappa]^3 Log[\[Kappa]])/(4 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^4 Log[\[Kappa]])/(12 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4);*)
(*FHgammap2Sophie=(13 g1 g2^3 mb \[Kappa])/(72 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(g1 g2^3 mb \[Kappa]^2)/(8 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(5 g1 g2^3 mb \[Kappa]^3)/(8 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(23 g1 g2^3 mb \[Kappa]^4)/(72 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)+(g1 g2^3 mb \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^3 Log[\[Kappa]])/(4 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4)-(g1 g2^3 mb \[Kappa]^4 Log[\[Kappa]])/(12 Sqrt[g1^2+g2^2] mW^2 (-1+\[Kappa])^4);*)
(*FCgammaSophie=FCgammap1Sophie*p1eps+FCgammap2Sophie*p2eps;*)
(*FDgammaSophie=FDgammap1Sophie*p1eps+FDgammap2Sophie*p2eps;*)
(*FEgammaSophie=FEgammap1Sophie*p1eps+FEgammap2Sophie*p2eps;*)
(*FFgammaSophie=FFgammap1Sophie*p1eps+FFgammap2Sophie*p2eps;*)
(*FGgammaSophie=FGgammap1Sophie*p1eps+FGgammap2Sophie*p2eps;*)
(*FHgammaSophie=FHgammap1Sophie*p1eps+FHgammap2Sophie*p2eps;*)
(*Sophiegam={0,0,0,0,FCgammaSophie,FDgammaSophie,FEgammaSophie,FFgammaSophie,FGgammaSophie,FHgammaSophie}//Expand;*)
(**)


(* ::Input:: *)
(*(*compare dim 4 pieces*)*)
(*FORMalld4FeynGauge=Join[FORMZd4FeynGauge,FORMboxd4FeynGauge,FORMgamd4FeynGauge];*)
(*Sophiealld4=Join[SophieZ,Sophiebox,Sophiegam];*)
(*alld4match=(#[[1]]==#[[2]])===True&/@ Transpose[{FORMalld4FeynGauge,Sophiealld4}]*)
(*Grid[Transpose[{Join[diagnamesZ,diagnamesbox,diagnamesgam],alld4match,FORMalld4FeynGauge,Sophiealld4}],Frame->All]*)


(*Sophie's dim 6 results for Z penguins, not parameter shifts*)
(*everything here has been pre-multiplied by (2mW^2/(g2^4))(g2^2/(4mW^2))*)
C10CHuSophieGIM=(7 \[Kappa])/(32 (-1+\[Kappa])^2)-\[Kappa]^2/(4 (-1+\[Kappa])^2)+\[Kappa]^3/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);
C9CHuSophieGIM=(21 g1^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(7 g2^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^2)/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^2)/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2);
C10CHqSophieGIM=-((7 \[Kappa])/(32 (-1+\[Kappa])^2))+\[Kappa]^2/(4 (-1+\[Kappa])^2)-\[Kappa]^3/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);
C9CHqSophieGIM=-((21 g1^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2))+(7 g2^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2)/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2)/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2);
C10CHdSophieGIM=0;
C9CHdSophieGIM=0;
C10CHq3SophieGIM=(49 \[Kappa])/(32 (-1+\[Kappa])^2)-(2 \[Kappa]^2)/(-1+\[Kappa])^2+(15 \[Kappa]^3)/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(3 \[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);
C9CHq3SophieGIM=(147 g1^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(49 g2^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(6 g1^2 \[Kappa]^2)/((g1^2+g2^2) (-1+\[Kappa])^2)+(2 g2^2 \[Kappa]^2)/((g1^2+g2^2) (-1+\[Kappa])^2)+(45 g1^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(15 g2^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(9 g1^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g2^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(9 g1^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g2^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2);
C10CHDSophieGIM=(17 \[Kappa])/(64 (-1+\[Kappa])^2)-(5 \[Kappa]^2)/(16 (-1+\[Kappa])^2)+(3 \[Kappa]^3)/(64 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(16 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(16 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(32 (-1+\[Kappa])^2);
C9CHDSophieGIM=(51 g1^2 \[Kappa])/(64 (g1^2+g2^2) (-1+\[Kappa])^2)-(17 g2^2 \[Kappa])/(64 (g1^2+g2^2) (-1+\[Kappa])^2)-(15 g1^2 \[Kappa]^2)/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(5 g2^2 \[Kappa]^2)/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(9 g1^2 \[Kappa]^3)/(64 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g2^2 \[Kappa]^3)/(64 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa] Log[mW])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa] Log[mW])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[mW])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[mW])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[\[Kappa]])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[\[Kappa]])/(32 (g1^2+g2^2) (-1+\[Kappa])^2);
C10CHl3SophieGIM=(3 \[Kappa])/(4 (-1+\[Kappa])^2)-(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)+\[Kappa]^3/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);
C9CHl3SophieGIM=-((3 \[Kappa])/(4 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-\[Kappa]^3/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);
C10CHeSophieGIM=-((3 \[Kappa])/(4 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-\[Kappa]^3/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);
C9CHeSophieGIM=-((3 \[Kappa])/(4 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-\[Kappa]^3/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);
C10CHl1SophieGIM=-((3 \[Kappa])/(4 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-\[Kappa]^3/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);
C9CHl1SophieGIM=(3 \[Kappa])/(4 (-1+\[Kappa])^2)-(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)+\[Kappa]^3/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);



(* ::Input:: *)
(*(*Sophies non gimmed dim 6 results for Z penguins...*)*)
(*(*everything here has been pre-multiplied by (2mW^2/(g2^4))(g2^2/(4mW^2))*)*)
(*C10CHuSophie=(7 \[Kappa])/(32 (-1+\[Kappa])^2)-\[Kappa]^2/(4 (-1+\[Kappa])^2)+\[Kappa]^3/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9CHuSophie=(21 g1^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(7 g2^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^2)/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^2)/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2);*)
(*C10CHq1Sophie=-((7 \[Kappa])/(32 (-1+\[Kappa])^2))+\[Kappa]^2/(4 (-1+\[Kappa])^2)-\[Kappa]^3/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9CHq1Sophie=-((21 g1^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2))+(7 g2^2 \[Kappa])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2)/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2)/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3)/(32 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2) (-1+\[Kappa])^2);*)
(*C10CHdSophie=0;*)
(*C9CHdSophie=0;*)
(*C10CHWBSophie=newshiftdum*(-((g1 g2)/(8 (g1^2+g2^2) (-1+\[Kappa])^2))-(g1 g2 \[Kappa])/(2 (g1^2+g2^2) (-1+\[Kappa])^2)+(5 g1 g2 \[Kappa]^2)/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g1 g2 Log[mW])/(2 (g1^2+g2^2) (-1+\[Kappa])^2)+(g1 g2 \[Kappa] Log[mW])/((g1^2+g2^2) (-1+\[Kappa])^2)-(g1 g2 \[Kappa]^2 Log[mW])/(2 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1 g2 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2));*)
(*C9CHWBSophie=newshiftdum*(-((3 g1^3 g2)/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2))+(g1 g2^3)/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(9 g1^3 g2 \[Kappa])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(5 g1 g2^3 \[Kappa])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(43 g1^3 g2 \[Kappa]^2)/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(23 g1 g2^3 \[Kappa]^2)/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g1^3 g2 \[Kappa]^3)/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g1 g2^3 \[Kappa]^3)/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^3 g2 Log[mW])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(7 g1 g2^3 Log[mW])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g1^3 g2 \[Kappa] Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(7 g1 g2^3 \[Kappa] Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^3 g2 \[Kappa]^2 Log[mW])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(7 g1 g2^3 \[Kappa]^2 Log[mW])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g1^3 g2 \[Kappa] Log[\[Kappa]])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g1 g2^3 \[Kappa] Log[\[Kappa]])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(15 g1^3 g2 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1 g2^3 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2));*)
(*C10CWSophie=0;*)
(*C9CWSophie=0;*)
(*C10CHq3Sophie=-(1/(8 (-1+\[Kappa])^2))+(57 \[Kappa])/(32 (-1+\[Kappa])^2)-(17 \[Kappa]^2)/(8 (-1+\[Kappa])^2)+(15 \[Kappa]^3)/(32 (-1+\[Kappa])^2)-(g1^2 Log[mW])/(2 (g1^2+g2^2))+(3 g2^2 Log[mW])/(2 (g1^2+g2^2))-(g1^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2))-(g2^2 \[Kappa] Log[mW])/(8 (g1^2+g2^2))+(3 \[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9CHq3Sophie=-((3 g1^4)/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2))-(g1^2 g2^2)/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+g2^4/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(171 g1^4 \[Kappa])/(32 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(57 g1^2 g2^2 \[Kappa])/(16 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(57 g2^4 \[Kappa])/(32 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(51 g1^4 \[Kappa]^2)/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(17 g1^2 g2^2 \[Kappa]^2)/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(17 g2^4 \[Kappa]^2)/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(45 g1^4 \[Kappa]^3)/(32 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(15 g1^2 g2^2 \[Kappa]^3)/(16 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(15 g2^4 \[Kappa]^3)/(32 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^4 Log[mW])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(5 g1^2 g2^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g2^4 Log[mW])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(21 g1^4 \[Kappa] Log[mW])/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(41 g1^2 g2^2 \[Kappa] Log[mW])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(25 g2^4 \[Kappa] Log[mW])/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^4 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(11 g1^2 g2^2 \[Kappa]^2 Log[mW])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(7 g2^4 \[Kappa]^2 Log[mW])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^4 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^3 Log[mW])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(g2^4 \[Kappa]^3 Log[mW])/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(9 g1^4 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g1^2 g2^2 \[Kappa] Log[\[Kappa]])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g2^4 \[Kappa] Log[\[Kappa]])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(9 g1^4 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g2^4 \[Kappa]^2 Log[\[Kappa]])/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^4 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^3 Log[\[Kappa]])/(8 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(g2^4 \[Kappa]^3 Log[\[Kappa]])/(16 (g1^2+g2^2)^2 (-1+\[Kappa])^2);*)
(*C10CHDSophie=(17 \[Kappa])/(64 (-1+\[Kappa])^2)-(5 \[Kappa]^2)/(16 (-1+\[Kappa])^2)+(3 \[Kappa]^3)/(64 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(16 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(16 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(32 (-1+\[Kappa])^2)+newshiftdum*(-((3 \[Kappa])/(8 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(16 (-1+\[Kappa])^2)-\[Kappa]^3/(16 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(16 (-1+\[Kappa])^2));*)
(*C9CHDSophie=(51 g1^2 \[Kappa])/(64 (g1^2+g2^2) (-1+\[Kappa])^2)-(17 g2^2 \[Kappa])/(64 (g1^2+g2^2) (-1+\[Kappa])^2)-(15 g1^2 \[Kappa]^2)/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(5 g2^2 \[Kappa]^2)/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(9 g1^2 \[Kappa]^3)/(64 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g2^2 \[Kappa]^3)/(64 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa] Log[mW])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa] Log[mW])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[mW])/(8 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[mW])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[mW])/(16 (g1^2+g2^2) (-1+\[Kappa])^2)+(3 g1^2 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(g2^2 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2) (-1+\[Kappa])^2)-(3 g1^2 \[Kappa]^3 Log[\[Kappa]])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+(g2^2 \[Kappa]^3 Log[\[Kappa]])/(32 (g1^2+g2^2) (-1+\[Kappa])^2)+newshiftdum*(-((9 \[Kappa])/(8 (-1+\[Kappa])^2))+(21 \[Kappa]^2)/(16 (-1+\[Kappa])^2)-(3 \[Kappa]^3)/(16 (-1+\[Kappa])^2)-(2 g2^4 Log[mW])/(g1^2+g2^2)^2-(3 \[Kappa] Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(9 \[Kappa]^2 Log[\[Kappa]])/(16 (-1+\[Kappa])^2));*)
(*C10CHl3Sophie=(3 \[Kappa])/(4 (-1+\[Kappa])^2)-(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)+\[Kappa]^3/(8 (-1+\[Kappa])^2)+(g2^2 Log[mW])/(g1^2+g2^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+newshiftdum*(-((3 \[Kappa])/(-1+\[Kappa])^2)+(7 \[Kappa]^2)/(2 (-1+\[Kappa])^2)-\[Kappa]^3/(2 (-1+\[Kappa])^2)-(4 g2^2 Log[mW])/(g1^2+g2^2)-(\[Kappa] Log[\[Kappa]])/(-1+\[Kappa])^2-(3 \[Kappa]^2 Log[\[Kappa]])/(2 (-1+\[Kappa])^2));*)
(*C9CHl3Sophie=-((3 \[Kappa])/(4 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-\[Kappa]^3/(8 (-1+\[Kappa])^2)-(g2^2 Log[mW])/(g1^2+g2^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+newshiftdum*(-((9 g1^4 \[Kappa])/((g1^2+g2^2)^2 (-1+\[Kappa])^2))-(6 g1^2 g2^2 \[Kappa])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g2^4 \[Kappa])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(21 g1^4 \[Kappa]^2)/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(7 g1^2 g2^2 \[Kappa]^2)/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(7 g2^4 \[Kappa]^2)/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^4 \[Kappa]^3)/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g1^2 g2^2 \[Kappa]^3)/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(g2^4 \[Kappa]^3)/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(12 g1^2 g2^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(4 g2^4 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(24 g1^2 g2^2 \[Kappa] Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(8 g2^4 \[Kappa] Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(12 g1^2 g2^2 \[Kappa]^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(4 g2^4 \[Kappa]^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^4 \[Kappa] Log[\[Kappa]])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(2 g1^2 g2^2 \[Kappa] Log[\[Kappa]])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(g2^4 \[Kappa] Log[\[Kappa]])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(9 g1^4 \[Kappa]^2 Log[\[Kappa]])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g2^4 \[Kappa]^2 Log[\[Kappa]])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2));*)
(*C10CHeSophie=-((3 \[Kappa])/(4 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-\[Kappa]^3/(8 (-1+\[Kappa])^2)-(g2^2 Log[mW])/(g1^2+g2^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);*)
(*C9CHeSophie=-((3 \[Kappa])/(4 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-\[Kappa]^3/(8 (-1+\[Kappa])^2)-(g2^2 Log[mW])/(g1^2+g2^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);*)
(*C10CHl1Sophie=(3 \[Kappa])/(4 (-1+\[Kappa])^2)-(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)+\[Kappa]^3/(8 (-1+\[Kappa])^2)+(g2^2 Log[mW])/(g1^2+g2^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);*)
(*C9CHl1Sophie=-((3 \[Kappa])/(4 (-1+\[Kappa])^2))+(7 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-\[Kappa]^3/(8 (-1+\[Kappa])^2)-(g2^2 Log[mW])/(g1^2+g2^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2);*)
(*C10CllSophie=newshiftdum*((3 \[Kappa])/(2 (-1+\[Kappa])^2)-(7 \[Kappa]^2)/(4 (-1+\[Kappa])^2)+\[Kappa]^3/(4 (-1+\[Kappa])^2)+(2 g2^2 Log[mW])/(g1^2+g2^2)+(\[Kappa] Log[\[Kappa]])/(2 (-1+\[Kappa])^2)+(3 \[Kappa]^2 Log[\[Kappa]])/(4 (-1+\[Kappa])^2));*)
(*C9CllSophie=newshiftdum*((9 g1^4 \[Kappa])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g1^2 g2^2 \[Kappa])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g2^4 \[Kappa])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(21 g1^4 \[Kappa]^2)/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(7 g1^2 g2^2 \[Kappa]^2)/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(7 g2^4 \[Kappa]^2)/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g1^4 \[Kappa]^3)/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa]^3)/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g2^4 \[Kappa]^3)/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(6 g1^2 g2^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(2 g2^4 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(12 g1^2 g2^2 \[Kappa] Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(4 g2^4 \[Kappa] Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(6 g1^2 g2^2 \[Kappa]^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(2 g2^4 \[Kappa]^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g1^4 \[Kappa] Log[\[Kappa]])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(g1^2 g2^2 \[Kappa] Log[\[Kappa]])/((g1^2+g2^2)^2 (-1+\[Kappa])^2)-(g2^4 \[Kappa] Log[\[Kappa]])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(9 g1^4 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2)+(3 g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])^2)-(3 g2^4 \[Kappa]^2 Log[\[Kappa]])/(4 (g1^2+g2^2)^2 (-1+\[Kappa])^2));*)
(*listOfC9C10={*)
(*{C9CHl1Sophie,C10CHl1Sophie},*)
(*{C9CHl3Sophie,C10CHl3Sophie},*)
(*{C9CHeSophie,C10CHeSophie},*)
(*{C9CHq1Sophie,C10CHq1Sophie},*)
(*{C9CHuSophie,C10CHuSophie},*)
(*{C9CHq3Sophie,C10CHq3Sophie},*)
(*{C9CHdSophie,C10CHdSophie},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{C9CllSophie,C10CllSophie},*)
(*{C9CWSophie,C10CWSophie},*)
(*{C9CHWBSophie,C10CHWBSophie},*)
(*{C9CHDSophie,C10CHDSophie},*)
(*{0,0},*)
(*{0,0}*)
(*};*)
(*SophieZd6bits={Sophiecphil1,Sophiecphil3,Sophiecphie,Sophiecphiq1,Sophiecphiu,Sophiecphiq3,Sophiecphid,Sophieclq1,Sophieclq3,Sophieceu,Sophieclu,Sophiecqe,Sophiecll,SophiecW,SophiecHWB,SophiecHD};*)
(*SophieZd6bits=(-1)*(*NOTE: overall minus sign due to Z propagator*) 2*g2^2*(quarkL*#[[1]]*(lepL+lepR)+quarkL*#[[2]]*(-lepL+lepR))&/@listOfC9C10//Simplify//Expand;*)


(* ::Input:: *)
(*(*Sophie's dim 6 results for b\[Rule]s gamma*)*)
(*F2CHq3Sophie=-((46 g1 g2)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4))+(205 g1 g2 \[Kappa])/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(104 g1 g2 \[Kappa]^2)/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(175 g1 g2 \[Kappa]^3)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(22 g1 g2 \[Kappa]^4)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(4 g1 g2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(6 g1 g2 \[Kappa]^3 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4);*)
(*F1CHq3Sophie=(104 g1 g2)/(27 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(416 g1 g2 \[Kappa])/(27 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(61 g1 g2 \[Kappa]^2)/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(284 g1 g2 \[Kappa]^3)/(27 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(47 g1 g2 \[Kappa]^4)/(27 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(16 g1 g2 Log[\[Kappa]])/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(64 g1 g2 \[Kappa] Log[\[Kappa]])/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(12 g1 g2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(20 g1 g2 \[Kappa]^3 Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(2 g1 g2 \[Kappa]^4 Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4);*)
(*F2CWSophie=1/(2 (-1+\[Kappa])^3)-(2 \[Kappa])/(-1+\[Kappa])^3+(3 \[Kappa]^2)/(2 (-1+\[Kappa])^3)-(\[Kappa]^2 Log[\[Kappa]])/(-1+\[Kappa])^3;*)
(*F1CWSophie=-(1/(-1+\[Kappa])^3)+(5 \[Kappa])/(2 (-1+\[Kappa])^3)-\[Kappa]^2/(-1+\[Kappa])^3-\[Kappa]^3/(2 (-1+\[Kappa])^3)-(2 Log[mW])/(-1+\[Kappa])^3+(6 \[Kappa] Log[mW])/(-1+\[Kappa])^3-(6 \[Kappa]^2 Log[mW])/(-1+\[Kappa])^3+(2 \[Kappa]^3 Log[mW])/(-1+\[Kappa])^3+(\[Kappa]^3 Log[\[Kappa]])/(-1+\[Kappa])^3;*)
(*F1CHWBSophie=(3 g2^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(3 g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(2 g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(9 g2^2 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(2 g2^2 \[Kappa]^2)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(3 g2^2 \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+newshiftdum*(-((35 g2^2)/(54 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4))+(65 g2^2 \[Kappa])/(54 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(5 g2^2 \[Kappa]^2)/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(107 g2^2 \[Kappa]^3)/(54 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(47 g2^2 \[Kappa]^4)/(54 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(8 g2^2 Log[\[Kappa]])/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(32 g2^2 \[Kappa] Log[\[Kappa]])/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(3 g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(g2^2 \[Kappa]^3 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(g2^2 \[Kappa]^4 Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4));*)
(*F2CHWBSophie=-(g2^2/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3))-g2^2/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(2 g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(3 g2^2 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(3 g2^2 \[Kappa]^2)/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(2 g2^2 Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)-(4 g2^2 \[Kappa] Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(2 g2^2 \[Kappa]^2 Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+(g2^2 \[Kappa]^2 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^2)+newshiftdum*((25 g2^2)/(18 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(53 g2^2 \[Kappa])/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(47 g2^2 \[Kappa]^2)/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(41 g2^2 \[Kappa]^3)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(11 g2^2 \[Kappa]^4)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4));*)
(*F1CllSophie=newshiftdum*((26 g1 g2)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(104 g1 g2 \[Kappa])/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(61 g1 g2 \[Kappa]^2)/(4 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(71 g1 g2 \[Kappa]^3)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(47 g1 g2 \[Kappa]^4)/(36 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(4 g1 g2 Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(16 g1 g2 \[Kappa] Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(9 g1 g2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(5 g1 g2 \[Kappa]^3 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(g1 g2 \[Kappa]^4 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4));*)
(*F2CllSophie=newshiftdum*(-((23 g1 g2)/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4))+(205 g1 g2 \[Kappa])/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(26 g1 g2 \[Kappa]^2)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(175 g1 g2 \[Kappa]^3)/(12 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(11 g1 g2 \[Kappa]^4)/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(3 g1 g2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(9 g1 g2 \[Kappa]^3 Log[\[Kappa]])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4));*)
(*F1CHDSophie=newshiftdum*(-((13 g2^3)/(27 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4))+(52 g2^3 \[Kappa])/(27 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(61 g2^3 \[Kappa]^2)/(24 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(71 g2^3 \[Kappa]^3)/(54 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(47 g2^3 \[Kappa]^4)/(216 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(2 g2^3 Log[\[Kappa]])/(9 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(8 g2^3 \[Kappa] Log[\[Kappa]])/(9 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(3 g2^3 \[Kappa]^2 Log[\[Kappa]])/(2 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(5 g2^3 \[Kappa]^3 Log[\[Kappa]])/(6 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(g2^3 \[Kappa]^4 Log[\[Kappa]])/(12 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4));*)
(*F2CHDSophie=newshiftdum*((23 g2^3)/(36 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(205 g2^3 \[Kappa])/(72 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(13 g2^3 \[Kappa]^2)/(3 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(175 g2^3 \[Kappa]^3)/(72 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(11 g2^3 \[Kappa]^4)/(36 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(g2^3 \[Kappa]^2 Log[\[Kappa]])/(2 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(3 g2^3 \[Kappa]^3 Log[\[Kappa]])/(4 g1 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4));*)
(*F1CHl3Sophie=newshiftdum*(-((52 g1 g2)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4))+(208 g1 g2 \[Kappa])/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(61 g1 g2 \[Kappa]^2)/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(142 g1 g2 \[Kappa]^3)/(9 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(47 g1 g2 \[Kappa]^4)/(18 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(8 g1 g2 Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(32 g1 g2 \[Kappa] Log[\[Kappa]])/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(18 g1 g2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(10 g1 g2 \[Kappa]^3 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(g1 g2 \[Kappa]^4 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4));*)
(*F2CHl3Sophie=newshiftdum*((23 g1 g2)/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(205 g1 g2 \[Kappa])/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(52 g1 g2 \[Kappa]^2)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(175 g1 g2 \[Kappa]^3)/(6 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(11 g1 g2 \[Kappa]^4)/(3 Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)-(6 g1 g2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4)+(9 g1 g2 \[Kappa]^3 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^4));*)
(**)
(*listofF1F2={*)
(*{0,0},*)
(*{F1CHl3Sophie,F2CHl3Sophie},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{F1CHq3Sophie,F2CHq3Sophie},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{F1CllSophie,F2CllSophie},*)
(*{F1CWSophie,F2CWSophie},*)
(*{F1CHWBSophie,F2CHWBSophie},*)
(*{F1CHDSophie,F2CHDSophie},*)
(*{0,0},*)
(*{0,0}*)
(*};*)
(*Sophiegamd6bits= (p1eps*(#[[1]]+#[[2]])*mb+p2eps*(#[[1]]-#[[2]])*mb)&/@listofF1F2//Simplify//Expand;*)


(* ::Input:: *)
(*(*Sophie's dim 6 results for boxes*)*)
(*FboxCHq3Sophie=-((2 g2^2)/(-1+\[Kappa])^2)+(2 g2^2 \[Kappa])/(-1+\[Kappa])^2-(2 g2^2 \[Kappa] Log[\[Kappa]])/(-1+\[Kappa])^2;*)
(*FboxCHl3Sophie=-((2 g2^2)/(-1+\[Kappa])^2)+(2 g2^2 \[Kappa])/(-1+\[Kappa])^2-(2 g2^2 \[Kappa] Log[\[Kappa]])/(-1+\[Kappa])^2+newshiftdum*((4 g2^2)/(-1+\[Kappa])^2-(4 g2^2 \[Kappa])/(-1+\[Kappa])^2+(4 g2^2 \[Kappa] Log[\[Kappa]])/(-1+\[Kappa])^2);*)
(*FboxCllSophie=newshiftdum*(-((2 g2^2)/(-1+\[Kappa])^2)+(2 g2^2 \[Kappa])/(-1+\[Kappa])^2-(2 g2^2 \[Kappa] Log[\[Kappa]])/(-1+\[Kappa])^2);*)
(*Sophieboxd6bits=quarkL*lepL*{0,FboxCHl3Sophie,0,0,0,FboxCHq3Sophie,0,0,0,0,0,0,FboxCllSophie,0,0,0,0,0}//Simplify//Expand;*)


(* ::Input:: *)
(*FORMZd6FeynGaugebits=((Coefficient[FORMZd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;*)
(*FORMgamd6FeynGaugebits=((Coefficient[FORMgamd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;*)
(*FORMboxd6FeynGaugebits=((Coefficient[FORMboxd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;*)
(*FORMnewd6FeynGaugebits=((Coefficient[FORMnewd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;*)


(* ::Input:: *)
(*boxd6match=(#[[1]]==#[[2]])===True&/@ Transpose[{FORMboxd6FeynGaugebits,Sophieboxd6bits}]*)
(*Grid[Transpose[{wilsonCoeffs,boxd6match,FORMboxd6FeynGaugebits,Sophieboxd6bits}],Frame->All]*)


(* ::Input:: *)
(*gamd6match=(#[[1]]==#[[2]])===True&/@ Transpose[{FORMgamd6FeynGaugebits,Sophiegamd6bits}]*)
(*Grid[Transpose[{wilsonCoeffs,gamd6match,FORMgamd6FeynGaugebits,Sophiegamd6bits}],Frame->All]*)


FORMgamd6FeynGaugebits[[-2]]//DSimplify//Expand


{mb,g1,g2,\[Kappa]}=Table[RandomReal[],{i,1,4}]
FORMgamd6FeynGaugebits[[-2]]
Sophiegamd6bits[[-2]]
{mb,g1,g2,\[Kappa]}=.


(* ::Input:: *)
(*Zd6match=(#[[1]]==#[[2]])===True&/@ Transpose[{FORMZd6FeynGaugebits/.d6dum->1,SophieZd6bits}]*)
(*Grid[Transpose[{wilsonCoeffs,Zd6match,FORMZd6FeynGaugebits/.d6dum->1,SophieZd6bits}],Frame->All]*)


(* ::Input:: *)
(*(*Sophies HWB pieces*)*)
(*FfHWBF1Sophie=(g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(2 g2^2 \[Kappa]^2)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(g2^2 \[Kappa]^3)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(g2^2 \[Kappa]^3 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3);*)
(*FfHWBF2Sophie=g2^2/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(3 g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(7 g2^2 \[Kappa]^2)/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(g2^2 \[Kappa]^3)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(2 g2^2 Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(6 g2^2 \[Kappa] Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(6 g2^2 \[Kappa]^2 Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(2 g2^2 \[Kappa]^3 Log[mW])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(g2^2 \[Kappa]^3 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3);*)
(*FgHWBF1Sophie=(g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(2 g2^2 \[Kappa]^2)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(g2^2 \[Kappa]^3)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(g2^2 \[Kappa]^3 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3);*)
(*FgHWBF2Sophie=(g2^2 \[Kappa])/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(g2^2 \[Kappa]^3)/(2 Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3);*)
(*FeHWBF1Sophie=(3 g2^2)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(12 g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(9 g2^2 \[Kappa]^2)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(6 g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3);*)
(*FeHWBF2Sophie=-(g2^2/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3))+(4 g2^2 \[Kappa])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)-(3 g2^2 \[Kappa]^2)/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3)+(2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(Sqrt[g1^2+g2^2] (-1+\[Kappa])^3);*)
(*listofHWBF1F2={*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{FeHWBF1Sophie,FeHWBF2Sophie},*)
(*{FfHWBF1Sophie,FfHWBF2Sophie},*)
(*{FgHWBF1Sophie,FgHWBF2Sophie},*)
(*{0,0}*)
(*};*)
(*SophiecHWBbits= (p1eps*(#[[1]]+#[[2]])*mb+p2eps*(#[[1]]-#[[2]])*mb)&/@listofHWBF1F2//Simplify//Expand;*)


(* ::Input:: *)
(*(*Sophies new diagram results*)*)
(*C10CeuSophie=-((7 \[Kappa])/(32 (-1+\[Kappa])^2))+\[Kappa]^2/(4 (-1+\[Kappa])^2)-\[Kappa]^3/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9CeuSophie=-((7 \[Kappa])/(32 (-1+\[Kappa])^2))+\[Kappa]^2/(4 (-1+\[Kappa])^2)-\[Kappa]^3/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C10CluSophie=(7 \[Kappa])/(32 (-1+\[Kappa])^2)-\[Kappa]^2/(4 (-1+\[Kappa])^2)+\[Kappa]^3/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9CluSophie=-((7 \[Kappa])/(32 (-1+\[Kappa])^2))+\[Kappa]^2/(4 (-1+\[Kappa])^2)-\[Kappa]^3/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C10Clq3Sophie=-(1/(8 (-1+\[Kappa])^2))+(9 \[Kappa])/(32 (-1+\[Kappa])^2)-(3 \[Kappa]^2)/(8 (-1+\[Kappa])^2)+(7 \[Kappa]^3)/(32 (-1+\[Kappa])^2)-Log[mW]/(2 (-1+\[Kappa])^2)+(7 \[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9Clq3Sophie=1/(8 (-1+\[Kappa])^2)-(9 \[Kappa])/(32 (-1+\[Kappa])^2)+(3 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-(7 \[Kappa]^3)/(32 (-1+\[Kappa])^2)+Log[mW]/(2 (-1+\[Kappa])^2)-(7 \[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(3 \[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C10CqeSophie=(7 \[Kappa])/(32 (-1+\[Kappa])^2)-\[Kappa]^2/(4 (-1+\[Kappa])^2)+\[Kappa]^3/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9CqeSophie=(7 \[Kappa])/(32 (-1+\[Kappa])^2)-\[Kappa]^2/(4 (-1+\[Kappa])^2)+\[Kappa]^3/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C10Clq1Sophie=-((7 \[Kappa])/(32 (-1+\[Kappa])^2))+\[Kappa]^2/(4 (-1+\[Kappa])^2)-\[Kappa]^3/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9Clq1Sophie=(7 \[Kappa])/(32 (-1+\[Kappa])^2)-\[Kappa]^2/(4 (-1+\[Kappa])^2)+\[Kappa]^3/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa] Log[\[Kappa]])/(4 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(8 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9extraCHl1Sophie=-((17 \[Kappa])/(32 (-1+\[Kappa])^2))+(5 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-(3 \[Kappa]^3)/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(2 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C10extraCHl1Sophie=(17 \[Kappa])/(32 (-1+\[Kappa])^2)-(5 \[Kappa]^2)/(8 (-1+\[Kappa])^2)+(3 \[Kappa]^3)/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[\[Kappa]])/(2 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9extraCHeSophie=-((17 \[Kappa])/(32 (-1+\[Kappa])^2))+(5 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-(3 \[Kappa]^3)/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(2 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C10extraCHeSophie=-((17 \[Kappa])/(32 (-1+\[Kappa])^2))+(5 \[Kappa]^2)/(8 (-1+\[Kappa])^2)-(3 \[Kappa]^3)/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[\[Kappa]])/(2 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2);*)
(*C9extraCHl3Sophie=(\[Kappa]/(32 (-1+\[Kappa])^2)-\[Kappa]^2/(8 (-1+\[Kappa])^2)+(3 \[Kappa]^3)/(32 (-1+\[Kappa])^2)-(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2));*)
(*C10extraCHl3Sophie=(-(\[Kappa]/(32 (-1+\[Kappa])^2))+\[Kappa]^2/(8 (-1+\[Kappa])^2)-(3 \[Kappa]^3)/(32 (-1+\[Kappa])^2)+(\[Kappa] Log[mW])/(8 (-1+\[Kappa])^2)-(\[Kappa]^2 Log[mW])/(4 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[mW])/(8 (-1+\[Kappa])^2)+(\[Kappa]^3 Log[\[Kappa]])/(16 (-1+\[Kappa])^2));*)
(*C10extraCHq3Sophie=(-1)((g1^2 \[Kappa])/(2 (g1^2+g2^2) (-1+\[Kappa]))-(g1^2 \[Kappa]^2)/(2 (g1^2+g2^2) (-1+\[Kappa]))-(g1^2 \[Kappa] Log[mW])/((g1^2+g2^2) (-1+\[Kappa]))+(g1^2 \[Kappa]^2 Log[mW])/((g1^2+g2^2) (-1+\[Kappa]))+(g1^2 \[Kappa]^2 Log[\[Kappa]])/(2 (g1^2+g2^2) (-1+\[Kappa])));*)
(*C9extraCHq3Sophie=(-1)((3 g1^4 \[Kappa])/(2 (g1^2+g2^2)^2 (-1+\[Kappa]))-(g1^2 g2^2 \[Kappa])/(2 (g1^2+g2^2)^2 (-1+\[Kappa]))-(3 g1^4 \[Kappa]^2)/(2 (g1^2+g2^2)^2 (-1+\[Kappa]))+(g1^2 g2^2 \[Kappa]^2)/(2 (g1^2+g2^2)^2 (-1+\[Kappa]))-(3 g1^4 \[Kappa] Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa]))+(g1^2 g2^2 \[Kappa] Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa]))+(3 g1^4 \[Kappa]^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa]))-(g1^2 g2^2 \[Kappa]^2 Log[mW])/((g1^2+g2^2)^2 (-1+\[Kappa]))+(3 g1^4 \[Kappa]^2 Log[\[Kappa]])/(2 (g1^2+g2^2)^2 (-1+\[Kappa]))-(g1^2 g2^2 \[Kappa]^2 Log[\[Kappa]])/(2 (g1^2+g2^2)^2 (-1+\[Kappa])));*)
(*listOfC9C10={*)
(*{C9extraCHl1Sophie,C10extraCHl1Sophie},*)
(*{C9extraCHl3Sophie,C10extraCHl3Sophie},*)
(*{C9extraCHeSophie,C10extraCHeSophie},*)
(*{0,0},*)
(*{0,0},*)
(*{C9extraCHq3Sophie,C10extraCHq3Sophie},*)
(*{0,0},*)
(*{C9Clq1Sophie,C10Clq1Sophie},*)
(*{C9Clq3Sophie,C10Clq3Sophie},*)
(*{C9CeuSophie,C10CeuSophie},*)
(*{C9CluSophie,C10CluSophie},*)
(*{C9CqeSophie,C10CqeSophie},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0},*)
(*{0,0}*)
(*};*)
(*Sophienewd6bits= 2*g2^2*(quarkL*#[[1]]*(lepL+lepR)+quarkL*#[[2]]*(-lepL+lepR))&/@listOfC9C10//Simplify//Expand;*)
(**)
(**)
(**)


Gj+Gk


(* ::Input:: *)
(*newd6match=(#[[1]]==#[[2]])===True&/@ Transpose[{FORMnewd6FeynGaugebits,Sophienewd6bits}]*)
(*Grid[Transpose[{wilsonCoeffs,newd6match,FORMnewd6FeynGaugebits,Sophienewd6bits}],Frame->All]*)


(* ::Input:: *)
(*(*Checking SM values of Wilson coeffs*)*)
(*TotalSMZ=Total[FORMZd4FeynGauge];*)
(*TotalSMbox=Total[FORMboxd4FeynGauge];*)
(*TotalSMgam=Total[FORMgamd4FeynGauge];*)
(*SMC10Zresult=((Coefficient[TotalSMZ,lepL]-Coefficient[TotalSMZ,lepR])*(-mW^2/g2^4/quarkL))//Expand//Simplify//Expand;*)
(*SMC10boxresult=((Coefficient[TotalSMbox,lepL]-Coefficient[TotalSMbox,lepR])*(-mW^2/g2^4/quarkL))//Expand//Simplify//Expand;*)
(*SMGIMMEDC10Zresult=SMC10Zresult-Limit[SMC10Zresult,\[Kappa]->0];*)
(*SMGIMMEDC10totalresult=SMC10Zresult+SMC10boxresult-Limit[SMC10Zresult+SMC10boxresult,\[Kappa]->0]//Simplify*)
(*SMC7result=1/2((Coefficient[TotalSMgam,p1eps]-Coefficient[TotalSMgam,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))//Simplify;*)
(*SMGIMMEDC7result=SMC7result-Limit[SMC7result,\[Kappa]->0]//Simplify*)
(*(*Total C9 result = 1/s^2 (SMC9Zresult+SMC9boxresult) + SMC9gamresult *)*)
(*(*NB not yet gimmed because C9 gamma causes problems*)*)
(*SMC9gamresult=((Coefficient[TotalSMgam,p1eps]+Coefficient[TotalSMgam,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))//Simplify*)
(*SMC9Zresult=-((Coefficient[TotalSMZ,lepL]+Coefficient[TotalSMZ,lepR])*(-mW^2/g2^4/quarkL))//Expand//Simplify*)
(*SMC9boxresult=-((Coefficient[TotalSMbox,lepL]+Coefficient[TotalSMbox,lepR])*(-mW^2/g2^4/quarkL))//Expand//Simplify*)


SMC9gamresultnolog=SMC9gamresult-Coefficient[SMC9gamresult,Log[\[Kappa]]]*Log[\[Kappa]]//Simplify


SMC9gamGIMMEDresult=(SMC9gamresultnolog-Limit[SMC9gamresultnolog,\[Kappa]->0]+Coefficient[SMC9gamresult,Log[\[Kappa]]]*Log[\[Kappa]])//Simplify


(x^2 (-25+44 x-19 x^2)-2 (8-32 x+54 x^2-30 x^3+3 x^4) Log[x])/(36 (-1+x)^4)


(* ::Input:: *)
(*(*now lets get it to calculate C7, C9 and C10 for me*)*)
(*deltaalphaover2alpha=(-e*s\[Theta]*Sqrt[1-s\[Theta]^2]*v^2*COUPcHWB+deltaenew*newshiftdum )/e;*)
(*deltaenew=(deltag2new*s\[Theta]^3+deltag1new*Sqrt[1-s\[Theta]^2]^3);*)
(*deltag2new=g2*v^2*(-COUPcphil3+1/2*COUPcll);*)
(*deltag1new=v^2*(g1*(-COUPcphil3+1/2*COUPcll)-1/4*1/g1*(g2^2+g1^2)*COUPcHD-g2*COUPcHWB);*)
(*\[Delta]GF=1/(Sqrt[2]GF) (Sqrt[2]COUPcphil3 -1/Sqrt[2]COUPcll);*)
(*\[Delta]mZ2=mZ^2((1/(2Sqrt[2]GF))COUPcHD+Sqrt[2]/GF mW/mZ Sqrt[1-mW^2/mZ^2]COUPcHWB);*)
(*mZ=1/Sqrt[1-s\[Theta]^2] mW;v=2 mW /g2; GF=Sqrt[2]g2^2/(8mW^2);*)
(*s\[Theta]=g1/Sqrt[g1^2+g2^2];*)
(*e=g1 g2/Sqrt[g1^2+g2^2];*)
(*C10Zresult=((Coefficient[FORMZd6FeynGaugebits,lepL]-Coefficient[FORMZd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C9Zresult=-((Coefficient[FORMZd6FeynGaugebits,lepL]+Coefficient[FORMZd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C10boxresult=((Coefficient[FORMboxd6FeynGaugebits,lepL]-Coefficient[FORMboxd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C9boxresult=-((Coefficient[FORMboxd6FeynGaugebits,lepL]+Coefficient[FORMboxd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C10newresult=((Coefficient[FORMnewd6FeynGaugebits,lepL]-Coefficient[FORMnewd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C9newresult=-((Coefficient[FORMnewd6FeynGaugebits,lepL]+Coefficient[FORMnewd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C7result=1/2((Coefficient[FORMgamd6FeynGaugebits,p1eps]-Coefficient[FORMgamd6FeynGaugebits,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))(g2^2/4/mW^2)//Simplify*)
(*C9gamresult=((Coefficient[FORMgamd6FeynGaugebits,p1eps]+Coefficient[FORMgamd6FeynGaugebits,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))(g2^2/4/mW^2)//Simplify*)
(*C9gamresultdeltaalphasummed=SMC9gamGIMMEDresult*(g2^2/4/mW^2)*deltaalphaover2alpha//Simplify;*)
(*C9gamresultdeltaalpha={Coefficient[C9gamresultdeltaalphasummed,COUPcphil1],Coefficient[C9gamresultdeltaalphasummed,COUPcphil3],Coefficient[C9gamresultdeltaalphasummed,COUPcphie],Coefficient[C9gamresultdeltaalphasummed,COUPcphiq1],Coefficient[C9gamresultdeltaalphasummed,COUPcphiu],Coefficient[C9gamresultdeltaalphasummed,COUPcphiq3],Coefficient[C9gamresultdeltaalphasummed,COUPcphid],Coefficient[C9gamresultdeltaalphasummed,COUPclq1],Coefficient[C9gamresultdeltaalphasummed,COUPclq3],Coefficient[C9gamresultdeltaalphasummed,COUPceu],Coefficient[C9gamresultdeltaalphasummed,COUPclu],Coefficient[C9gamresultdeltaalphasummed,COUPcqe],Coefficient[C9gamresultdeltaalphasummed,COUPcll],Coefficient[C9gamresultdeltaalphasummed,COUPcW],Coefficient[C9gamresultdeltaalphasummed,COUPcHWB],Coefficient[C9gamresultdeltaalphasummed,COUPcHD],Coefficient[C9gamresultdeltaalphasummed,COUPced],Coefficient[C9gamresultdeltaalphasummed,COUPcld]}*)
(*GIMMEDC7result=C7result-Limit[C7result,\[Kappa]->0];*)
(*C9gamresultnolog=C9gamresult-Coefficient[C9gamresult,Log[\[Kappa]]]*Log[\[Kappa]];*)
(*GIMMEDC9gamresult=(C9gamresultnolog-Limit[C9gamresultnolog,\[Kappa]->0]+Coefficient[C9gamresult,Log[\[Kappa]]]*Log[\[Kappa]]);*)
(*GIMMEDC9boxresult=C9boxresult-Limit[C9boxresult,\[Kappa]->0];*)
(*GIMMEDC9Zresult=C9Zresult-Limit[C9Zresult,\[Kappa]->0];*)
(*GIMMEDC9newresult=C9newresult-Limit[C9newresult,\[Kappa]->0];*)
(*GIMMEDC10Zresult=C10Zresult-Limit[C10Zresult,\[Kappa]->0];*)
(*GIMMEDC10boxresult=C10boxresult-Limit[C10boxresult,\[Kappa]->0];*)
(*GIMMEDC10newresult=C10newresult-Limit[C10newresult,\[Kappa]->0];*)
(*GIMMEDC10totalresult=C10Zresult+C10boxresult+C10newresult-Limit[C10Zresult+C10boxresult+C10newresult,\[Kappa]->0];*)
(*Ix=-((x (7-8 x+x^2+4 (-1+x)^2 Log[mW]+2 (4-2 x+x^2) Log[x]))/(32 (-1+x)^2))//Simplify*)
(**)


simpGIMMedC10Zresult=GIMMEDC10Zresult//Simplify;
simpGIMMedC10newresult=GIMMEDC10newresult//Simplify;
simpGIMMedC10boxresult=GIMMEDC10boxresult//Simplify;
simpGIMMedC10totalresult=GIMMEDC10totalresult//Simplify;
simpGIMMedC9Zresult=GIMMEDC9Zresult//Simplify;
simpGIMMedC9newresult=GIMMEDC9newresult//Simplify;
simpGIMMedC9boxresult=GIMMEDC9boxresult//Simplify; (*todo: check that this is consistent with setting shiftdum\[Rule]0 before this step!*)
simpGIMMedC7result=GIMMEDC7result//Simplify;
simpGIMMedC9gamresult=GIMMEDC9gamresult+C9gamresultdeltaalpha//Simplify//Expand//Simplify;
simpGIMMedC9totalresult=GIMMEDC9gamresult+C9gamresultdeltaalpha+GIMMEDC9boxresult+GIMMEDC9newresult+GIMMEDC9Zresult//Simplify//Expand//Simplify;
simpGIMMedC9hardresult=GIMMEDC9boxresult+GIMMEDC9newresult+GIMMEDC9Zresult//Simplify;
simpC9gamresult=C9gamresult+C9gamresultdeltaalpha//Simplify;


SMGIMMEDC10Zresult//Simplify
SMC9gamGIMMEDresult//Simplify




Grid[Transpose[{wilsonCoeffs,simpGIMMedC9Zresult,simpGIMMedC9boxresult,simpGIMMedC9newresult,simpC9gamresult,simpGIMMedC9gamresult}/.newshiftdum->0//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC9hardresult,simpGIMMedC9gamresult}/.newshiftdum->0//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMedC9Zresult,newshiftdum],Coefficient[simpGIMMedC9boxresult,newshiftdum],Coefficient[simpGIMMedC9gamresult,newshiftdum]}//Simplify],Frame->All]


SMGIMMEDC10Zresult//Simplify


(\[Kappa] (-17+20 \[Kappa]-3 \[Kappa]^2+4 (-1+\[Kappa])^2 Log[mW]+2 (-8+\[Kappa]) \[Kappa] Log[\[Kappa]]))/(16(-1+\[Kappa])^2)+(\[Kappa]^2 (25-44 \[Kappa]+19 \[Kappa]^2)+2 (8-32 \[Kappa]+54 \[Kappa]^2-30 \[Kappa]^3+3 \[Kappa]^4) Log[\[Kappa]])/(72 (-1+\[Kappa])^4)//Simplify


(-153+536 \[Kappa]-628 \[Kappa]^2+272 \[Kappa]^3-27 \[Kappa]^4)/(-1+\[Kappa])^4//Simplify


(16-64 \[Kappa]+36 \[Kappa]^2+93 \[Kappa]^3-84 \[Kappa]^4+9 \[Kappa]^5) /(-1+\[Kappa])^4//Simplify


2/144//Simplify


 (\[Kappa] (27-88 \[Kappa]+89 \[Kappa]^2-28 \[Kappa]^3)+2 (-8+32 \[Kappa]-36 \[Kappa]^2+3 \[Kappa]^3+6 \[Kappa]^4) Log[\[Kappa]])/(18  (-1+\[Kappa])^4)+2 SMC9gamGIMMEDresult//Simplify


Grid[Transpose[{wilsonCoeffs,simpGIMMedC9Zresult//Simplify,simpGIMMedC9boxresult,simpGIMMedC9newresult,simpGIMMedC9gamresult}/.newshiftdum->1//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC9gamresult,simpGIMMedC9Zresult+simpGIMMedC9newresult,simpGIMMedC9boxresult}/.newshiftdum->1//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC10Zresult+simpGIMMedC10newresult,simpGIMMedC10boxresult,simpGIMMedC10totalresult}/.newshiftdum->1//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC10Zresult+simpGIMMedC10newresult,simpGIMMedC10boxresult,simpGIMMedC10totalresult}/.newshiftdum->0//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMedC10Zresult+simpGIMMedC10newresult,newshiftdum],Coefficient[simpGIMMedC10boxresult,newshiftdum],Coefficient[simpGIMMedC10totalresult,newshiftdum]}//Simplify],Frame->All]


(6-7 \[Kappa]+\[Kappa]^2)/(-1+\[Kappa])^2//Simplify


(17-3 \[Kappa]) (-1+\[Kappa])//Expand


Grid[Transpose[{wilsonCoeffs,simpGIMMedC9Zresult+simpGIMMedC9newresult,simpGIMMedC9boxresult,simpGIMMedC9totalresult}/.newshiftdum->0//Simplify],Frame->All]


c


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMedC7result,newshiftdum]}]//Simplify,Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC7result}/.newshiftdum->0]//Simplify,Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC7result}/.newshiftdum->1//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMedC7result,newshiftdum]}//Simplify],Frame->All]


SMGIMMEDC7result


SMGIMMEDC9gamresult


Grid[Transpose[{wilsonCoeffs,simpGIMMedC9Zresult,simpGIMMedC9boxresult,simpGIMMedC9newresult,simpC9gamresult,simpGIMMedC9gamresult}/.newshiftdum->1],Frame->All]


(*simplifying shiz*)
1/(32 (-1+\[Kappa])^2) \[Kappa] ((25-7 \[Kappa]) (-1+\[Kappa])+4 (-1+\[Kappa])^2 Log[mW]+2 (4-14 \[Kappa]+\[Kappa]^2) Log[\[Kappa]])//Expand//Simplify


(-25+32 \[Kappa]-7 \[Kappa]^2)/(-1+\[Kappa])^2//Simplify


deltag1new



