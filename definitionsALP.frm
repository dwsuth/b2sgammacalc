#define feyngauge "feyngauge"

Dimension D;
Vectors kbub,kbub2,kbubA,kbubB,kbubC,ktri1,ktri2,ktri3,ktri4,ktri5,ktri6,p1,p2,p3,p4,p5,k,r1,r2,r3,kint,epspol1,epspol2,epspol3,epspol4,sophie;
Symbols xi,feynX,feynY,feynZ,feynXbub,DEN,ERROR,marker1,marker2,marker3,
COUPcBB,COUPgp,COUPg,COUPyu, COUPyudag, COUPyutilde, COUPyutildedag, COUPyd, COUPydtilde, COUPyddag,COUPydtildedag,
intDUMMY,x1,x2,x3,mw,mw2,mz2,mb,qsqr,x,logx,ALARMk,ALARMeps,MARK1,MARK2,MARK3,DEL,a,b,c,d,n,ksqr,eps,mt,oneoveroneminusx,d6dum,lightdum;
Indices mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,gamma4,delta,alpha1,beta1,mu1,nu1,sigma1,delta1;
Functions VHdagHB,VHdagHBB,VBBalp,VBBalp2,VBBB,VBBBalp,VBBBLastBIsSpecialAlp,VBBBO2GUNPERM,VBBBO3G,VBBO2G,
PRgauge,PRalp,
PRh,PRf, VQbaruH,VubarQH,VQbardH,VdbarQH,
VQbaruHa,VubarQHa,VQbardHa,VdbarQHa,
scalarint,feyn2int,feyn3int,egamp,egamm,hypergeom,denT1,denT2,denW1,denW2,denG1,denG2,intdummy,log,oneoveroneminus;

******
***hack to stop immediate contraction of indices
*******
Index gamma1=0;
Index gamma2=0;
Index gamma3=0;
Index gamma4=0;
Index mu=0;
Index nu=0;
Index rho=0;
Index sigma=0;
Index alpha=0;
Index beta=0;
Index gamma=0;
Index delta=0;
Index delta1=0;
Index mu1=0;
Index alpha1=0;
Index sigma1=0;
Index beta1=0;
Index nu1=0;
