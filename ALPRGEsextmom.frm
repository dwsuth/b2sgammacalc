#include definitionsALP.frm

**********
****define diagrams
***********

Local FBBHHtri = VBBalp(p1,k-p1,-k,alpha,beta) * PRgauge(p1-k,beta,gamma,feynX) * VHdagHBB(p3,p2,p1-k,p4+k,gamma,delta) * PRgauge(p4+k,delta,sigma,feynY) * VBBalp2(-p4-k,p4,k,sigma,nu) * PRalp(k) * epspol1(alpha) * epspol4(nu);
Local FBBHHbox =
VBBalp(p1,k-p1,-k,alpha,beta) * PRgauge(p1-k,beta,gamma,feynX) * VHdagHB(k-p1-p2,p2,p1-k,gamma) * PRh(p1+p2-k) * VHdagHB(p3,p1+p2-k,p4+k,delta) * PRgauge(p4+k,delta,sigma,feynY) * VBBalp2(-p4-k,p4,k,sigma,nu) * PRalp(k) * epspol1(alpha) * epspol4(nu) +
VBBalp(p1,k-p1,-k,alpha,beta) * PRgauge(p1-k,beta,gamma,feynX) * VHdagHB(p1+p3-k,p2,k+p4,delta) * PRh(p1+p3-k) * VHdagHB(p3,k-p1-p3,p1-k,gamma) * PRgauge(p4+k,delta,sigma,feynY) * VBBalp2(-p4-k,p4,k,sigma,nu) * PRalp(k) * epspol1(alpha) * epspol4(nu);
Local FBBtwopoint = VBBalp(p1,kbub-p1,-kbub,alpha,beta) * PRgauge(p1-kbub,beta,gamma,feynXbub) * VBBalp2(p1-kbub,-p1,kbub,gamma,nu) * epspol1(alpha) * epspol4(nu) * PRalp(kbub);

Local FYukdbubble=VdbarQHa(p4,p1+p2-kbub2,p3,kbub2)*PRf(p1+p2-kbub2,mu)*VQbardHa(kbub2-p1-p2,p1,p2,-kbub2)*PRalp(kbub2);
Local FYukdtriangle=VQbardHa(p5,-k-p4-p5,p4,k)*PRf(-k-p4-p5,mu)*VdbarQH(k+p4+p5,p1+p2-k,p3)*PRf(-k+p1+p2,nu)*VQbardHa(k-p1-p2,p1,p2,-k)*PRalp(k);
Local FYukdtriangleswaphiggses=VQbardHa(p5,-k-p4-p5,p4,k)*PRf(-k-p4-p5,mu)*VdbarQH(k+p4+p5,p1+p3-k,p2)*PRf(-k+p1+p3,nu)*VQbardHa(k-p1-p3,p1,p3,-k)*PRalp(k);

Local FBBBtri =
 VBBalp(p1,ktri1-p1,-ktri1,alpha,beta) * PRgauge(p1-ktri1,beta,gamma,feynX) * VBBB(p1-ktri1,p3+ktri1,p2,gamma,delta,rho) * PRgauge(p3+ktri1,delta,sigma,feynY) * VBBalp2(-p3-ktri1,p3,ktri1,sigma,nu) * PRalp(ktri1) * epspol1(alpha) * epspol2(rho) * epspol3(nu)
+ VBBalp(p2,ktri2-p2,-ktri2,alpha,beta) * PRgauge(p2-ktri2,beta,gamma,feynX) * VBBB(p2-ktri2,p1+ktri2,p3,gamma,delta,rho) * PRgauge(p1+ktri2,delta,sigma,feynY) * VBBalp2(-p1-ktri2,p1,ktri2,sigma,nu) * PRalp(ktri2) * epspol2(alpha) * epspol3(rho) * epspol1(nu)
+ VBBalp(p3,ktri3-p3,-ktri3,alpha,beta) * PRgauge(p3-ktri3,beta,gamma,feynX) * VBBB(p3-ktri3,p2+ktri3,p1,gamma,delta,rho) * PRgauge(p2+ktri3,delta,sigma,feynY) * VBBalp2(-p2-ktri3,p2,ktri3,sigma,nu) * PRalp(ktri3) * epspol3(alpha) * epspol1(rho) * epspol2(nu);

Local FBBBbub1PI=
+ VBBalp(p2,kbubA-p2,-kbubA,alpha,beta) * PRgauge(kbubA-p2,beta,gamma,feynXbub) * PRalp(kbubA) * VBBBalp(p1,p3,p2-kbubA,kbubA,rho,sigma,gamma) * epspol1(rho) * epspol3(sigma) * epspol2(alpha) * marker2
+ VBBalp(p3,kbubB-p3,-kbubB,alpha,beta) * PRgauge(kbubB-p3,beta,gamma,feynXbub) * PRalp(kbubB) * VBBBalp(p2,p1,p3-kbubB,kbubB,rho,sigma,gamma) * epspol2(rho) * epspol1(sigma) * epspol3(alpha) * marker3
+ VBBalp(p1,kbubC-p1,-kbubC,alpha,beta) * PRgauge(kbubC-p1,beta,gamma,feynXbub) * PRalp(kbubC) * VBBBalp(p3,p2,p1-kbubC,kbubC,rho,sigma,gamma) * epspol3(rho) * epspol2(sigma) * epspol1(alpha) * marker1;

Local FBBBbubSpecial1PI=+ VBBalp(p2,kbubA-p2,-kbubA,alpha,beta) * PRgauge(kbubA-p2,beta,gamma,feynXbub) * PRalp(kbubA) * VBBBLastBIsSpecialAlp(p1,p3,p2-kbubA,kbubA,rho,sigma,gamma) * epspol1(rho) * epspol3(sigma) * epspol2(alpha) 
+ VBBalp(p3,kbubB-p3,-kbubB,alpha,beta) * PRgauge(kbubB-p3,beta,gamma,feynXbub) * PRalp(kbubB) * VBBBLastBIsSpecialAlp(p2,p1,p3-kbubB,kbubB,rho,sigma,gamma) * epspol2(rho) * epspol1(sigma) * epspol3(alpha) 
+ VBBalp(p1,kbubC-p1,-kbubC,alpha,beta) * PRgauge(kbubC-p1,beta,gamma,feynXbub) * PRalp(kbubC) * VBBBLastBIsSpecialAlp(p3,p2,p1-kbubC,kbubC,rho,sigma,gamma) * epspol3(rho) * epspol2(sigma) * epspol1(alpha);

*- VBBalp(p3,ktri4-p3,-ktri4,alpha,beta) * PRgauge(p3-ktri4,beta,gamma,feynX) * VBBB(p3-ktri4,p1+ktri4,p2,gamma,delta,rho) * PRgauge(p1+ktri4,delta,sigma,feynY) * VBBalp2(-p1-ktri4,p1,ktri4,sigma,nu) * PRalp(ktri4) * epspol3(alpha) * epspol2(rho) * epspol1(nu)
*- VBBalp(p1,ktri5-p1,-ktri5,alpha,beta) * PRgauge(p1-ktri5,beta,gamma,feynX) * VBBB(p1-ktri5,p2+ktri5,p3,gamma,delta,rho) * PRgauge(p2+ktri5,delta,sigma,feynY) * VBBalp2(-p2-ktri5,p2,ktri5,sigma,nu) * PRalp(ktri5) * epspol1(alpha) * epspol3(rho) * epspol2(nu)
*- VBBalp(p2,ktri6-p2,-ktri6,alpha,beta) * PRgauge(p2-ktri6,beta,gamma,feynX) * VBBB(p2-ktri6,p3+ktri6,p1,gamma,delta,rho) * PRgauge(p3+ktri6,delta,sigma,feynY) * VBBalp2(-p3-ktri6,p3,ktri6,sigma,nu) * PRalp(ktri6) * epspol2(alpha) * epspol1(rho) * epspol3(nu); *you should multiply by a structure factor f^abc

Local FBBBbub1PR= 
+ VBBalp(p2,kbubA-p2,-kbubA,alpha,beta) * PRgauge(kbubA-p2,beta,gamma,feynXbub) * PRalp(kbubA) * VBBalp2(p2-kbubA,-p2,kbubA,gamma,delta) * (-i_/p2.p2) * VBBB(p2,p1,p3,delta,rho,sigma) * epspol1(rho) * epspol3(sigma) * epspol2(alpha) * marker2
+ VBBalp(p3,kbubB-p3,-kbubB,alpha,beta) * PRgauge(kbubB-p3,beta,gamma,feynXbub) * PRalp(kbubB) * VBBalp2(p3-kbubB,-p3,kbubB,gamma,delta) * (-i_/p3.p3) * VBBB(p3,p2,p1,delta,rho,sigma) * epspol2(rho) * epspol1(sigma) * epspol3(alpha) * marker3
+ VBBalp(p1,kbubC-p1,-kbubC,alpha,beta) * PRgauge(kbubC-p1,beta,gamma,feynXbub) * PRalp(kbubC) * VBBalp2(p1-kbubC,-p1,kbubC,gamma,delta) * (-i_/p1.p1) * VBBB(p1,p3,p2,delta,rho,sigma) * epspol3(rho) * epspol2(sigma) * epspol1(alpha) * marker1;


*********
**apply feynman rules for vertices (V) and propagators (P)
*********

#include feynRulesALPgaugebasis.frm
.sort

********
***shift integration momentum
*******
id k(mu?)=kint(mu)-sophie(mu); *if sophie appears, this approximation is bollocks and we need to define this properly for each diagram configuration
id ktri1(mu?) = kint(mu) + feynX*p1(mu) - feynY*p3(mu);
id ktri2(mu?) = kint(mu) + feynX*p2(mu) - feynY*p1(mu);
id ktri3(mu?) = kint(mu) + feynX*p3(mu) - feynY*p2(mu);
id ktri4(mu?) = kint(mu) + feynX*p3(mu) - feynY*p1(mu);
id ktri5(mu?) = kint(mu) + feynX*p1(mu) - feynY*p2(mu);
id ktri6(mu?) = kint(mu) + feynX*p2(mu) - feynY*p3(mu);
id kbub(mu?)=kint(mu)+feynXbub*p1(mu);
id kbub2(mu?)=kint(mu)+feynXbub*(p1(mu)+p2(mu));

id kbubA(mu?)=kint(mu)+feynXbub*p2(mu);
id kbubB(mu?)=kint(mu)+feynXbub*p3(mu);
id kbubC(mu?)=kint(mu)+feynXbub*p1(mu);
.sort
*******
***reduce tensors of loop momenta to products of k-squared(ksqr) and metric factors (d_(??,??))
*******
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)*kint(beta?)*kint(gamma?)=ALARMk;
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)*kint(beta?)=
ksqr^3*(
d_(mu,nu)*(d_(rho,sigma)*d_(alpha,beta)+d_(rho,alpha)*d_(sigma,beta)+d_(rho,beta)*d_(alpha,sigma))
+d_(mu,rho)*(d_(nu,sigma)*d_(alpha,beta)+d_(nu,alpha)*d_(sigma,beta)+d_(nu,beta)*d_(alpha,sigma))
+d_(mu,sigma)*(d_(rho,nu)*d_(alpha,beta)+d_(rho,alpha)*d_(nu,beta)+d_(rho,beta)*d_(alpha,nu))
+d_(mu,alpha)*(d_(rho,sigma)*d_(nu,beta)+d_(rho,nu)*d_(sigma,beta)+d_(rho,beta)*d_(nu,sigma))
+d_(mu,beta)*(d_(rho,sigma)*d_(alpha,nu)+d_(rho,alpha)*d_(sigma,nu)+d_(rho,nu)*d_(alpha,sigma))
) * (1+eps/2)/4 * (1+eps/3)/6 * (1+eps/4)/8 + eps^2*ALARMeps;

id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)=0;
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)=ksqr^2*
( d_(mu,nu)*d_(rho,sigma)+d_(mu,rho)*d_(nu,sigma)+d_(mu,sigma)*d_(nu,rho) )*
(1+eps/2)/4 * (1+eps/3)/6 + eps^2*ALARMeps;
id kint(mu?)*kint(nu?)*kint(rho?)=0;
id kint(mu?)*kint(nu?)=ksqr*d_(mu,nu) * ((1+eps/2)/4) + eps^2*ALARMeps;
id kint(mu?)=0;

.sort

*********feynman gauge
id xi=1;
.sort

**************
***contract and sum over lorentz indices
************
sum mu,nu,rho,sigma,alpha,beta,gamma,delta,gamma1,gamma2,gamma3,gamma4,alpha1,beta1,delta1,mu1,nu1,sigma1;
.sort

Contract;
*id D = 4-2*eps;
.sort


**********
***do scalar integrals
**********
*if ( count(dentdent,1) > 0);
*  id ksqr^d? = scalarint(d,2) * ( DEL^(2+d-2) - (feynY*feynZ*qsqr+feynY*feynZ*mb^2)*(2+d-eps-2)*DEL^(2+d-3)/mw2/x );
*else;
*  if ( count(denw,1) > 0 );
*    id ksqr^d?*denw = denw * scalarint(d,4) * 3 * ( DEL^(2+d-4) - (feynX*feynZ*mb^2+feynY*feynZ*qsqr)*(2+d-eps-4)*DEL^(2+d-5)/mw2/x );
*  else;
*    id ksqr^d? = scalarint(d,3) * 2 * ( DEL^(2+d-3) - (feynX*feynZ*mb^2+feynY*feynZ*qsqr)*(2+d-eps-3)*DEL^(2+d-4)/mw2/x );
*  endif;
*endif;


*Bracket intdummy;
*Print;
.sort

id ksqr^a?*DEN^b? = (-1)^(a-b)/16/pi_^2 *  egamm(a+2) * egamp(b-a-2) / fac_(b-1) * (1+eps+eps^2*ALARMeps) * DEL^(2+a-b) * (1-eps*log(DEL));
*id scalarint(a?,b?) = (-1)^(a-b)/16/pi_^2 * egamm(a+2) * egamp(b-a-2) / fac_(b-1) * (1+eps+eps^2*ALARMeps) * (mw2*x)^(2+a-b) * (1-eps*log(x));
.sort


****************
****expand euler gamma functions in eps, where egamp(n) = \Gamma(n+\epsilon) and egamm(n) = \Gamma(n-\epsilon)
***************
id egamp(4)=(3+eps)*egamp(3);
id egamp(3)=(2+eps)*egamp(2);
id egamp(2)=(1+eps)*egamp(1);
id egamp(1)=1;
id egamp(-2) = -(1+eps/2+eps^2*ALARMeps)/2 * egamp(-1);
id egamp(-1) = -(1+eps+eps^2*ALARMeps) * egamp(0);
id egamp(0) = 1/eps;

id egamm(4)=(3-eps)*egamm(3);
id egamm(3)=(2-eps)*egamm(2);
id egamm(2)=(1-eps)*egamm(1);
id egamm(1)=1;
id egamm(-2) = -(1-eps/2+eps^2*ALARMeps)/2 * egamm(-1);
id egamm(-1) = -(1-eps+eps^2*ALARMeps) * egamm(0);
id egamm(0) = -1/eps;

id D = 4-2*eps;
.sort

***************
********do feynman integral for triangle case
***************
id feynZ=1-feynX-feynY;
id feynX^a?*feynY^b? = 2*fac_(a)*fac_(b)/fac_(a+b+2);
id feynX^a? = 2*fac_(a)/fac_(a+2);
id feynY^b? = 2*fac_(b)/fac_(b+2);

***************
********do feynman integral for bubble case
***************
id DEL*marker1 = -feynXbub*(1-feynXbub)*p1.p1;
id DEL*marker2 = -feynXbub*(1-feynXbub)*p2.p2;
id DEL*marker3 = -feynXbub*(1-feynXbub)*p3.p3;
.sort
id feynXbub^a? = 1/(a+1);
.sort

*id p1.epspol1=0;
*id p2.epspol2=0;
*id p3.epspol3=0;
*id p1.p1=0;
*id p2.p2=0;
*id p3.p3=0;

id p1.epspol1=-p2.epspol1-p3.epspol1;
id p2.epspol2=-p1.epspol2-p3.epspol2;
id p3.epspol3=-p1.epspol3-p2.epspol3;
id p1.p2=p3.p3/2-p1.p1/2-p2.p2/2;
id p2.p3=p1.p1/2-p2.p2/2-p3.p3/2;
id p3.p1=p2.p2/2-p3.p3/2-p1.p1/2;
.sort

*id p1.p1=p2.p2+2*p2.p3+p3.p3;

**********
***discard terms of order \epsilon^0 or higher
***********
if ( count(eps,1) > -1 ) discard;
.sort

Local loopcomb = FBBBtri+FBBBbub1PI+FBBBbub1PR;
*todo: check the overall factors of these, how is the RGE actually defined in terms of epsilon contributions?

*Local BBBtarget=-6*eps^-1*(-i_)*(
*(p1(mu)*epspol1(nu)-p1(nu)*epspol1(mu))*
*(p2(alpha)*epspol2(beta)-p2(beta)*epspol2(alpha))*
*(p3(gamma)*epspol3(delta)-p3(delta)*epspol3(gamma))*d_(nu,alpha)*d_(beta,gamma)*d_(delta,mu)
*);

Local countertermO3G=(-i_)*-1/4/pi_^2*COUPcBB^2*COUPg*eps^-1*VBBBO3G(p1,p2,p3,alpha,beta,gamma)*epspol1(alpha)*epspol2(beta)*epspol3(gamma);

Local countertermO2G=(-i_)*eps^-1*(
+VBBBO2GUNPERM(p1,p2,p3,alpha,beta,gamma)
+VBBBO2GUNPERM(p2,p3,p1,beta,gamma,alpha)
+VBBBO2GUNPERM(p3,p1,p2,gamma,alpha,beta)
-VBBBO2GUNPERM(p1,p3,p2,alpha,gamma,beta)
-VBBBO2GUNPERM(p2,p1,p3,beta,alpha,gamma)
-VBBBO2GUNPERM(p3,p2,p1,gamma,beta,alpha)
)*epspol1(alpha)*epspol2(beta)*epspol3(gamma);

Local countertermO2GR=(-i_)*eps^-1*(
+VBBO2G(p1,-p1,alpha,mu)*(-i_)/p1.p1*VBBB(p1,p2,p3,mu,beta,gamma)
+VBBO2G(p2,-p2,beta,mu)*(-i_)/p2.p2*VBBB(p2,p3,p1,mu,gamma,alpha)
+VBBO2G(p3,-p3,gamma,mu)*(-i_)/p3.p3*VBBB(p3,p1,p2,mu,alpha,beta)
)*epspol1(alpha)*epspol2(beta)*epspol3(gamma);

Local HBtarget=eps^-1*-4*(p1.p4*d_(alpha,beta)-p1(beta)*p4(alpha))*epspol1(alpha)*epspol4(beta);

Local Hdtarget=g_(1,p4)-g_(1,p3);
Local Hdptarget=g_(1,p2)-g_(1,p1);

Local countertermcomb2G=countertermO2G+countertermO2GR;

#include feynRulesALPgaugebasis.frm
.sort
sum mu,nu,alpha,beta,gamma,delta,mu1,nu1,alpha1,beta1,gamma1,delta1;

id p1.epspol1=-p2.epspol1-p3.epspol1;
id p2.epspol2=-p1.epspol2-p3.epspol2;
id p3.epspol3=-p1.epspol3-p2.epspol3;
id p1.p2=p3.p3/2-p1.p1/2-p2.p2/2;
id p2.p3=p1.p1/2-p2.p2/2-p3.p3/2;
id p3.p1=p2.p2/2-p3.p3/2-p1.p1/2;
.sort

Local check3G=loopcomb-countertermO3G;
.sort

Bracket p1.p1,p2.p2,p3.p3;
Print +s;
.end
