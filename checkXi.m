(* ::Package:: *)

(* ::Input:: *)
(*FORMtoMatRules={oneoveroneminusx->1/(1-x),logx->Log[x],logxi->Log[xi],logxoverxi->Log[x/xi],logxioverx->Log[xi/x],logxxi->Log[x*xi],COUPe->g2 *COUPsinW,mw->mW,mw2->mW^2,COUPgp->g1,COUPg->g2,COUPcosW->g2/Sqrt[g1^2+g2^2],COUPsinW->g1/Sqrt[g1^2+g2^2],mz2->mW^2/COUPcosW^2,mt->Sqrt[\[Kappa]]*mW,COUPv->2*mW/g2,eps->-1/2/Log[mW],GF->Sqrt[2]*g2^2/8/mW^2};*)
(*Rules2={x->\[Kappa],xi->\[Xi],COUPsinW->g1/Sqrt[g1^2+g2^2],COUPcosW->g2/Sqrt[g1^2+g2^2]};*)
(*StringReplacementRules={"g_(1,6_,delta1)"->"2*quarkL","g_(1,6_,N1_?)"->"2*quarkL","g_(1,6_,alpha)"->"2*quarkL","g_(2,6_,delta1)"->"2*lepL","g_(2,6_,N1_?)"->"2*lepL","g_(2,6_,alpha)"->"2*lepL","g_(2,7_,delta1)"->"2*lepR","g_(2,7_,N1_?)"->"2*lepR","g_(2,7_,alpha)"->"2*lepR","pi_"->"Pi","log(x)"->"logx","log(xi)"->"logxi","log(xi^-1*x)"->"logxoverxi","log(xi*x^-1)"->"logxioverx","log(xi*x)"->"logxxi","log(x*xi)"->"logxxi","g_(1,6_,epspol)"->"2*vectorshiz","g_(1,6_)"->"2*scalarshiz","p1.epspol"->"p1eps","p2.epspol"->"p2eps","sqrt_(2)"->"Sqrt[2]",";"->";\n"};*)
(*$Assumptions=\[Kappa]>1&&\[Xi]>0&&g1>0&&g2>0;*)


(* ::Input:: *)
(*(*only needed on sophies mac*)*)
(*If[$MachineName!="walnuts",SetDirectory["~/Work/b2sgammacalc"];]*)
(**)


(* ::Input:: *)
(*inputFORMfiles={"HRMEXIout","bubblegamAout","bubbleZAXIout","bubbleZBXIout","bsgammaout"};*)
(*resultstrs=Import/@inputFORMfiles;*)
(*sanitisedStrings=StringReplace[StringDelete[#,"\n"|"\r"|" "],StringReplacementRules]&/@resultstrs;*)
(*ToExpression/@sanitisedStrings*)


(* ::Input:: *)
(*diagnamesbox={"Fbox"};*)
(*diagnamesnew={"Ga","Gb","Gc","Gd","Ge","Gf","Gg","GhW","GhG","GiW","GiG","Gj","Gk","Gbox1","Gbox2"};*)
(*diagnamesZ={"FaWZ","FaGZ","FbGZ","FbWZ","FcZ","FdZ","FeZ","FfZ","FgZ","FhZ"};*)
(*diagnamesgam={"FaWgam","FaGgam","FbGgam","FbWgam","Fcgam","Fdgam","Fegam","Ffgam","Fggam","Fhgam"};*)
(*FORMbox=(16*Pi^2*{Fbox}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMnew=(16*Pi^2*{Ga,Gb,Gc,Gd,Ge,Gf,Gg,GhW,GhG,GiW,GiG, Gj, Gk,Gbox1,Gbox2}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMZ=(16*Pi^2*{FaGZ,FaWZ,FbGZ,FbWZ,FcZ,FdZ,FeZ,FfZ,FgZ,FhZ}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMgam=(16*Pi^2*{FaWgam,FaGgam,0,0,Fcgam,Fdgam,Fegam,Ffgam,Fggam,Fhgam}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)
(*{FORMboxd4,FORMnewd4,FORMZd4,FORMgamd4}={FORMbox,FORMnew,FORMZ,FORMgam}/.d6dum->0;*)


(* ::Input:: *)
(*(*check xi dependence of axial dim 4 piece*)*)
(*endd4result=Total[FORMboxd4]+Total[FORMZd4]//Simplify;*)
(*axiald4result=(Coefficient[endd4result,lepL]-Coefficient[endd4result,lepR]//Expand//Simplify//Expand);*)
(*vectoriald4result=Limit[(Coefficient[endd4result,lepL]+Coefficient[endd4result,lepR]//Expand//Simplify//Expand),\[Xi]->1];*)
(*GIMMEDaxiald4result=axiald4result-Limit[axiald4result,\[Kappa]->0]//Simplify*)
(*GIMMEDvectoriald4result=vectoriald4result-Limit[vectoriald4result,\[Kappa]->0]//Simplify*)


(* ::Input:: *)
(*(*find c7 and c9*)*)
(*c9gamd4=Limit[(Coefficient[FORMgamd4,lepL]+Coefficient[endd4result,lepR]//Expand//Simplify//Expand),\[Xi]->1]*)
(**)
(**)


(* ::Input:: *)
(*wilsonCoeffs={COUPcphil1,COUPcphil3,COUPcphie,COUPcphiq1,COUPcphiu,COUPcphiq3,COUPcphid,COUPclq1,COUPclq3,COUPceu,COUPclu,COUPcqe,COUPcll,COUPcW,COUPcHWB,COUPcHD,COUPced,COUPcld};*)
(*{FORMboxd6,FORMnewd6,FORMZd6,FORMgamd6}=Coefficient[Total[#],d6dum]&/@{FORMbox,FORMnew,FORMZ,FORMgam}*)
(*FORMd6bsll=Total[{FORMboxd6,FORMnewd6,FORMZd6}];*)
(**)


wcbits=Coefficient[FORMd6bsll,#]&/@wilsonCoeffs


axiald6result=(Coefficient[wcbits,lepL]-Coefficient[wcbits,lepR]//Expand//Simplify//Expand)


vectoriald6result=(Coefficient[wcbits,lepL]+Coefficient[wcbits,lepR]//Expand//Simplify//Expand)


Length[axialresult]


GIMMEDaxiald6result=axiald6result-Limit[axiald6result,\[Kappa]->0]


simpGIMMEDaxiald6=GIMMEDaxiald6result//Simplify
simpaxiald6=axiald6result//Simplify


Gj


xiindep=(#==0)===True& /@ D[simpGIMMEDaxiald6/.{newshiftdum->0}//Simplify,\[Xi]]
Grid[Transpose[{wilsonCoeffs,simpGIMMEDaxiald6,xiindep}/.{newshiftdum->0}]//Simplify,Frame->All]


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMEDaxiald6,newshiftdum]}]//FullSimplify,Frame->All]


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMEDaxiald6,newshiftdum]}]//FullSimplify,Frame->All]


Gamma[4]


xiindep=(#==0)===True& /@ D[simpGIMMEDaxiald6/.{newshiftdum->1,fielddum->1},\[Xi]]
Grid[Transpose[{wilsonCoeffs,simpGIMMEDaxiald6,xiindep}/.{newshiftdum->1,fielddum->1}]//Simplify,Frame->All]


(simpGIMMEDaxiald6[[-3]]/.{shiftdum->1,fielddum->1})//Simplify
(simpGIMMEDaxiald6[[-4]]/.{shiftdum->1})//Simplify//Expand//Simplify
(simpGIMMEDaxiald6[[-4]]/.{shiftdum->0,fielddum->0})//Simplify//Expand//Simplify
(simpGIMMEDaxiald6[[-4]]/.{shiftdum->0,fielddum->1})//Simplify//Expand//Simplify





-(( \[Kappa] (-1+8 \[Kappa]-7 \[Kappa]^2+4 (-1+\[Kappa])^2 Log[mW]+2 (4-2 \[Kappa]+\[Kappa]^2) Log[\[Kappa]]))/(32 (-1+\[Kappa])^2))//Expand


(-1+8 \[Kappa]-7 \[Kappa]^2)/(-1+\[Kappa])^2//Simplify


(*this is the answer for the overall coupcHWB bit when using daveVC feynman rules or trott feynman rules*)
-((3 g1 g2^3 quarkL \[Kappa] (\[Kappa] (-1 + \[Xi]) (\[Kappa] - 2 \[Xi] + \[Kappa] \[Xi]) Log[\[Kappa]] - (-1 + \[Kappa]) ((\[Kappa] - \[Xi]) (-1 + \[Xi]) + (-1 + \[Kappa]) \[Xi]^2 Log[\[Xi]])))/((g1^2 + 
    g2^2) (-1 + \[Kappa])^2 (\[Kappa] - \[Xi]) (-1 + \[Xi])))


(simpGIMMEDaxiald6[[-4]]/.{shiftdum->1,fielddum->1})//Simplify//Limit[#,\[Xi]->1]&


(*xi to one limit of chwb piece, trott and dave*)
-((6 g1 g2^3 quarkL \[Kappa] (1-\[Kappa]+\[Kappa] Log[\[Kappa]]))/((g1^2+g2^2) (-1+\[Kappa])^2))


(* ::Code:: *)
(*1/(4 (-1+\[Kappa])^2 (\[Kappa]-\[Xi])^2 (-1+\[Xi]))g2^2 quarkL \[Kappa] ((-1+\[Xi]) (6 \[Kappa]^3-4 \[Xi]^2+14 \[Kappa] \[Xi]^2-\[Kappa]^2 (-3+\[Xi] (18+\[Xi]))) Log[\[Kappa]]+(-1+\[Kappa]) ((-1+\[Xi]) (-\[Kappa]+\[Xi]) (-10 \[Xi]+\[Kappa] (11-2 \[Kappa]+\[Xi]))+(-1+\[Kappa]) \[Xi] (6 \[Kappa]+(-7+\[Xi]) \[Xi]) Log[\[Xi]]))//Expand //FullSimplify//Expand//Simplify*)
