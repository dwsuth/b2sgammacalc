
id g_(1,6_,epspol,p1,p2) = 2*p1.p2*g_(1,6_,epspol)-g_(1,6_,epspol,p2,p1);
id g_(1,6_,epspol,p2,p1) = mb*g_(1,6_,epspol,p2);

id g_(1,6_,p1,epspol,p2) = 2*p2.epspol*g_(1,6_,p1)-g_(1,6_,p1,p2,epspol);
id g_(1,6_,p2,epspol,p1) = 0;

id g_(1,6_,p1,p2,epspol) = 2*p1.p2*g_(1,6_,epspol)-g_(1,6_,p2,p1,epspol);
id g_(1,6_,p2,p1,epspol) = 0;

id g_(1,6_,epspol,p2) = 2*p2.epspol*g_(1,6_) - g_(1,6_,p2,epspol);
id g_(1,6_,p2,epspol) = 0;

id g_(1,6_,p1,epspol) = 2*p1.epspol*g_(1,6_) - g_(1,6_,epspol,p1);
id g_(1,6_,epspol,p1) = mb*g_(1,6_,epspol);

id g_(1,6_,p1,p2) = 2*p1.p2*g_(1,6_) - g_(1,6_,p2,p1); 
id g_(1,6_,p2,p1) = 0;
 
id g_(1,6_,p2) = 0;
id g_(1,6_,p1) = g_(1,6_)*mb;

id g_(1,7_,epspol,p1,p2) = 2*p1.p2*g_(1,7_,epspol)-g_(1,7_,epspol,p2,p1);
id g_(1,7_,epspol,p2,p1) = mb*g_(1,7_,epspol,p2);

id g_(1,7_,p1,epspol,p2) = 2*p2.epspol*g_(1,7_,p1)-g_(1,7_,p1,p2,epspol);
id g_(1,7_,p2,epspol,p1) = 0;

id g_(1,7_,p1,p2,epspol) = 2*p1.p2*g_(1,7_,epspol)-g_(1,7_,p2,p1,epspol);
id g_(1,7_,p2,p1,epspol) = 0;

id g_(1,7_,epspol,p2) = 2*p2.epspol*g_(1,7_) - g_(1,7_,p2,epspol);
id g_(1,7_,p2,epspol) = 0;

id g_(1,7_,p1,epspol) = 2*p1.epspol*g_(1,7_) - g_(1,7_,epspol,p1);
id g_(1,7_,epspol,p1) = mb*g_(1,7_,epspol);

id g_(1,7_,p1,p2) = 2*p1.p2*g_(1,7_) - g_(1,7_,p2,p1); 
id g_(1,7_,p2,p1) = 0;
 
id g_(1,7_,p2) = 0;
id g_(1,7_,p1) = g_(1,7_)*mb;
id p1.p2 = (p3.p3 - p1.p1 - p2.p2)/2;
id p3.p3 = qsqr;
id p1.p1 = mb^2;
id p2.p2 = 0;
