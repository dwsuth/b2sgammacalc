#include definitions.frm

**********
****define diagrams
***********

****SM-like diagrams
Local FcZ = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * VtbartZ(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbW(k-p1,p1,-k,sigma) * PRwplus(-k,sigma,nu) * PRz(p3+p4,alpha,beta)/(-mz2) * VlbarlZ(p4,p3,p3+p4,beta) * denw * denT1(p2) * denT2(-p1);
Local FdZ = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * VtbartZ(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbG(k-p1,p1,-k) * PRGplus(-k) * PRz(p3+p4,alpha,beta)/(-mz2) * VlbarlZ(p4,p3,p3+p4,beta) * deng * denT1(p2) * denT2(-p1);
Local FeZ = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,rho) * PRwplus(k+p2,nu,alpha) * PRwplus(k-p1,rho,beta) * VwpwmZ(k+p2,p1-k,p3,alpha,beta,gamma) * PRz(p3+p4,gamma,sigma)/(-mz2) * VlbarlZ(p4,p3,p3+p4,sigma) * dent * denW1(p2) * denW2(-p1);
Local FfZ = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRwplus(k+p2,nu,alpha) * PRGplus(k-p1) * VwpGmZ(k+p2,p1-k,p3,alpha,gamma) * PRz(p3+p4,gamma,sigma)/(-mz2) * VlbarlZ(p4,p3,p3+p4,sigma) * dent * denW1(p2) * denG2(-p1);
Local FgZ = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,rho) * PRGplus(k+p2) * PRwplus(k-p1,rho,beta) * VGpwmZ(k+p2,p1-k,p3,beta,gamma) * PRz(p3+p4,gamma,sigma)/(-mz2) * VlbarlZ(p4,p3,p3+p4,sigma) * dent * denG1(p2) * denW2(-p1);
Local FhZ = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k+p2) * PRGplus(k-p1) * VGpGmZ(k+p2,p1-k,p3,gamma) * PRz(p3+p4,gamma,sigma)/(-mz2) * VlbarlZ(p4,p3,p3+p4,sigma) * dent * denG1(p2) * denG2(-p1);
Local FcZnu = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * VtbartZ(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbW(k-p1,p1,-k,sigma) * PRwplus(-k,sigma,nu) * PRz(p3+p4,alpha,beta)/(-mz2) * VnubarnuZ(p4,p3,p3+p4,beta) * denw * denT1(p2) * denT2(-p1);
Local FdZnu = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * VtbartZ(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbG(k-p1,p1,-k) * PRGplus(-k) * PRz(p3+p4,alpha,beta)/(-mz2) * VnubarnuZ(p4,p3,p3+p4,beta) * deng * denT1(p2) * denT2(-p1);
Local FeZnu = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,rho) * PRwplus(k+p2,nu,alpha) * PRwplus(k-p1,rho,beta) * VwpwmZ(k+p2,p1-k,p3,alpha,beta,gamma) * PRz(p3+p4,gamma,sigma)/(-mz2) * VnubarnuZ(p4,p3,p3+p4,sigma) * dent * denW1(p2) * denW2(-p1);
Local FfZnu = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRwplus(k+p2,nu,alpha) * PRGplus(k-p1) * VwpGmZ(k+p2,p1-k,p3,alpha,gamma) * PRz(p3+p4,gamma,sigma)/(-mz2) * VnubarnuZ(p4,p3,p3+p4,sigma) * dent * denW1(p2) * denG2(-p1);
Local FgZnu = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,rho) * PRGplus(k+p2) * PRwplus(k-p1,rho,beta) * VGpwmZ(k+p2,p1-k,p3,beta,gamma) * PRz(p3+p4,gamma,sigma)/(-mz2) * VnubarnuZ(p4,p3,p3+p4,sigma) * dent * denG1(p2) * denW2(-p1);
Local FhZnu = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k+p2) * PRGplus(k-p1) * VGpGmZ(k+p2,p1-k,p3,gamma) * PRz(p3+p4,gamma,sigma)/(-mz2) * VnubarnuZ(p4,p3,p3+p4,sigma) * dent * denG1(p2) * denG2(-p1);

****loopy diagram
*Local FloopcqqZ = Vsbartnil(p2,k-p1,nu) * PRt(k-p1,mu)* VtbartZ(-k+p1,k+p2,p3,alpha)*PRt(k+p2,rho)*Vtbarbnil(-k-p2,p1,nu)*epspol(alpha)*dent*dent;
Local FloopcqqZ = Vsbartnil(p2,k-p1,nu) * PRt(k-p1,mu)* VtbartZ(-k+p1,k+p2,p3,gamma)* PRz(p3+p4,gamma,sigma)/(-mz2) * VlbarlZ(p4,p3,p3+p4,sigma)*PRt(k+p2,rho)*Vtbarbnil(-k-p2,p1,nu)*denT1(p2) * denT2(-p1);
Local FloopcqqZnu = Vsbartnil(p2,k-p1,nu) * PRt(k-p1,mu)* VtbartZ(-k+p1,k+p2,p3,gamma)* PRz(p3+p4,gamma,sigma)/(-mz2) * VnubarnuZ(p4,p3,p3+p4,sigma)*PRt(k+p2,rho)*Vtbarbnil(-k-p2,p1,nu)*denT1(p2) * denT2(-p1);

****diagrams with 4 point vertices involving leptons
Local Ga = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRGplus(k+p2) * VGpGmlbarl(k+p2,p1-k,p4,p3) * dent * denG1(p2) * denG2(-p1);
Local Gb = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRwplus(k+p2,nu,alpha) * VWpGmlbarl(k+p2,p1-k,p4,p3,alpha) * dent * denW1(p2) * denG2(-p1);
Local Gc = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,nu) * PRwplus(k-p1,nu,alpha) * PRGplus(k+p2) * VGpWmlbarl(k+p2,p1-k,p4,p3,alpha) * dent * denG1(p2) * denW2(-p1);
Local Gd = VsbartW(p2,-k-p2,k,nu) * PRt(k,mu) * Vtbartlbarl(k+p2,p1-k,p4,p3) * PRt(k,rho) * VtbarbW(k-p1,p1,-k,alpha) * PRwplus(k,nu,alpha) * denw * denT1(p2) * denT2(-p1);
Local Ge = VsbartG(p2,-k-p2,k) * PRt(k,mu) * Vtbartlbarl(k+p2,p1-k,p4,p3) * PRt(k,rho) * VtbarbG(k-p1,p1,-k) * PRGplus(k) * deng * denT1(p2) * denT2(-p1);
Local Gj = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * Vtbarblbarnu(-k,p1,p4,k+p2+p3) * PRnu(k+p2+p3,sigma) * VnubarlW(-k-p2-p3,p3,k+p2,rho) * PRwplus(k+p2,nu,rho) * dent * denw * ksqr^-1;
Local Gk = VlbarnuW(p4,k+p2+p3,p1-k,rho) * PRnu(k+p2+p3,sigma) * Vsbartnubarl(p2,k,-k-p2-p3,p3) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,nu) * PRwplus(k-p1,nu,rho) * dent * denw * ksqr^-1;
Local Ganu = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRGplus(k+p2) * VGpGmnubarnu(k+p2,p1-k,p4,p3) * dent * denG1(p2) * denG2(-p1);
Local Gbnu = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRwplus(k+p2,nu,alpha) * VWpGmnubarnu(k+p2,p1-k,p4,p3,alpha) * dent * denW1(p2) * denG2(-p1);
Local Gcnu = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,nu) * PRwplus(k-p1,nu,alpha) * PRGplus(k+p2) * VGpWmnubarnu(k+p2,p1-k,p4,p3,alpha) * dent * denG1(p2) * denW2(-p1);
Local Gdnu = VsbartW(p2,-k-p2,k,nu) * PRt(k,mu) * Vtbartnubarnu(k+p2,p1-k,p4,p3) * PRt(k,rho) * VtbarbW(k-p1,p1,-k,alpha) * PRwplus(k,nu,alpha) * denw * denT1(p2) * denT2(-p1);
Local Genu = VsbartG(p2,-k-p2,k) * PRt(k,mu) * Vtbartnubarnu(k+p2,p1-k,p4,p3) * PRt(k,rho) * VtbarbG(k-p1,p1,-k) * PRGplus(k) * deng * denT1(p2) * denT2(-p1);
Local Gjnu = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * Vtbarblbarnu(-k,p1,k+p2+p3,p4) * PRl(-k-p2-p3,sigma) * VlbarnuW(-k-p2-p3,p3,k+p2,rho) * PRwplus(k+p2,nu,rho) * dent * denw * ksqr^-1;
Local Gknu = VnubarlW(p4,k+p2+p3,p1-k,rho) * PRl(-k-p2-p3,sigma) * Vsbartnubarl(p2,k,p3,-k-p2-p3) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,nu) * PRwplus(k-p1,nu,rho) * dent * denw * ksqr^-1;

***note we're ignoring light momomentum dependence of propagator denominators
***semileptonic box diagrams
Local Fbox = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,alpha) * PRwplus(k-p1,alpha,beta) * PRwplus(-k-p2,nu,rho) * VlbarnuW(p4,k+p2+p3,p1-k,beta) * PRnu(k+p2+p3,sigma) * VnubarlW(-k-p2-p3,p3,k+p2,rho) * dent * denw^2 * ksqr^-1;
Local Fboxnu = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,alpha) * PRwplus(k-p1,alpha,beta) * PRwplus(-k-p2,nu,rho) * VnubarlW(p3,-k-p2-p3,k+p2,rho)*  PRl(-k-p2-p3,sigma)  *VlbarnuW(k+p2+p3,p4,p1-k,beta) * dent * denw^2 * ksqr^-1;
Local Gbox1 = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRwplus(k+p2,nu,rho) * VlbarnuG(p4,k+p2+p3,p1-k) * PRnu(k+p2+p3,sigma) * VnubarlW(-k-p2-p3,p3,k+p2,rho) * dent * denw * deng * ksqr^-1;
Local Gbox2 = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,alpha) * PRwplus(k-p1,alpha,beta) * PRGplus(k+p2) * VlbarnuW(p4,k+p2+p3,p1-k,beta) * PRnu(k+p2+p3,sigma) * VnubarlG(-k-p2-p3,p3,k+p2) * dent * denw * deng * ksqr^-1;
Local Gbox1nu = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRwplus(k+p2,nu,rho) * VnubarlW(p3,-k-p2-p3,k+p2,rho)* PRl(-k-p2-p3,sigma)  * VlbarnuG(k+p2+p3,p4,p1-k)* dent * denw * deng * ksqr^-1;
Local Gbox2nu = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,alpha) * PRwplus(k-p1,alpha,beta) * PRGplus(k+p2) * VnubarlG(p3,-k-p2-p3,k+p2) * PRl(-k-p2-p3,sigma)  * VlbarnuW(k+p2+p3,p4,p1-k,beta)* dent * denw * deng * ksqr^-1;


****mixing diagrams
Local Ha = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * Vtbartsbarb(k+p2,p1-k,p4,p3) * PRt(p1-k,sigma) * VtbarbW(k-p1,p1,-k,rho) * PRwplus(-k,nu,rho) * dent^2 * denw;
Local Hb = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * Vtbarbsbart(k+p2,p3,p4,p1-k) * PRt2(p1-k,sigma) * VtbarbW2(k-p1,p1,-k,rho) * PRwplus(-k,nu,rho) * dent^2 * denw;
Local Hc = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * Vtbartsbarb(k+p2,p1-k,p4,p3) * PRt(p1-k,sigma) * VtbarbG(k-p1,p1,-k) * PRGplus(-k) * dent^2 * deng;
Local Hd = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * Vtbarbsbart(k+p2,p3,p4,p1-k) * PRt2(p1-k,sigma) * VtbarbG2(k-p1,p1,-k) * PRGplus(-k) * dent^2 * deng;
****the same as the above four diagrams, but with exactly one top massless (hence factor of two - either top can be massless)
Local He = VsbartW(p2,-k-p2,k,nu) * PRu(-k-p2,mu) * Vtbartsbarb(k+p2,p1-k,p4,p3) * PRt(p1-k,sigma) * VtbarbW(k-p1,p1,-k,rho) * PRwplus(-k,nu,rho) * dent * ksqr^-1 * denw+
 VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * Vtbartsbarb(k+p2,p1-k,p4,p3) * PRu(p1-k,sigma) * VtbarbW(k-p1,p1,-k,rho) * PRwplus(-k,nu,rho) * dent * ksqr^-1 * denw;
Local Hf = VsbartW(p2,-k-p2,k,nu) * PRu(-k-p2,mu) * Vtbarbsbart(k+p2,p3,p4,p1-k) * PRt2(p1-k,sigma) * VtbarbW2(k-p1,p1,-k,rho) * PRwplus(-k,nu,rho) * dent * ksqr^-1 * denw+
VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * Vtbarbsbart(k+p2,p3,p4,p1-k) * PRu2(p1-k,sigma) * VtbarbW2(k-p1,p1,-k,rho) * PRwplus(-k,nu,rho) * dent * ksqr^-1 * denw;
*Local Hg = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * Vtbartsbarb(k+p2,p1-k,p4,p3) * PRt(p1-k,sigma) * VtbarbG(k-p1,p1,-k) * PRGplus(-k) * dent * ksqr^-1 * deng;
*Local Hh = 2 * VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * Vtbarbsbart(k+p2,p3,p4,p1-k) * PRt2(p1-k,sigma) * VtbarbG2(k-p1,p1,-k) * PRGplus(-k) * dent * ksqr^-1 * deng;
Local Hg=0;
Local Hh=0;

****mixing BOX diagrams
***two Ws, two tops
Local Ka = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * VtbarbW(k+p2,-p3,-k-p2+p3,alpha) * VsbartW2(-p4,p1-k,k+p2-p3,rho) *PRt2(p1-k,sigma)*VtbarbW2(k-p1,p1,-k,beta)*PRwplus(-k,nu,beta)*PRwplus(k+p2-p3,rho,alpha)  * dent^2 * denw^2;
Local Kb = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,beta) * VsbartW2(-p4,k+p2-p3,p1-k,rho)* PRt2(k+p2-p3,sigma) *VtbarbW2(-k-p2+p3,-p3,k+p2,alpha)* PRwplus(k-p1,rho,beta)*PRwplus(k+p2,nu,alpha) * dent^2 * denw^2;
***one W, one G, two tops
Local Kc = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * VtbarbG(k+p2,-p3,-k-p2+p3) * VsbartG2(-p4,p1-k,k+p2-p3) *PRt2(p1-k,sigma)*VtbarbW2(k-p1,p1,-k,beta)*PRwplus(-k,nu,beta)*PRGplus(k+p2-p3)  * dent^2 * denw * deng;
Local Kd = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * VtbarbW(k+p2,-p3,-k-p2+p3,alpha) * VsbartW2(-p4,p1-k,k+p2-p3,rho) *PRt2(p1-k,sigma)*VtbarbG(k-p1,p1,-k)*PRGplus(-k)*PRwplus(k+p2-p3,rho,alpha)  * dent^2 * denw*deng;
Local Ke = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * VsbartG2(-p4,k+p2-p3,p1-k)* PRt2(k+p2-p3,sigma) *VtbarbW2(-k-p2+p3,-p3,k+p2,alpha)* PRGplus(k-p1)*PRwplus(k+p2,nu,alpha) * dent^2 * denw * deng;
Local Kf = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,beta) * VsbartW2(-p4,k+p2-p3,p1-k,rho)* PRt2(k+p2-p3,sigma) *VtbarbG2(-k-p2+p3,-p3,k+p2)* PRwplus(k-p1,rho,beta)*PRGplus(k+p2) * dent^2 * denw * deng;
***two Gs, two tops
Local Kg = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * VtbarbG(k+p2,-p3,-k-p2+p3) * VsbartG2(-p4,p1-k,k+p2-p3) *PRt2(p1-k,sigma)*VtbarbG2(k-p1,p1,-k)*PRGplus(-k)*PRGplus(k+p2-p3)  * dent^2 * deng^2;
Local Kh = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * VsbartG2(-p4,k+p2-p3,p1-k)* PRt2(k+p2-p3,sigma) *VtbarbG2(-k-p2+p3,-p3,k+p2)* PRGplus(k-p1)*PRGplus(k+p2) * dent^2 * deng^2;
****the same as the above eight diagrams, but with exactly one top massless
***two Ws, one top, one up
Local Ki = VsbartW2(p2,-k-p2,k,nu) * PRt2(-k-p2,mu) * VtbarbW2(k+p2,-p3,-k-p2+p3,alpha) * VsbaruW(-p4,p1-k,k+p2-p3,rho) *PRu(p1-k,sigma)*VubarbW(k-p1,p1,-k,beta)*PRwplus(-k,nu,beta)*PRwplus(k+p2-p3,rho,alpha)  * dent * denw^2* ksqr^-1+VsbaruW(p2,-k-p2,k,nu) * PRu(-k-p2,mu) * VubarbW(k+p2,-p3,-k-p2+p3,alpha) * VsbartW2(-p4,p1-k,k+p2-p3,rho) *PRt2(p1-k,sigma)*VtbarbW2(k-p1,p1,-k,beta)*PRwplus(-k,nu,beta)*PRwplus(k+p2-p3,rho,alpha)  * dent * denw^2 * ksqr^-1;
Local Kj = VsbartW2(p2,k,-k-p2,nu) * PRt2(k,mu) * VtbarbW2(-k,p1,k-p1,beta) * VsbaruW(-p4,k+p2-p3,p1-k,rho)* PRu(k+p2-p3,sigma) *VubarbW(-k-p2+p3,-p3,k+p2,alpha)* PRwplus(k-p1,rho,beta)*PRwplus(k+p2,nu,alpha) * dent * denw^2 * ksqr^-1 +VsbaruW(p2,k,-k-p2,nu) * PRu(k,mu) * VubarbW(-k,p1,k-p1,beta) * VsbartW2(-p4,k+p2-p3,p1-k,rho)* PRt2(k+p2-p3,sigma) *VtbarbW2(-k-p2+p3,-p3,k+p2,alpha)* PRwplus(k-p1,rho,beta)*PRwplus(k+p2,nu,alpha) * dent * denw^2 * ksqr^-1;
***one W, one G, one top, one up
Local Kk = VsbaruW(p2,-k-p2,k,nu) * PRu(-k-p2,mu) * VubarbG(k+p2,-p3,-k-p2+p3) * VsbartG2(-p4,p1-k,k+p2-p3) *PRt2(p1-k,sigma)*VtbarbW2(k-p1,p1,-k,beta)*PRwplus(-k,nu,beta)*PRGplus(k+p2-p3)  * dent * denw * deng * ksqr^-1+ VsbartW2(p2,-k-p2,k,nu) * PRt2(-k-p2,mu) * VtbarbG2(k+p2,-p3,-k-p2+p3) * VsbaruG(-p4,p1-k,k+p2-p3) *PRu(p1-k,sigma)*VubarbW(k-p1,p1,-k,beta)*PRwplus(-k,nu,beta)*PRGplus(k+p2-p3)  * dent * denw * deng * ksqr^-1;
Local Kl = VsbaruG(p2,-k-p2,k) * PRu(-k-p2,mu) * VubarbW(k+p2,-p3,-k-p2+p3,alpha) * VsbartW2(-p4,p1-k,k+p2-p3,rho) *PRt2(p1-k,sigma)*VtbarbG2(k-p1,p1,-k)*PRGplus(-k)*PRwplus(k+p2-p3,rho,alpha)  * dent * denw*deng * ksqr^-1 +VsbartG2(p2,-k-p2,k) * PRt2(-k-p2,mu) * VtbarbW2(k+p2,-p3,-k-p2+p3,alpha) * VsbaruW(-p4,p1-k,k+p2-p3,rho) *PRu(p1-k,sigma)*VubarbG(k-p1,p1,-k)*PRGplus(-k)*PRwplus(k+p2-p3,rho,alpha)  * dent * denw*deng * ksqr^-1;
Local Km = VsbaruW(p2,k,-k-p2,nu) * PRu(k,mu) * VubarbG(-k,p1,k-p1) * VsbartG2(-p4,k+p2-p3,p1-k)* PRt2(k+p2-p3,sigma) *VtbarbW2(-k-p2+p3,-p3,k+p2,alpha)* PRGplus(k-p1)*PRwplus(k+p2,nu,alpha) * dent * denw * deng * ksqr^-1+ VsbartW2(p2,k,-k-p2,nu) * PRt2(k,mu) * VtbarbG2(-k,p1,k-p1) * VsbaruG(-p4,k+p2-p3,p1-k)* PRu(k+p2-p3,sigma) *VubarbW(-k-p2+p3,-p3,k+p2,alpha)* PRGplus(k-p1)*PRwplus(k+p2,nu,alpha) * dent * denw * deng * ksqr^-1;
Local Kn = VsbaruG(p2,k,-k-p2) * PRu(k,mu) * VubarbW(-k,p1,k-p1,beta) * VsbartW2(-p4,k+p2-p3,p1-k,rho)* PRt2(k+p2-p3,sigma) *VtbarbG2(-k-p2+p3,-p3,k+p2)* PRwplus(k-p1,rho,beta)*PRGplus(k+p2) * dent * denw * deng * ksqr^-1 +VsbartG2(p2,k,-k-p2) * PRt2(k,mu) * VtbarbW2(-k,p1,k-p1,beta) * VsbaruW(-p4,k+p2-p3,p1-k,rho)* PRu(k+p2-p3,sigma) *VubarbG(-k-p2+p3,-p3,k+p2)* PRwplus(k-p1,rho,beta)*PRGplus(k+p2) * dent * denw * deng * ksqr^-1;
***two Gs, one top, one up
Local Ko=0;
Local Kp=0;

************
**apply feynman rules for vertices (V) and propagators (P)
*********
#include feynRules.frm
.sort
*******
**remove dim 8 and higher pieces
*******

if ( count(d6dum,1) > 1 ) discard;
.sort

********
***expand light propagator denominators in hard region
*******

id denT1(r1?) = dent; 
id denT2(r1?) = dent;
id denW1(r1?) = denw;
id denW2(r1?) = denw;
id denG1(r1?) = deng;
id denG2(r1?) = deng;

.sort
**********
***eliminate external momenta
**********
id p1=0;
id p2=0;
id p3=0;
id p4=0;

*******
***reduce tensors of loop momenta to products of k-squared(ksqr) and metric factors (d_(??,??))
*******
id k(mu?)*k(nu?)*k(rho?)*k(sigma?)*k(alpha?)*k(beta?)*k(gamma?)*k(gamma2?)=ALARMk;
id k(mu?)*k(nu?)*k(rho?)*k(sigma?)*k(alpha?)*k(beta?)*k(gamma?)=0;
id k(mu?)*k(nu?)*k(rho?)*k(sigma?)*k(alpha?)*k(beta?)=
ksqr^3*(
d_(mu,nu)*(d_(rho,sigma)*d_(alpha,beta)+d_(rho,alpha)*d_(sigma,beta)+d_(rho,beta)*d_(alpha,sigma))
+d_(mu,rho)*(d_(nu,sigma)*d_(alpha,beta)+d_(nu,alpha)*d_(sigma,beta)+d_(nu,beta)*d_(alpha,sigma))
+d_(mu,sigma)*(d_(rho,nu)*d_(alpha,beta)+d_(rho,alpha)*d_(nu,beta)+d_(rho,beta)*d_(alpha,nu))
+d_(mu,alpha)*(d_(rho,sigma)*d_(nu,beta)+d_(rho,nu)*d_(sigma,beta)+d_(rho,beta)*d_(nu,sigma))
+d_(mu,beta)*(d_(rho,sigma)*d_(alpha,nu)+d_(rho,alpha)*d_(sigma,nu)+d_(rho,nu)*d_(alpha,sigma))
) * (1+eps/2)/4 * (1+eps/3)/6 * (1+eps/4)/8 + eps^2*ALARMeps;

id k(mu?)*k(nu?)*k(rho?)*k(sigma?)*k(alpha?)=0;
id k(mu?)*k(nu?)*k(rho?)*k(sigma?)=ksqr^2*
( d_(mu,nu)*d_(rho,sigma)+d_(mu,rho)*d_(nu,sigma)+d_(mu,sigma)*d_(nu,rho) )*
(1+eps/2)/4 * (1+eps/3)/6 + eps^2*ALARMeps;
id k(mu?)*k(nu?)*k(rho?)=0;
id k(mu?)*k(nu?)=ksqr*d_(mu,nu) * (1+eps/2)/4 + eps^2*ALARMeps;
id k(mu?)=0;

.sort
*********feynman gauge
#ifdef `feyngauge'
id deng = denw;
id xi=1;
#endif

**********remove SM piece
*if ( count(d6dum,1) != 1 ) discard;
*id d6dum=1;
********************

**************
***contract and sum over lorentz indices
************
sum mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,gamma4,alpha1,beta1,delta1,mu1,nu1,sigma1;
.sort

**********
***apply momentum cons 
**********

id sqrt_(2)^2 = 2;
*id p3=-p1-p2;

*Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,xi,mt,pi_;
.sort
**********
***gamma idents 1
******
#include gammaidents.frm
#include gammaidents2.frm

.sort


*****TODO check!!!
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,alpha?,beta?,rho?) = g_(1,6_,mu,nu,rho)*(2*d_(beta,rho)*g_(2,6_,alpha) - g_(2,6_,alpha,rho,beta));
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,alpha?,rho?,beta?) = g_(1,6_,mu,nu,rho)*(2*d_(alpha,rho)*g_(2,6_,beta) - g_(2,6_,rho,alpha,beta));
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,rho?,mu?,nu?) = g_(1,6_,mu,nu,rho)*(2*d_(mu,nu)*g_(2,6_,rho) - g_(2,6_,rho,nu,mu));

#include gammaidents.frm

id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,rho?,nu?,mu?) = (3*D-8)*g_(1,6_,delta1) * g_(2,6_,delta1);
*id eps=0;
*id D=4;
*Bracket COUPVts,COUPVtb,COUPg,COUPv,dent,denw,deng,xi,ksqr,mt;
*Print;
*.end

**********
***do scalar integrals
**********
id dent^a?*denw^b?*deng^c?*ksqr^d? = intdummy(a,b,c,d);
*Bracket intdummy;
*Print;
.sort

*id intdummy(a?,0,c?,d?) = scalarint(d,a+c) * fac_(a+c-1)/fac_(a-1)/fac_(c-1)*feyn2intTG(a,c,d);
*id intdummy(a?,b?,0,d?) = scalarint(d,a+b) * fac_(a+b-1)/fac_(a-1)/fac_(b-1)*feyn2intTW(a,b,d);
id intdummy(a?,0,c?,d?) = scalarint(d,a+c) * feyn2int(a,c,d,x/xi);
id intdummy(a?,b?,0,d?) = scalarint(d,a+b) * feyn2int(a,b,d,x);
id intdummy(a?,b?,c?,d?) = scalarint(d,a+b+c) * fac_(a+b+c-1)/fac_(a-1)/fac_(b-1)/fac_(c-1) * feyn3int(a,b,c,d);


id scalarint(a?,b?) = (-1)^(a-b)/16/pi_^2 * egamm(a+2) * egamp(b-a-2) / fac_(b-1) * (1+eps+eps^2*ALARMeps) * (mw2*x)^(2+a-b) * (1-eps*log(x));
*id feyn2int(a?,b?,d?,x?) = hypergeom(a+b-d-2,b,a+b,x);

*id egamp(4)=(3+eps)*egamp(3);
*id egamp(3)=(2+eps)*egamp(2);
*id egamp(2)=(1+eps)*egamp(1);
*id egamp(1)=1;
*id egamp(-2) = -(1+eps/2+eps^2*ALARMeps)/2 * egamp(-1);
*id egamp(-1) = -(1+eps+eps^2*ALARMeps) * egamp(0);
*id egamp(0) = 1/eps;

*id egamm(4)=(3-eps)*egamm(3);
*id egamm(3)=(2-eps)*egamm(2);
*id egamm(2)=(1-eps)*egamm(1);
*id egamm(1)=1;
*id egamm(-2) = -(1-eps/2+eps^2*ALARMeps)/2 * egamm(-1);
*id egamm(-1) = -(1-eps+eps^2*ALARMeps) * egamm(0);
*id egamm(0) = -1/eps;

*id D = 4-2*eps;

*if ( count(eps,1) > -1 ) discard;
*Bracket hypergeom,feyn3int;
*Print;
*.end
.sort

***TODO remove*****
*if ( count(COUPcHWB,1) == 0 ) discard;
*if ( count(eps,1) > 0 ) discard;
*Bracket feyn2intTW,feyn2intTG,feyn3int;
*Print;
*.end

id feyn2int(a?,b?,d?,x?) = hypergeom(a+b-d-2,b,a+b,x);
*id feyn2int(a?,b?,d?,x?) = fac_(a-1) * fac_(b-1)/fac_(a+b-1) * hypergeom(a+b-d-2,b,a+b,x);

.sort

*if ( count(eps,1) > -1 ) discard;
*Bracket hypergeom,feyn3int;
*Print;
*.end

****************
***feyn2int sub
****************
id feyn2intTW(2,1,0)=(x*(-1+x-log(x)))/(-1+x)^2+ALARMeps*eps;
id feyn2intTW(1,2,0)=(x*(1-x+x*log(x)))/(-1+x)^2+ALARMeps*eps;
id feyn2intTG(2,1,0)=(x*(x-xi+xi*log(xi/x)))/(x-xi)^2+ALARMeps*eps;
id feyn2intTG(1,2,0)=(x*(-x+xi+x*log(x/xi)))/(x-xi)^2+ALARMeps*eps;
id feyn2intTW(2,1,1)=1/2+((3-4*x+x^2+2*log(x))*eps)/(4*(-1+x)^2)+ALARMeps*eps^2;
id feyn2intTW(1,1,0)=1+((-1+x-log(x))*eps)/(-1+x)+ALARMeps*eps^2;
id feyn2intTW(1,2,1)=1/2+((1-4*x+3*x^2+2*log(x)-4*x*log(x))*eps)/(4*(-1+x)^2)+ALARMeps*eps^2;
id feyn2intTG(2,1,1)=1/2+((x^2-4*x*xi+3*xi^2+2*xi^2*log(x)-2*xi^2*log(xi))*eps)/(4*(x-xi)^2)+ALARMeps*eps^2;
id feyn2intTG(1,1,0)=1+(1-(xi*log(x/xi))/(x-xi))*eps+ALARMeps*eps^2;
id feyn2intTG(1,2,1)=1/2+((3*x^2-4*x*xi+xi^2-4*x*xi*log(x)+2*xi^2*log(x)+4*x*xi*log(xi)-2*xi^2*log(xi))*eps)/(4*(x-xi)^2)+ALARMeps*eps^2;
****************
***feyn3int sub
****************

id feyn3int(2,1,1,1)=-((x*(x*(-1+xi)*(-(-1+x)*(x-xi)+(x+(-2+x)*xi)*log(x))-(-1+x)^2*xi^2*log(xi)))/(2*(-1+x)^2*(x-xi)^2*(-1+xi)))+ALARMeps*eps;
*id feyn3int(2,1,1,2)=1/6;
*id feyn3int(1,2,1,2)=1/6;
id feyn3int(1,2,2,0)=(x^3*(x*(-1+xi)^3*log(x)+(-1+x)*(-(-1+xi)*(1-2*x+xi)*(-x+xi)+(-1+x)*(x+x*xi-2*xi^2)*log(xi))))/(2*(-1+x)^2*(x-xi)^2*(-1+xi)^3)+ALARMeps*eps;
id feyn3int(1,1,1,0)=(x*(x*(-1+xi)*log(x)-(-1+x)*xi*log(xi)))/((-1+x)*(x-xi)*(-1+xi))+ALARMeps*eps;
id feyn3int(1,1,2,1)=(x^3*(-1+xi)^2*log(x)-(-1+x)*x*xi*((x-xi)*(-1+xi)+(x*(-2+xi)+xi)*log(xi)))/(2*(-1+x)*(x-xi)^2*(-1+xi)^2)+ALARMeps*eps;
id feyn3int(1,2,1,1)=(x^3*(-1+xi)^2*log(x)-(-1+x)*x*(-(x-xi)*(-1+xi)+(-1+x)*xi^2*log(xi)))/(2*(-1+x)^2*(x-xi)*(-1+xi)^2)+ALARMeps*eps;
id feyn3int(1,2,2,-1)=-((x^4*((-1+xi)^3*xi*log(x)-(-1+x)*((-1+xi)*(-x+xi)*(1+xi^2-x*(1+xi))+(-1+x)*xi*(-1-2*x+3*xi)*log(xi))))/(6*(-1+x)^2*(x-xi)^2*(-1+xi)^3*xi))+ALARMeps*eps;
id feyn3int(1,2,2,2)=(x^4*(-1+xi)^3*log(x)-(-1+x)*x*((x-xi)*(-1+xi)*(x-xi+(-1+x)*xi^2)+(-1+x)*xi^2*(x*(-3+xi)+2*xi)*log(xi)))/(6*(-1+x)^2*(x-xi)^2*(-1+xi)^3)+ALARMeps*eps;

id feyn3int(2,1,1,2)=1/6+((-11*x^2+16*x^3-5*x^4+22*x*xi-27*x^2*xi+5*x^4*xi-11*xi^2+27*x^2*xi^2-16*x^3*xi^2+11*xi^3-22*x*xi^3+11*x^2*xi^3-6*x^2*log(x)-6*xi^2*log(x)+6*xi^3*log(x)-12*x*xi^3*log(x)+6*x^2*xi^3*log(x)-12*x*xi*log(xi)-6*xi^3*log(xi)+12*x*xi^3*log(xi)-6*x^2*xi^3*log(xi)+12*x*xi*log(x*xi))*eps)/(36*(-1+x)^2*(x-xi)^2*(-1+xi))+ALARMeps*eps^2;
id feyn3int(1,2,1,2)=1/6+(((-x+2*x^2-x^3*(-1+xi)^2+xi-3*x^2*xi+3*x*xi^2+(-2+xi)*xi^2-2*x*xi^3+x^2*xi^3)*(-(11/36)+log(xi)/6)+1/6*(-x+x^2+xi-x^2*xi+x*xi^2-2*x^2*log(x)+2*x*xi^3*log(x)-x^2*xi^3*log(x)-xi^2*(1+(-2+xi)*log(x)-2*log(xi))-2*x^2*log(xi)+x^3*(-1+xi)^2*log(xi)+x*log(x*xi)-xi*log(x*xi)+3*x^2*xi*log(x*xi)-3*x*xi^2*log(x*xi)))*eps)/((-1+x)^2*(x-xi)*(-1+xi)^2)+ALARMeps*eps^2;


id feyn3int(1,1,1,1)=1/2+(((x-x^2-xi+x^2*xi+xi^2-x*xi^2)*(3/4-log(xi)/2)+1/2*(xi^2*log(x)-x*xi^2*log(x)-x^2*log(xi)+x^2*xi*log(xi)+x*log(x*xi)-xi*log(x*xi)))*eps)/((-1+x)*(x-xi)*(-1+xi))+ALARMeps*eps^2;
id feyn3int(1,1,2,2)=1/6+(((-x^3*(-1+xi)^2-2*x*xi+3*x*xi^2-x*xi^4+xi^2*(1-2*xi+xi^2)+x^2*(1-3*xi^2+2*xi^3))*(-(11/36)+log(xi)/6)+1/6*(x*xi^2-x*xi^4-3*x*xi^2*log(x)+x*xi^4*log(x)-x^2*(1-3*xi^2+2*xi^3)*log(x)+x^3*(-1+xi)^2*log(xi)-x^2*(xi^2-xi^3+log(xi))-xi^2*(xi-xi^2+(1-2*xi+xi^2)*log(x)+log(xi))+2*x*xi*log(x*xi)))*eps)/((-1+x)*(x-xi)^2*(-1+xi)^2)+ALARMeps*eps^2;

id feyn3int(1,2,2,3)=1/24+(1/((-1+x)^2*(x-xi)^2*(-1+xi)^3))*((x^4*(-1+xi)^3-4*x^3*xi+x^2*xi*(-1+8*xi)+x*xi*(2-4*xi-4*xi^2)-2*x*xi^3*(-2-2*xi+xi^2)+xi^2*(-1+3*xi-3*xi^2+xi^3)-2*x^3*(-1-2*xi^3+xi^4)+x^2*(-1-8*xi^3+xi^4+xi^5))*(25/288-log(xi)/24)+1/24*(x^3*xi+x^3*(2+4*xi^3-2*xi^4)*log(x)+x^2*(-1-8*xi^3+xi^4+xi^5)*log(x)+xi^3*(x*(-1-xi+2*xi^2)+x*(4+4*xi-2*xi^2)*log(x))+x^2*(1+2*xi^3-xi^4-xi^5-log(xi))+x^4*(-1+xi)^3*log(xi)+x^3*(-1-xi^3+xi^4+2*log(xi))+xi^2*(1-xi+xi^2-xi^3+(-1+3*xi-3*xi^2+xi^3)*log(x)-log(xi)+3*xi*log(xi))-4*x^3*xi*log(x*xi)+x*(-x*xi*(-1+2*xi)-x*(1-8*xi)*xi*log(x*xi))+x*xi*(-2+xi+xi^2+(2-4*xi-4*xi^2)*log(x*xi))))*eps+ALARMeps*eps^2;

id feyn3int(1,1,3,1)=(1/(2*(x-xi)^3*(-1+xi)^3))*x^2*(-(-1+xi)*(-x+xi)*(x*(-3+xi)+xi+xi^2)-(2*x^2*(-1+xi)^3*log(x))/(-1+x)+2*(x^2+x*(-3+xi)*xi^2+xi^3)*log(xi))+ALARMeps*eps;
id feyn3int(1,2,1,0)=(x^2*(-x*(-1+xi)^2*log(x)+(-1+x)*(x-(1+x)*xi+xi^2+(-1+x)*xi*log(xi))))/((-1+x)^2*(x-xi)*(-1+xi)^2)+ALARMeps*eps;
id feyn3int(1,2,2,1)=-((x^2*(-1+xi+(x^2*(-1+xi)^3*log(x)+(-1+x)*(-x*(x-xi)*(-1+2*x-xi)*(-1+xi)-(-1+x)*xi*(-2*x+xi+xi^2)*log(xi)))/(2*(-1+x)^2*(x-xi)^2)))/(-1+xi)^3)+ALARMeps*eps;
id feyn3int(1,2,3,2)=(1/(6*(x-xi)^3*(-1+xi)^4))*x^2*(((x-xi)*(-1+xi)*(-xi^2*(5+xi)+x^2*(-2+(-5+xi)*xi)+x*xi*(9+xi*(2+xi))))/(-1+x)-(2*x^3*(-1+xi)^4*log(x))/(-1+x)^2+2*xi*(3*x^2+xi^2*(1+2*xi)+x*xi*(-3+(-4+xi)*xi))*log(xi))+ALARMeps*eps;
id feyn3int(1,3,1,1)=(x^2*((-1+x)*(-1+xi)*(-x+xi)*(-1-xi+x*(-1+3*xi))-2*x^2*(-1+xi)^3*log(x)+2*(-1+x)^3*xi^2*log(xi)))/(2*(-1+x)^3*(x-xi)*(-1+xi)^3)+ALARMeps*eps;
id feyn3int(1,3,1,2)=((-1+x)*x*(x-xi)*(-1+xi)*(1-3*xi+x*(-3+5*xi))+2*x^4*(-1+xi)^3*log(x)-2*(-1+x)^3*x*xi^3*log(xi))/(6*(-1+x)^3*(x-xi)*(-1+xi)^3)+ALARMeps*eps;
id feyn3int(1,3,2,2)=-((x^2*((-1+x)*(-1+xi)*(-x+xi)*(-x*(1+x)+xi+x*(-2+5*x)*xi+(5+x*(-9+2*x))*xi^2)+2*x^3*(-1+xi)^4*log(x)-2*(-1+x)^3*xi^2*(-3*x+xi*(2+xi))*log(xi)))/(6*(-1+x)^3*(x-xi)^2*(-1+xi)^4))+ALARMeps*eps;
id feyn3int(2,1,2,1)=-((x^2*(-x*(-1+xi)^2*(x^2+(-2+x)*xi)*log(x)+(-1+x)*((x-xi)*(-1+xi)*(-xi+x*(-1+2*xi))+(-1+x)*xi*(x*(-2+xi)+xi^2)*log(xi))))/(2*(-1+x)^2*(x-xi)^3*(-1+xi)^2))+ALARMeps*eps;
id feyn3int(2,1,2,2)=-((x*(x^2*(-1+xi)^2*(x-3*xi+2*x*xi)*log(x)+(-1+x)*((-1+xi)*(-x+xi)*(x^2*(-1+xi)-xi^2+x*xi^2)-(-1+x)*xi^2*(xi+x*(-3+2*xi))*log(xi))))/(6*(-1+x)^2*(x-xi)^3*(-1+xi)^2))+ALARMeps*eps;
id feyn3int(2,2,1,1)=(x^2*((-1+x)*(-1+xi)*(-x+xi)*(x*(-2+xi)+xi)+x*(x+x^2-2*xi)*(-1+xi)^2*log(x)-(-1+x)^3*xi^2*log(xi)))/(2*(-1+x)^3*(x-xi)^2*(-1+xi)^2)+ALARMeps*eps;
id feyn3int(2,2,1,2)=(x*((-1+xi)*((-1+x)*(x-xi)*(x*(-1+x*(-1+xi))+xi)-x^2*(-1+xi)*(-3*xi+x*(2+xi))*log(x))+(-1+x)^3*xi^3*log(xi)))/(6*(-1+x)^3*(x-xi)^2*(-1+xi)^2)+ALARMeps*eps;
id feyn3int(1,1,2,0)=(x^2*(-x*(-1+xi)^2*log(x)+(-1+x)*((x-xi)*(-1+xi)+(-x+xi^2)*log(xi))))/((-1+x)*(x-xi)^2*(-1+xi)^2)+ALARMeps*eps;
.sort

****************
****expand euler gamma functions in eps, where egamp(n) = \Gamma(n+\epsilon) and egamm(n) = \Gamma(n-\epsilon)
***************
id egamp(4)=(3+eps)*egamp(3);
id egamp(3)=(2+eps)*egamp(2);
id egamp(2)=(1+eps)*egamp(1);
id egamp(1)=1;
id egamp(-2) = -(1+eps/2+eps^2*ALARMeps)/2 * egamp(-1);
id egamp(-1) = -(1+eps+eps^2*ALARMeps) * egamp(0);
id egamp(0) = 1/eps;

id egamm(5)=(4-eps)*egamm(4);
id egamm(4)=(3-eps)*egamm(3);
id egamm(3)=(2-eps)*egamm(2);
id egamm(2)=(1-eps)*egamm(1);
id egamm(1)=1;
id egamm(-2) = -(1-eps/2+eps^2*ALARMeps)/2 * egamm(-1);
id egamm(-1) = -(1-eps+eps^2*ALARMeps) * egamm(0);
id egamm(0) = -1/eps;

id D = 4-2*eps;
.sort

***********8
***hypergeom sub
************

id hypergeom(0,2,3,x?) = 1 + eps/2*(1-4*x+3*x^2+2*log(x)-4*x*log(x))*oneoveroneminus(x)^2+ALARMeps*eps^2;
id hypergeom(0,1,3,x?) =1+(3-4*x+x^2+2*log(x))*eps/2*oneoveroneminus(x)^2+ALARMeps*eps^2;
id hypergeom(1,3,4,x?) = 3/2* x*(1-4*x+3*x^2-2*x^2*log(x))*oneoveroneminus(x)^3+ALARMeps*eps;
id hypergeom(1,4,5,x?) = -2/3* x*(-2+9*x-18*x^2+11*x^3-6*x^3*log(x))*oneoveroneminus(x)^4+ALARMeps*eps;

id hypergeom(1,2,3,x?) = -2*x*oneoveroneminus(x)^2 * (-1+x-x*log(x))+ALARMeps*eps;

id hypergeom(1,1,3,x?) = (2*x*(-1+x-log(x)))*oneoveroneminus(x)^2+ALARMeps*eps;
id hypergeom(1,1,2,x?) =  -(x*log(x))*oneoveroneminus(x)+ALARMeps*eps;

id hypergeom(0,1,2,x?) = 1+((-1+x-log(x))*eps)/(-1+x)+ALARMeps*eps^2;

id hypergeom(1,2,4,x?) = -(3*x*(-1+x^2-2*x*log(x)))*oneoveroneminus(x)^3 +ALARMeps*eps;
id hypergeom(2,2,4,x?) = (6*x^2*(-2 + 2*x - log(x) - x*log(x)))*oneoveroneminus(x)^3 +ALARMeps*eps;
id hypergeom(2,3,4,x?) = -(3*(-x^2+x^4-2*x^3*log(x)))*oneoveroneminus(x)^3 +ALARMeps*eps;

id hypergeom(0,0,2,x?) = 1;
id hypergeom(-1,0,2,x?) = 1;

id hypergeom(2,2,3,x?)=(2*x^2*(-1+x-log(x)))/(-1+x)^2;
id hypergeom(3,3,4,x?)=(3*x^3*(3-4*x+x^2+2*log(x)))/(2*(-1+x)^3);
id hypergeom(2,3,5,x?)=-((6*x^2*(-1-4*x+5*x^2-4*x*log(x)-2*x^2*log(x)))/(-1+x)^4);
id hypergeom(3,3,5,x?)=(6*x^3*(-5+4*x+x^2-2*log(x)-4*x*log(x)))/(-1+x)^4;

**********
***discard terms of order \epsilon^1 or (1/M^2)^2 or higher
***********
*id ALARMeps=0;
if ( count(eps,1) > 0 ) discard;
if ( count(mb,1) > 0 ) discard;
*Bracket feyn3int,hypergeom;
*Print;
*.end
*if ( count(mw2,1) < -2 ) discard;
*if ( count(d6dum,1) != 1 ) discard;
*id d6dum=1;

id COUPv = 2*mw/COUPg;
id mw^2 = mw2;
id mz2 = mw2/COUPcosW^2;
id COUPg = COUPe/COUPsinW;
id COUPgp = COUPe/COUPcosW;

id oneoveroneminus(x?) = 1/(1-x);
.sort
#ifdef `feyngauge'
  #write <HRMEout> "FcZ=%e\nFdZ=%e\nFeZ=%e\nFfZ=%e\nFgZ=%e\nFhZ=%e\nFbox=%e\nGa=%e\nGb=%e\nGc=%e\nGd=%e\nGe=%e\nGj=%e\nGk=%e\nGbox1=%e\nGbox2=%e\nFloopcqqZ=%e\nHa=%e\nHb=%e\nHc=%e\nHd=%e\nHe=%e\nHf=%e\nHg=%e\nHh=%e\nFcZnu=%e\nFdZnu=%e\nFeZnu=%e\nFfZnu=%e\nFgZnu=%e\nFhZnu=%e\nFboxnu=%e\nGanu=%e\nGbnu=%e\nGcnu=%e\nGdnu=%e\nGenu=%e\nGjnu=%e\nGknu=%e\nGbox1nu=%e\nGbox2nu=%e\nFloopcqqZnu=%e\nKa=%e\nKb=%e\nKc=%e\nKd=%e\nKe=%e\nKf=%e\nKg=%e\nKh=%e\nKi=%e\nKj=%e\nKk=%e\nKl=%e\nKm=%e\nKn=%e\nKo=%e\nKp=%e\n",FcZ,FdZ,FeZ,FfZ,FgZ,FhZ,Fbox,Ga,Gb,Gc,Gd,Ge,Gj,Gk,Gbox1,Gbox2,FloopcqqZ,Ha,Hb,Hc,Hd,He,Hf,Hg,Hh,FcZnu,FdZnu,FeZnu,FfZnu,FgZnu,FhZnu,Fboxnu,Ganu,Gbnu,Gcnu,Gdnu,Genu,Gjnu,Gknu,Gbox1nu,Gbox2nu,FloopcqqZnu,Ka,Kb,Kc,Kd,Ke,Kf,Kg,Kh,Ki,Kj,Kk,Kl,Km,Kn,Ko,Kp
#endif
#ifndef `feyngauge'
  #write <HRMEXIout> "FcZ=%e\nFdZ=%e\nFeZ=%e\nFfZ=%e\nFgZ=%e\nFhZ=%e\nFbox=%e\nGa=%e\nGb=%e\nGc=%e\nGd=%e\nGe=%e\nGj=%e\nGk=%e\nGbox1=%e\nGbox2=%e\nFloopcqqZ=%e\nHa=%e\nHb=%e\nHc=%e\nHd=%e\nHe=%e\nHf=%e\nHg=%e\nHh=%e\nFcZnu=%e\nFdZnu=%e\nFeZnu=%e\nFfZnu=%e\nFgZnu=%e\nFhZnu=%e\nFboxnu=%e\nGanu=%e\nGbnu=%e\nGcnu=%e\nGdnu=%e\nGenu=%e\nGjnu=%e\nGknu=%e\nGbox1nu=%e\nGbox2nu=%e\nFloopcqqZnu=%e\nKa=%e\nKb=%e\nKc=%e\nKd=%e\nKe=%e\nKf=%e\nKg=%e\nKh=%e\nKi=%e\nKj=%e\nKk=%e\nKl=%e\nKm=%e\nKn=%e\nKo=%e\nKp=%e\n",FcZ,FdZ,FeZ,FfZ,FgZ,FhZ,Fbox,Ga,Gb,Gc,Gd,Ge,Gj,Gk,Gbox1,Gbox2,FloopcqqZ,Ha,Hb,Hc,Hd,He,Hf,Hg,Hh,FcZnu,FdZnu,FeZnu,FfZnu,FgZnu,FhZnu,Fboxnu,Ganu,Gbnu,Gcnu,Gdnu,Genu,Gjnu,Gknu,Gbox1nu,Gbox2nu,FloopcqqZnu,Ka,Kb,Kc,Kd,Ke,Kf,Kg,Kh,Ki,Kj,Kk,Kl,Km,Kn,Ko,Kp
#endif

.sort

Bracket feyn3int,hypergeom;
*Bracket g_;
Print;

.end
************
***manual eom relations for dipole diagrams
************
id g_(1,6_,epspol,p1,p2) = 2*p1.p2*g_(1,6_,epspol)-g_(1,6_,epspol,p2,p1);
id g_(1,6_,epspol,p2,p1) = mb*g_(1,6_,epspol,p2);

id g_(1,6_,p1,p2,epspol) = 2*p1.p2*g_(1,6_,epspol)-g_(1,6_,p2,p1,epspol);
id g_(1,6_,p2,p1,epspol) = 0;

id g_(1,6_,epspol,p2) = 2*p2.epspol*g_(1,6_) - g_(1,6_,p2,epspol);
id g_(1,6_,p2,epspol) = 0;

id g_(1,6_,p2) = 0;
id g_(1,6_,p1) = g_(1,6_)*mb;

id p1.p1 = mb^2;
id p2.p2 = 0;

id mz2 = mw2/COUPcosW^2;
id COUPg = COUPe/COUPsinW;
id COUPgp = COUPe/COUPcosW;

************
***final print
*************
*Bracket p1,p2,p3,M2,COUPA,delABdelCD,delADdelBC,sigACsigBD,delACdelBD;
Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,COUPgp,COUPsinW,COUPcosW,oneoveroneminusx,xi,mt,pi_,mw2;
Print;
.sort

.end

