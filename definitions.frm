#define feyngauge "feyngauge"

Dimension D;
Vectors p1,p2,p3,p4,p5,k,r1,r2,r3,kint,epspol;
Symbols dent,denw,deng,xi,denwdentdent,dentdenwdenw,feynX,feynY,feynZ,dentdenw,dentdeng,dentdent,
COUPuH,COUPdH,COUPe,COUPg,COUPVts,COUPVtb,COUPv,COUPgp,COUPsinW,COUPcosW,
COUPcphil1,COUPcphil3,COUPcphie,COUPcphiq1,COUPcphiu,COUPcphiq3,COUPcphid,COUPclq1,COUPclq3,COUPceu,COUPced,COUPclu,COUPcld,COUPcqe,COUPcW,COUPcll,COUPcHWB,COUPcHD,COUPcqq,COUPcqqprime,COUPcuG,COUPcdG,COUPcquqd
delABdelCD,delADdelBC,
sigABsigCD,sigADsigBC,
epsABepsCD,
delACdelBD,
sigACsigBD,
delADdelBCelim, 
intDUMMY,x1,x2,x3,mw,mw2,mz2,mb,qsqr,x,logx,ALARMk,ALARMeps,MARK1,MARK2,MARK3,DEL,a,b,c,d,n,ksqr,eps,mt,oneoveroneminusx,d6dum,lightdum,
shiftdum,GF,deltagZ,deltamz2,deltag1,deltag2,deltasW2,deltaGF,deltaalphaover2alpha,deltakappaZ,deltag1Z,deltag1gamma,deltakappagamma,
g1,g2,vsqrcHWB,
DAVEVC,DAVEdeltag1overg1,DAVEdeltasW2, POLESVC, POLESdeltamz2
fielddum, COUPGF, sophiedeltag1,sophiedeltag2,sophiedeltas2,COUPsophiecHWB,sophiedeltaGF,sophiedeltamz2,COUPcuW,COUPcdW,COUPcuB, COUPcdB, COUPcud,
deltanunuZ,deltaWGA,oldrulesdum,newrulesdum,deltaenew,deltagcosWnew,deltagpsinWnew,deltagZnew,deltamZ2new,deltamW2new,deltag1new,deltavnew,deltag2new,
newshiftdum,deltaWGZ,deltaGGZ,deltaWWZ,deltattZL,deltattZR,deltabbZL,deltabbZR,deltassZL,deltassZR,deltallZL,deltallZR,DAVE1,DAVE2,DAVE3,DAVE4,DAVE5,DAVE6;
Indices mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,gamma4,delta,alpha1,beta1,mu1,nu1,sigma1,delta1;
Functions Vhhs,Vuqh,Vquh,Vdqh,Vqdh,Vhbarhb,Vhbarhw,VGpwmgam,VwpGmgam,VGpGmgam,Vsbarsgam,Vbbarbgam,Vlbarlgam,VbbarbZ,
VEFTHu,VEFTHuprime,VEFTHd,VEFTHdprime,VEFTHq1,VEFTHqprime1,VEFTHq3,VEFTHqprime3,VEFTboxH,VEFTH,VEFTR,VEFTT,VsbartW,VsbartW2,VtbarbW,VtbarbW2,Vwpwmgam,VGpGmlbarl,Vtbartgam,VsbartG,VsbartG2,VtbarbG,VtbarbG2,VGpWmlbarl,VWpGmlbarl,Vtbartlbarl,Vbbarblbarl,Vsbarslbarl,Vtbarblbarnu,Vsbartnubarl,Vtbartsbarb,Vtbarbsbart,VsbartGmgam,VtbarbGpgam,Vbbarbglue
VGpGmZ,VGpwmZ,VwpGmZ,VtbartZ,VnubarnuZ,Vtbartglue,VwpwmZ,VlbarnuW,VnubarlW,VlbarlZ,VsbartGmZ,VtbarbGpZ,VsbarsZ,VlbarnuG,VnubarlG,Vsbartnil,Vtbarbnil,VtbarbWgam,VsbartWgam,VtbarbWZ,VsbartWZ,VtbarbGpglue,VsbartGmglue 
Vtbartnubarnu,Vsbarsnubarnu,Vbbarbnubarnu,VWpGmnubarnu,VGpWmnubarnu,VGpGmnubarnu, VsbaruG, VsbaruW, VubarbW, VubarbG
PRh,PRq,PRu,PRu2,PRd,PRs,PRb,PRwplus,PRt,PRt2,PRGplus,PRnu,PRz,PRl,
MHHHHpartialb,
MHHHHpartialw,
scalarint,feyn2int,feyn3int,egamp,egamm,hypergeom,denT1,denT2,denW1,denW2,denG1,denG2,intdummy,log,oneoveroneminus,twwint,wttint,twint,twgint,tgint,
feyn2intTG,feyn2intTW;

******
***hack to stop immediate contraction of indices
*******
Index gamma1=0;
Index gamma2=0;
Index gamma3=0;
Index gamma4=0;
Index mu=0;
Index nu=0;
Index rho=0;
Index sigma=0;
Index alpha=0;
Index beta=0;
Index gamma=0;
Index delta=0;
Index delta1=0;
Index mu1=0;
Index alpha1=0;
Index sigma1=0;
Index beta1=0;
Index nu1=0;
