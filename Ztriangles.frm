Dimension 4;
Vectors p1,p2,p3,k,epsstar,vectorcurrent;
Symbols mt,mb,scalarcharge,davedavedave,C00,C12,C11,C22,C1,C2,C0,d,yt,yb,q,G,Gp,v;
Indices mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3;
Functions it44,it43,it22,it23,it62,it61mu,it61nu,it60,it33,it32,it12,it11,it54,it53mu,it53nu,it52;

*hack to stop immediate contraction of indices
Index gamma1=0;
Index gamma2=0;
Index gamma3=0;
Index mu=0;
Index nu=0;
Index rho=0;
Index sigma=0;
Index alpha=0;
Index beta=0;
Index gamma=0;


***note g_(1,6_)= 2*PR and g_(1,7_) = 2*PL
Local FC = C0 * (-i_/2 *G* g_(1,beta,7_)/2) * (
it54(mu,nu,alpha,beta) * g_(1,nu,gamma,7_,mu) +
it53mu(mu,alpha,beta) * g_(1,gamma,7_,mu) +
it53nu(nu,alpha,beta) * g_(1,nu,gamma,7_) +
it52(alpha,beta) * g_(1,gamma,7_) ) *
(-i_ *G* g_(1,alpha,7_)/2 ) * ((Gp^2-3*G^2) * i_/12) * epsstar(gamma) +
C0 * (-i_/2 *G* g_(1,beta,7_)/2) * (
it54(mu,nu,alpha,beta) * g_(1,nu,gamma,6_,mu) +
it53mu(mu,alpha,beta) * g_(1,gamma,6_,mu) +
it53nu(nu,alpha,beta) * g_(1,nu,gamma,6_) +
it52(alpha,beta) * g_(1,gamma,6_) ) *
(-i_ *G* g_(1,alpha,7_)/2 ) * (4*Gp^2 * i_/12) * epsstar(gamma);
Local FD = C0 * i_ * (yt * g_(1,6_)/2 ) * (it62(mu,nu) * g_(1,nu,gamma,7_,mu) + it61mu(mu) * g_(1,gamma,7_,mu) + it61nu(nu) * g_(1,nu,gamma,7_) + it60 * g_(1,gamma,7_) ) * (i_) * (yt * g_(1,7_)/2 - yb * g_(1,6_)/2 ) * epsstar(gamma) * ((Gp^2-3*G^2) * i_/12)+ C0 * i_ * (yt * g_(1,6_)/2 ) * (it62(mu,nu) * g_(1,nu,gamma,6_,mu) + it61mu(mu) * g_(1,gamma,6_,mu) + it61nu(nu) * g_(1,nu,gamma,6_) + it60 * g_(1,gamma,6_) ) * (i_) * (yt * g_(1,7_)/2 - yb * g_(1,6_)/2 ) * epsstar(gamma) * (4*Gp^2* i_/12);
*Local FE = C0 * (i_/2) * g_(1,beta,7_)/2 *
* (it44(mu,alpha,beta,gamma) * g_(1,mu) + it43(alpha,beta,gamma) ) * (-i_) * i_ *
*g_(1,alpha,7_)/2 * epsstar(gamma);
Local FF = C0 * (-i_ /sqrt_(2) *G* g_(1,beta,7_)/2) * (it23(mu,beta,gamma) * g_(1,mu) + it22(beta,gamma)) * (i_) * (yt * g7_(1)/2 - yb * g6_(1)/2) * (-i_/2*Gp^2*G*v) * d_(gamma,rho) * epsstar(rho);
Local FG = C0 * i_ * (yt * g_(1,6_)/2 ) * (it33(mu,alpha,gamma) * g_(1,mu) + it32(alpha,gamma) ) * (-i_) *G* g_(1,alpha,7_)/2/sqrt_(2) * (-i_/2*Gp^2*G*v) * d_(gamma,rho) * epsstar(rho);
Local FH = C0 * i_ * (yt * g_(1,6_)/2 ) * (it12(mu,gamma) * g_(1,mu) + it11(gamma)) * (i_) * (yt * g_(1,7_)/2 - yb * g_(1,6_)/2 ) * epsstar(gamma) * ((Gp^2-G^2)*i_/2);
Local FE = C0 * (-i_/2) *G* g_(1,beta,7_)/2 * (it44(mu,alpha,beta,gamma) * g_(1,mu) + it43(alpha,beta,gamma)) * (-i_ *G* g_(1,alpha,7_)/2) * i_*G^2 * epsstar(gamma);

.sort
Print;
*id it44(mu?,alpha?,beta?,gamma?) = -i_ * (
*p3(gamma3) * ( p2(gamma2) + k(gamma2) ) * ( p1(gamma3) - k(gamma3) ) - 
*p3(gamma2) * ( p2(gamma3) + k(gamma3) ) * ( p1(gamma1) - k(gamma1) ) +
*d_(gamma1,gamma2) * ( p2(gamma3) + k(gamma3) ) * p3(sigma) * ( p1(sigma) - k(sigma) ) 
*d_(gamma1,gamma2) * ( p2(gamma3) + k(gamma3) ) * p3(sigma) * ( p1(sigma) - k(sigma) ) 
*
*);
id it54(mu?,nu?,alpha?,beta?) = i_ * (k(mu)-p1(mu)) * (k(nu) + p2(nu)) * d_(alpha,beta);
id it53mu(mu?,alpha?,beta?) = -i_ * (k(mu)-p1(mu)) * (mt) * d_(alpha,beta);
id it53nu(nu?,alpha?,beta?) = -i_ * (mt) * (k(nu) + p2(nu)) * d_(alpha,beta);
id it52(alpha?,beta?) = i_ * mt^2 * d_(alpha,beta);
*id it44(mu?,alpha?,beta?,gamma?) = -i_ * (
* d_(gamma1,gamma2) * ( 2*k(gamma) + p2(gamma) - p1(gamma) ) +
* d_(gamma1,gamma) * ( -k(gamma2) + 2*p1(gamma2) + p2(gamma2) ) +
* d_(gamma,gamma2) * ( -k(gamma1) - 2*p2(gamma1) - p1(gamma1) )
*) * k(mu) * d_(alpha,gamma1) * d_(beta,gamma2);
*id it43(alpha?,beta?,gamma?) = -i_ * (
* d_(gamma1,gamma2) * ( 2*k(gamma) + p2(gamma) - p1(gamma) ) +
* d_(gamma1,gamma) * ( -k(gamma2) + 2*p1(gamma2) + p2(gamma2) ) +
* d_(gamma,gamma2) * ( -k(gamma1) - 2*p2(gamma1) - p1(gamma1) )
*) * mt * d_(alpha,gamma1) * d_(beta,gamma2);
id it44(mu?,alpha?,beta?,gamma?) = -i_ * (
d_(gamma1,gamma2) * (2* k(gamma) + p2(gamma) - p1(gamma) ) +
d_(gamma1,gamma) * (-k(gamma2) + 2*p1(gamma2) + p2(gamma2) ) +
d_(gamma,gamma2) * (-k(gamma1) - 2*p2(gamma1) - p1(gamma1) )
) * k(mu) * d_(alpha,gamma1) * d_(beta,gamma2);
id it43(alpha?,beta?,gamma?) = -i_ * (
d_(gamma1,gamma2) * (2* k(gamma) + p2(gamma) - p1(gamma) ) +
d_(gamma1,gamma) * (-k(gamma2) + 2*p1(gamma2) + p2(gamma2) ) +
d_(gamma,gamma2) * (-k(gamma1) - 2*p2(gamma1) - p1(gamma1) )
) * mt * d_(alpha,gamma1) * d_(beta,gamma2);
id it23(mu?,beta?,gamma?) = i_ * k(mu) * d_(beta,gamma);
id it22(beta?,gamma?) = i_ * mt * d_(beta,gamma);
id it62(mu?,nu?) = -i_ * (k(mu) - p1(mu)) * (k(nu) + p2(nu));
id it61mu(mu?) = i_ * (k(mu) - p1(mu)) * mt;
id it61nu(nu?) = i_ * mt * (k(nu) + p2(nu));
id it60 = -i_ * mt^2;
id it33(mu?,alpha?,gamma?) = i_ * k(mu) * d_(alpha,gamma);
id it32(alpha?,gamma?) = i_ * mt * d_(alpha,gamma);
id it12(mu?,gamma?) = -i_ * (2 * k(gamma) + p2(gamma) - p1(gamma)) * k(mu);
id it11(gamma?) = -i_ * (2 * k(gamma) + p2(gamma) - p1(gamma)) * mt;

Print;
.sort
id k(mu?) * k(nu?) = 1/C0 * (C00 * d_(mu,nu) + C12 * (p1(mu)*p2(nu) + p1(nu)*p2(mu)) + C11 * p1(mu) * p1(nu) + C22 * p2(mu) * p2(nu) );
id k(mu?) = 1/C0 * (p1(mu) * C1 + p2(mu) * C2);
Bracket C0,C1,C2,C00,C11,C12,C22;
*.sort
Print;
.sort
sum mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3;
Print;
.sort


****these subs have to be done in a specific order!! all longer gamma matrix products must be eliminated first. Within that, subs with fewer wildcards come first
****five gammas (plus proj matrix)
id g_(1,6_,alpha?,mu?,gamma?,nu?,alpha?) = -2 * g_(1,6_,nu,gamma,mu) + (4-d)*g_(1,6_,mu,gamma,nu);
****three gammas (plus projection matrix)
id g_(1,6_,gamma?,p1,p1) = mb^2 * g_(1,6_,gamma);
id g_(1,6_,gamma?,p1,p2) = 2 * p1(mu) * p2(mu) * g_(1,6_,gamma) - 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,gamma?,p2,p1) = 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,gamma?,p2,p2) = 0;
id g_(1,6_,p1,p1,gamma?) = mb^2 * g_(1,6_,gamma);
id g_(1,6_,p1,p2,gamma?) = 2 * p1(mu) * p2(mu) * g_(1,6_,gamma); 
id g_(1,6_,p2,p1,gamma?) = 0;
id g_(1,6_,p2,p2,gamma?) = 0;
id g_(1,6_,p1,gamma?,p2) = -2 * p1(mu) * p2(mu) * g_(1,6_,gamma) + 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,p1,gamma?,p1) = - mb^2 * g_(1,6_,gamma) + 2 * mb * p1(gamma) * g_(1,6_);
id g_(1,6_,p2,gamma?,p2) = 0;
id g_(1,6_,p2,gamma?,p1) = 0;
id g_(1,6_,mu?,nu?,mu?) = -(d-2)*g_(1,6_,nu);
id g_(1,6_,mu?,mu?,nu?) = d*g_(1,6_,nu);
id g_(1,6_,nu?,mu?,mu?) = d*g_(1,6_,nu);
****two gammas (plus projection matrix)
id g_(1,6_,gamma?,p2) = 2 * p2(gamma) * g_(1,6_);
id g_(1,6_,p1,gamma?) = 2 * p1(gamma) * g_(1,6_) - mb * g_(1,6_,gamma);
id g_(1,6_,gamma?,p1) = mb * g_(1,6_,gamma);
id g_(1,6_,p2,gamma?) = 0;
id g_(1,6_,mu?,mu?) = d * g_(1,6_);
****one gamma (plus projection matrix)
id g_(1,6_,p1) = mb * g_(1,6_);
id g_(1,6_,p2) = 0;

sum mu;
Print;
.sort

id g_(1,6_,epsstar) = 2*vectorcurrent;
id g_(1,6_) = 2*scalarcharge;
id p1.p2 = (q^2-mb^2)/2;
id q^2=0;
id mb=0;
id yb=0;

Bracket scalarcharge,vectorcurrent;
Print;
.end

