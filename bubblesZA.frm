#include definitions.frm

**********
****define diagrams
***********

Local FaWZ = VsbarsZ(p2,p1,p3+p4,nu) * PRs(p1,mu)/mb^2 * VsbartW(-p1,k,p1-k,alpha) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta) * PRz(p3+p4,nu,sigma)/(-mz2) * VlbarlZ(p4,p3,-p3-p4,sigma) * dent * denw;
Local FaGZ = VsbarsZ(p2,p1,p3+p4,nu) * PRs(p1,mu)/mb^2 * VsbartG(-p1,k,p1-k) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRz(-p3-p4,nu,sigma)/(-mz2) * VlbarlZ(p4,p3,p3+p4,sigma) * dent * deng;
Local Gg = VsbartGmZ(p2,k,p1-k,p3+p4,nu) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRz(p3+p4,nu,sigma)/(-mz2) * VlbarlZ(p4,p3,-p3-p4,sigma) * dent * deng;
Local GgW = VsbartWZ(p2,k,p1-k,p3+p4,alpha,nu) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta) * PRz(p3+p4,nu,sigma)/(-mz2) * VlbarlZ(p4,p3,-p3-p4,sigma) * dent * denw;
Local GiW = Vsbarslbarl(p2,p1,p4,p3) * PRs(p1,mu)/mb^2 * VsbartW(-p1,k,p1-k,alpha) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta) * dent * denw;
Local GiG = Vsbarslbarl(p2,p1,p4,p3) * PRs(p1,mu)/mb^2 * VsbartG(-p1,k,p1-k) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * dent * deng;
Local FloopcqqZ = Vsbartnil(p2,k-p1-p2,nu) * PRt(k-p1-p2,mu)* VtbartZ(-k+p1+p2,k,p3,alpha)*PRt(k,rho)*Vtbarbnil(-k,p1,nu)*epspol(alpha)*dent * dent;
Local FaWZnu = VsbarsZ(p2,p1,p3+p4,nu) * PRs(p1,mu)/mb^2 * VsbartW(-p1,k,p1-k,alpha) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta) * PRz(p3+p4,nu,sigma)/(-mz2) * VnubarnuZ(p4,p3,-p3-p4,sigma) * dent * denw;
Local FaGZnu = VsbarsZ(p2,p1,p3+p4,nu) * PRs(p1,mu)/mb^2 * VsbartG(-p1,k,p1-k) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRz(-p3-p4,nu,sigma)/(-mz2) * VnubarnuZ(p4,p3,p3+p4,sigma) * dent * deng;
Local Ggnu = VsbartGmZ(p2,k,p1-k,p3+p4,nu) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * PRz(p3+p4,nu,sigma)/(-mz2) * VnubarnuZ(p4,p3,-p3-p4,sigma) * dent * deng;
Local GgWnu = VsbartWZ(p2,k,p1-k,p3+p4,alpha,nu) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta) * PRz(p3+p4,nu,sigma)/(-mz2) * VnubarnuZ(p4,p3,-p3-p4,sigma) * dent * denw;
Local GiWnu = Vsbarsnubarnu(p2,p1,p4,p3) * PRs(p1,mu)/mb^2 * VsbartW(-p1,k,p1-k,alpha) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta) * dent * denw;
Local GiGnu = Vsbarsnubarnu(p2,p1,p4,p3) * PRs(p1,mu)/mb^2 * VsbartG(-p1,k,p1-k) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * dent * deng;


*********
**apply feynman rules for vertices (V) and propagators (P)
*********

#include feynRules.frm
.sort
*******
**remove dim 8 and higher pieces
*******

if ( count(d6dum,1) > 1 ) discard;
.sort

********
***shift integration momentum
*******
id k(mu?) = kint(mu) + (1-feynX)*p1(mu);

Print;
.sort
*******
***reduce tensors of loop momenta to products of k-squared(ksqr) and metric factors (d_(??,??))
*******
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)*kint(beta?)=
ksqr^3*(
d_(mu,nu)*(d_(rho,sigma)*d_(alpha,beta)+d_(rho,alpha)*d_(sigma,beta)+d_(rho,beta)*d_(alpha,sigma))
+d_(mu,rho)*(d_(nu,sigma)*d_(alpha,beta)+d_(nu,alpha)*d_(sigma,beta)+d_(nu,beta)*d_(alpha,sigma))
+d_(mu,sigma)*(d_(rho,nu)*d_(alpha,beta)+d_(rho,alpha)*d_(nu,beta)+d_(rho,beta)*d_(alpha,nu))
+d_(mu,alpha)*(d_(rho,sigma)*d_(nu,beta)+d_(rho,nu)*d_(sigma,beta)+d_(rho,beta)*d_(nu,sigma))
+d_(mu,beta)*(d_(rho,sigma)*d_(alpha,nu)+d_(rho,alpha)*d_(sigma,nu)+d_(rho,nu)*d_(alpha,sigma))
) * (1+eps/2)/4 * (1+eps/3)/6 * (1+eps/4)/8 + eps^2*ALARMeps;

id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)=0;
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)=ksqr^2*
( d_(mu,nu)*d_(rho,sigma)+d_(mu,rho)*d_(nu,sigma)+d_(mu,sigma)*d_(nu,rho) )*
(1+eps/2)/4 * (1+eps/3)/6 + eps^2*ALARMeps;
id kint(mu?)*kint(nu?)*kint(rho?)=0;
id kint(mu?)*kint(nu?)=ksqr*d_(mu,nu) * (1+eps/2)/4 + eps^2*ALARMeps;
id kint(mu?)=0;

.sort

*********feynman gauge
#ifdef `feyngauge'
id dentdeng=dentdenw;
id deng = denw;
id xi=1;
#endif

**********remove SM piece
*if ( count(d6dum,1) != 1 ) discard;
*id d6dum=1;
********************

**************
***contract and sum over lorentz indices
************
sum mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,gamma4,alpha1,beta1,delta1,mu1,nu1,sigma1;
.sort

**********
***apply momentum cons 
**********

id sqrt_(2)^2 = 2;

**********
***gamma idents 1
******
#include gammaidents.frm
#include gammaidents2.frm

.sort


*****TODO check!!!
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,alpha?,beta?,rho?) = g_(1,6_,mu,nu,rho)*(2*d_(beta,rho)*g_(2,6_,alpha) - g_(2,6_,alpha,rho,beta));
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,alpha?,rho?,beta?) = g_(1,6_,mu,nu,rho)*(2*d_(alpha,rho)*g_(2,6_,beta) - g_(2,6_,rho,alpha,beta));
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,rho?,mu?,nu?) = g_(1,6_,mu,nu,rho)*(2*d_(mu,nu)*g_(2,6_,rho) - g_(2,6_,rho,nu,mu));

#include gammaidents.frm

id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,rho?,nu?,mu?) = 4*g_(1,6_,delta1) * g_(2,6_,delta1) + ALARMeps*MARK1*eps;
*id eps=0;
*id D=4;
*Bracket COUPVts,COUPVtb,COUPg,COUPv,dent,denw,deng,xi,ksqr,mt;
*Print;
*.end

**********
***do scalar integrals
**********
*id ksqr^d? = scalarint(d,2) * ( DEL^(2+d-2) );
if ( count(deng,1) > 0 );
  if ( count(denw,1) > 0 );
    id ksqr^d?*dent*denw^a?*deng^b? = fac_(a+b)/fac_(a-1)/fac_(b-1) * dent * denw^a * deng^b * scalarint(d,1+a+b) * feynY^(a-1) * feynZ^(b-1) * ( DEL^(2+d-1-a-b) - (feynX*(1-feynX)*mb^2)*(2+d-eps-1-a-b)*DEL^(2+d-2-a-b)/mw2/x );
  else;
    id ksqr^d?*dent*deng^b? = b * dent * deng^b * scalarint(d,1+b) * feynY^(b-1) * ( DEL^(2+d-1-b) - (feynX*(1-feynX)*mb^2)*(2+d-eps-1-b)*DEL^(2+d-2-b)/mw2/x );
  endif;
else;
  id ksqr^d?*dent*denw^a? = a * dent * denw^a * scalarint(d,1+a) * feynY^(a-1) * ( DEL^(2+d-1-a) - (feynX*(1-feynX)*mb^2)*(2+d-eps-1-a)*DEL^(2+d-2-a)/mw2/x );
endif;
*Bracket intdummy;
*Print;
.sort

id scalarint(a?,b?) = (-1)^(a-b)/16/pi_^2 * egamm(a+2) * egamp(b-a-2) / fac_(b-1) * (1+eps+eps^2*ALARMeps) * (mw2*x)^(2+a-b) * (1-eps*log(x));
.sort

if ( count(deng,1) > 0 );
  if ( count(denw,1) > 0 );
    id feynX^a?*feynY^b?*feynZ^c?*DEL^d? = twgint(a,b,c,d); 
  else;
    id feynX^a?*feynY^b?*DEL^d? = tgint(a,b,d); 
  endif;
else;
  id feynX^a?*feynY^b?*DEL^d? = twint(a,b,d); 
endif;

id dent=1;
id denw=1;
id deng=1;
.sort

*Bracket dent,denw,deng,twgint,tgint,twint;
*Print;
*.end

****************
***twint sub
****************
*id twint(0,1,0)=1/2+ALARMeps*eps;
*id twint(1,2,-1)=(x*(1-6*x+3*x^2+2*x^3-6*x^2*log(x)))/(6*(-1+x)^4)+ALARMeps*eps;

*id twint(0,1,0)=1/2+((1-4*x+3*x^2+2*log(x)-4*x*log(x))*eps)/(4*(-1+x)^2)+ALARMeps*eps^2;
*id twint(1,2,-1)=(x-6*x^2+3*x^3+2*x^4-6*x^3*log(x))/(6*(-1+x)^4)+((5*x-54*x^2+27*x^3+22*x^4+6*x*log(x)-36*x^2*log(x)-36*x^3*log(x)-18*x^3*log(x)^2)*eps)/(36*(-1+x)^4)+ALARMeps*eps^2;
*id twint(0,0,0)=1+((-1+x-log(x))*eps)/(-1+x)+ALARMeps*eps^2;
*id twint(1,1,-1)=(x*(-1+x^2-2*x*log(x)))/(2*(-1+x)^3)+(x*(-3+3*x^2-2*log(x)-4*x*log(x)-2*x*log(x)^2)*eps)/(4*(-1+x)^3)+ALARMeps*eps^2;

*id twint(1,0,0)=1/2+((3-4*x+x^2+2*log(x))*eps)/(4*(-1+x)^2)+ALARMeps*eps^2;
*id twint(2,1,-1)=(x*(2+3*x-6*x^2+x^3+6*x*log(x)))/(6*(-1+x)^4)+(x*(22+27*x-54*x^2+5*x^3+12*log(x)+54*x*log(x)+18*x*log(x)^2)*eps)/(36*(-1+x)^4)+ALARMeps*eps^2;
*id twint(0,0,0)=1+((-1+x-log(x))*eps)/(-1+x)+ALARMeps*eps^2;
*id twint(1,1,-1)=(x*(-1+x^2-2*x*log(x)))/(2*(-1+x)^3)+(x*(-3+3*x^2-2*log(x)-4*x*log(x)-2*x*log(x)^2)*eps)/(4*(-1+x)^3)+ALARMeps*eps^2;

*id twint(1,0,0)=1/2+((3-4*x+x^2+2*log(x))*eps)/(4*(-1+x)^2)+ALARMeps*eps^2;
*id twint(2,0,-1)=(x*(3-4*x+x^2+2*log(x)))/(2*(-1+x)^3)+(x*(7-8*x+x^2+6*log(x)+2*log(x)^2)*eps)/(4*(-1+x)^3)+ALARMeps*eps^2;
*id twint(3,0,-1)=(x*(-11+18*x-9*x^2+2*x^3-6*log(x)))/(6*(-1+x)^4)+(x*(-85+108*x-27*x^2+4*x^3-66*log(x)-18*log(x)^2)*eps)/(36*(-1+x)^4)+ALARMeps*eps^2;
*id twint(0,0,0)=1+((-1+x-log(x))*eps)/(-1+x)+ALARMeps*eps^2;
*id twint(1,0,-1)=(x*(-1+x-log(x)))/(-1+x)^2+(x*(-1+x-log(x)-log(x)^2/2)*eps)/(-1+x)^2+ALARMeps*eps^2;
id twgint(1,0,0,0)=1/6+((-11*x^2+16*x^3-5*x^4+22*x*xi-27*x^2*xi+5*x^4*xi-11*xi^2+27*x^2*xi^2-16*x^3*xi^2+11*xi^3-22*x*xi^3+11*x^2*xi^3-6*x^2*log(x)-6*xi^2*log(x)+6*xi^3*log(x)-12*x*xi^3*log(x)+6*x^2*xi^3*log(x)-12*x*xi*log(xi)-6*xi^3*log(xi)+12*x*xi^3*log(xi)-6*x^2*xi^3*log(xi)+12*x*xi*log(x*xi))*eps)/(36*(-1+x)^2*(x-xi)^2*(-1+xi))+ALARMeps*eps^2;
id twgint(1,0,0,1)=(1+2*x+xi)/(24*x)+((-13*x^2+27*x^4-14*x^5+26*x*xi-40*x^3*xi+14*x^5*xi-13*xi^2+40*x^3*xi^2-27*x^4*xi^2+13*xi^4-26*x*xi^4+13*x^2*xi^4-12*x^2*log(x)-12*xi^2*log(x)+12*xi^4*log(x)-24*x*xi^4*log(x)+12*x^2*xi^4*log(x)-24*x*xi*log(xi)-12*xi^4*log(xi)+24*x*xi^4*log(xi)-12*x^2*xi^4*log(xi)+24*x*xi*log(x*xi))*eps)/(288*(-1+x)^2*x*(x-xi)^2*(-1+xi))+ALARMeps*eps^2;
id twgint(2,0,0,0)=1/12+((25*x^3-45*x^4+27*x^5-7*x^6-75*x^2*xi+128*x^3*xi-60*x^4*xi+7*x^6*xi+75*x*xi^2-108*x^2*xi^2+60*x^4*xi^2-27*x^5*xi^2-25*xi^3+108*x^2*xi^3-128*x^3*xi^3+45*x^4*xi^3+25*xi^4-75*x*xi^4+75*x^2*xi^4-25*x^3*xi^4+12*x^3*log(x)-36*x^2*xi*log(x)-12*xi^3*log(x)+12*xi^4*log(x)-36*x*xi^4*log(x)+36*x^2*xi^4*log(x)-12*x^3*xi^4*log(x)-36*x*xi^2*log(xi)-12*xi^4*log(xi)+36*x*xi^4*log(xi)-36*x^2*xi^4*log(xi)+12*x^3*xi^4*log(xi)+36*x*xi^2*log(x*xi))*eps)/(144*(-1+x)^3*(x-xi)^3*(-1+xi))+ALARMeps*eps^2;
id twgint(3,0,0,0)=1/20+((-137*x^4+288*x^5-252*x^6+128*x^7-27*x^8+548*x^3*xi-1125*x^4*xi+900*x^5*xi-350*x^6*xi+27*x^8*xi-822*x^2*xi^2+1600*x^3*xi^2-1000*x^4*xi^2+350*x^6*xi^2-128*x^7*xi^2+548*x*xi^3-900*x^2*xi^3+1000*x^4*xi^3-900*x^5*xi^3+252*x^6*xi^3-137*xi^4+900*x^2*xi^4-1600*x^3*xi^4+1125*x^4*xi^4-288*x^5*xi^4+137*xi^5-548*x*xi^5+822*x^2*xi^5-548*x^3*xi^5+137*x^4*xi^5-60*x^4*log(x)-60*xi^4*log(x)+60*xi^5*log(x)-240*x*xi^5*log(x)+360*x^2*xi^5*log(x)-240*x^3*xi^5*log(x)+60*x^4*xi^5*log(x)-240*x^3*xi*log(xi)+360*x^2*xi^2*log(xi)-240*x*xi^3*log(xi)-60*xi^5*log(xi)+240*x*xi^5*log(xi)-360*x^2*xi^5*log(xi)+240*x^3*xi^5*log(xi)-60*x^4*xi^5*log(xi)+240*x^3*xi*log(x*xi)-360*x^2*xi^2*log(x*xi)+240*x*xi^3*log(x*xi))*eps)/(1200*(-1+x)^4*(x-xi)^4*(-1+xi))+ALARMeps*eps^2;
id twgint(0,0,0,0)=1/2+(((x-x^2-xi+x^2*xi+xi^2-x*xi^2)*(3/4-log(xi)/2)+1/2*(xi^2*log(x)-x*xi^2*log(x)-x^2*log(xi)+x^2*xi*log(xi)+x*log(x*xi)-xi*log(x*xi)))*eps)/((-1+x)*(x-xi)*(-1+xi))+ALARMeps*eps^2;

id twgint(3,0,0,-1)=(x*((26*x-57*x^2+42*x^3-11*x^4+6*log(x))/(-1+x)^4+(11*x^4-42*x^3*xi+57*x^2*xi^2-26*x*xi^3-6*xi^4*log(x)+6*xi^4*log(xi))/(x-xi)^4))/(24*(-1+xi))+(1/(-1+xi))*x*(25/288*((26*x-57*x^2+42*x^3-11*x^4+6*log(x))/(-1+x)^4+(11*x^4-42*x^3*xi+57*x^2*xi^2-26*x*xi^3-6*xi^4*log(x)+6*xi^4*log(xi))/(x-xi)^4)+1/24*((3*(-3*x+8*x^2-7*x^3+2*x^4+log(x)*log(x)))/(-1+x)^4-(1/((x-xi)^4))*3*(2*x^4-7*x^3*xi+8*x^2*xi^2-3*x*xi^3+xi^4*log(x)*log(x)-2*xi^4*log(x)*log(xi)+xi^4*log(xi)*log(xi))))*eps+ALARMeps*eps^2;

id twgint(4,0,0,-1)=(1/(-1+xi))*x*((-137+300*x-300*x^2+200*x^3-75*x^4+12*x^5-60*log(x))/(300*(-1+x)^5)+(1/(300*(x-xi)^5))*(-12*x^5+75*x^4*xi-200*x^3*xi^2+300*x^2*xi^3-300*x*xi^4+137*xi^5+60*xi^5*log(x)-60*xi^5*log(xi)))+(1/(-1+xi))*x*((-12019+18000*x-9000*x^2+4000*x^3-1125*x^4+144*x^5-8220*log(x)-1800*log(x)^2)/(18000*(-1+x)^5)+(1/(18000*(x-xi)^5))*(-144*x^5+1125*x^4*xi-4000*x^3*xi^2+9000*x^2*xi^3-18000*x*xi^4+12019*xi^5+8220*xi^5*log(x)+1800*xi^5*log(x)^2-8220*xi^5*log(xi)-3600*xi^5*log(x)*log(xi)+1800*xi^5*log(xi)^2))*eps+ALARMeps*eps^2;
id twgint(5,0,0,-1)=(1/(-1+xi))*x*((147-360*x+450*x^2-400*x^3+225*x^4-72*x^5+10*x^6+60*log(x))/(360*(-1+x)^6)+(1/(360*(x-xi)^6))*(-10*x^6+72*x^5*xi-225*x^4*xi^2+400*x^3*xi^3-450*x^2*xi^4+360*x*xi^5-147*xi^6-60*xi^6*log(x)+60*xi^6*log(xi)))+(1/(-1+xi))*x*((1/(21600*(-1+x)^6))*(13489-21600*x+13500*x^2-8000*x^3+3375*x^4-864*x^5+100*x^6+8820*log(x)+1800*log(x)^2)+(1/(21600*(x-xi)^6))*(-100*x^6+864*x^5*xi-3375*x^4*xi^2+8000*x^3*xi^3-13500*x^2*xi^4+21600*x*xi^5-13489*xi^6-8820*xi^6*log(x)-1800*xi^6*log(x)^2+8820*xi^6*log(xi)+3600*xi^6*log(x)*log(xi)-1800*xi^6*log(xi)^2))*eps+ALARMeps*eps^2;


id tgint(1,0,0)=1/2+((x^2-4*x*xi+3*xi^2+2*xi^2*log(x)-2*xi^2*log(xi))*eps)/(4*(x-xi)^2)+ALARMeps*eps^2;
id tgint(2,0,-1)=(x*(x^2-4*x*xi+3*xi^2+2*xi^2*log(x/xi)))/(2*(x-xi)^3)+(x*(x^2-8*x*xi+7*xi^2+6*xi^2*log(x/xi)+2*xi^2*log(x/xi)^2)*eps)/(4*(x-xi)^3)+ALARMeps*eps^2;
id tgint(3,0,-1)=(x*(2*x^3-9*x^2*xi+18*x*xi^2-11*xi^3-6*xi^3*log(x/xi)))/(6*(x-xi)^4)+(x*(4*x^3-27*x^2*xi+108*x*xi^2-85*xi^3-66*xi^3*log(x/xi)-18*xi^3*log(x/xi)^2)*eps)/(36*(x-xi)^4)+ALARMeps*eps^2;
id tgint(0,0,0)=1+(1-(xi*log(x/xi))/(x-xi))*eps+ALARMeps*eps^2;
id tgint(1,0,-1)=(x*(x-xi-xi*log(x/xi)))/(x-xi)^2+(x*(x-xi-xi*log(x/xi)-1/2*xi*log(x/xi)^2)*eps)/(x-xi)^2+ALARMeps*eps^2;

id twint(1,0,0)=1/2+((3-4*x+x^2+2*log(x))*eps)/(4*(-1+x)^2)+ALARMeps*eps^2;
id twint(2,0,-1)=(x*(3-4*x+x^2+2*log(x)))/(2*(-1+x)^3)+(x*(7-8*x+x^2+6*log(x)+2*log(x)^2)*eps)/(4*(-1+x)^3)+ALARMeps*eps^2;
id twint(3,0,-1)=(x*(-11+18*x-9*x^2+2*x^3-6*log(x)))/(6*(-1+x)^4)+(x*(-85+108*x-27*x^2+4*x^3-66*log(x)-18*log(x)^2)*eps)/(36*(-1+x)^4)+ALARMeps*eps^2;
id twint(0,0,0)=1+((-1+x-log(x))*eps)/(-1+x)+ALARMeps*eps^2;
id twint(1,0,-1)=(x*(-1+x-log(x)))/(-1+x)^2+(x*(-1+x-log(x)-log(x)^2/2)*eps)/(-1+x)^2+ALARMeps*eps^2;
id twint(5,0,-2)=(x*(24+130*x-240*x^2+120*x^3-40*x^4+6*x^5+120*x*log(x)))/(24*(-1+x)^6)+(x*(-144+1295*x-1440*x^2+360*x^3-80*x^4+9*x^5+780*x*log(x)+360*x*log(x)^2)*eps)/(144*(-1+x)^6)+ALARMeps*eps^2;
id twint(4,0,-2)=(x*(-3-10*x+18*x^2-6*x^3+x^4-12*x*log(x)))/(3*(-1+x)^5)+(x*(9-55*x+54*x^2-9*x^3+x^4-30*x*log(x)-18*x*log(x)^2)*eps)/(9*(-1+x)^5)+ALARMeps*eps^2;
id twint(3,0,-2)=(x*(2+3*x-6*x^2+x^3+6*x*log(x)))/(2*(-1+x)^4)+(x*(-4+15*x-12*x^2+x^3+6*x*log(x)+6*x*log(x)^2)*eps)/(4*(-1+x)^4)+ALARMeps*eps^2;

id twgint(2,0,0,-1)=(x*(-((5*x-8*x^2+3*x^3+2*log(x))/(-1+x)^3)-(-3*x^3+8*x^2*xi-5*x*xi^2-2*xi^3*log(x)+2*xi^3*log(xi))/(x-xi)^3))/(6*(-1+xi))+(x*(-(11/36)*((5*x-8*x^2+3*x^3+2*log(x))/(-1+x)^3+(-3*x^3+8*x^2*xi-5*x*xi^2-2*xi^3*log(x)+2*xi^3*log(xi))/(x-xi)^3)+1/6*(-((-x+2*x^2-x^3+log(x)^2)/(-1+x)^3)-(x^3-2*x^2*xi+x*xi^2-xi^3*log(x)^2+2*xi^3*log(x)*log(xi)-xi^3*log(xi)^2)/(x-xi)^3))*eps)/(-1+xi)+ALARMeps*eps^2;
id twgint(1,0,0,-1)=(x*((3-4*x+x^2+2*log(x))/(4*(-1+x)^2)+(-x^2+4*x*xi-3*xi^2-2*xi^2*log(x)+2*xi^2*log(xi))/(4*(x-xi)^2)))/(-1+xi)+(x*((7-8*x+x^2+6*log(x)+2*log(x)^2)/(8*(-1+x)^2)+(-x^2+8*x*xi-7*xi^2-6*xi^2*log(x)-2*xi^2*log(x)^2+6*xi^2*log(xi)+4*xi^2*log(x)*log(xi)-2*xi^2*log(xi)^2)/(8*(x-xi)^2))*eps)/(-1+xi)+ALARMeps*eps^2;
id twgint(0,0,0,0)=1/2+(((x-x^2-xi+x^2*xi+xi^2-x*xi^2)*(3/4-log(xi)/2)+1/2*(xi^2*log(x)-x*xi^2*log(x)-x^2*log(xi)+x^2*xi*log(xi)+x*log(x*xi)-xi*log(x*xi)))*eps)/((-1+x)*(x-xi)*(-1+xi))+ALARMeps*eps^2;


id twint(2,0,0)=1/3+((-11+18*x-9*x^2+2*x^3-6*log(x))*eps)/(18*(-1+x)^3)+ALARMeps*eps^2;
id twint(0,0,1)=(1+x)/(2*x)+((-1+x^2-2*log(x))*eps)/(4*(-1+x)*x)+ALARMeps*eps^2;

id tgint(0,0,1)=(x+xi)/(2*x)+((x^2-xi^2-2*xi^2*log(x)+2*xi^2*log(xi))*eps)/(4*x*(x-xi))+ALARMeps*eps^2;
id tgint(2,0,0)=1/3+((2*x^3-9*x^2*xi+18*x*xi^2-11*xi^3-6*xi^3*log(x)+6*xi^3*log(xi))*eps)/(18*(x-xi)^3)+ALARMeps*eps^2;

id tgint(0,1,-1)=(x*(-x+xi+x*log(x/xi)))/(x-xi)^2+ALARMeps*eps;
id tgint(1,1,-2)=-((x^2*(2*(x-xi)+(x+xi)*log(xi/x)))/(x-xi)^3)+ALARMeps*eps;
id tgint(1,1,-1)=(x*(x^2-xi^2+2*x*xi*log(xi/x)))/(2*(x-xi)^3)+ALARMeps*eps;
id tgint(2,1,-2)=(x^2*((x-xi)*(x+5*xi)+2*xi*(2*x+xi)*log(xi/x)))/(2*(x-xi)^4)+ALARMeps*eps;
id tgint(3,1,-2)=(x^2*((x-xi)*(x^2-8*x*xi-17*xi^2)+6*xi^2*(3*x+xi)*log(x/xi)))/(6*(x-xi)^5)+ALARMeps*eps;
id tgint(4,0,-1)=(x*((x-xi)*(3*x^3-13*x^2*xi+23*x*xi^2-25*xi^3)+12*xi^4*log(x/xi)))/(12*(x-xi)^5)+ALARMeps*eps;
id twint(0,1,-1)=(x*(1-x+x*log(x)))/(-1+x)^2+ALARMeps*eps;
id twint(1,1,-2)=(x^2*(2-2*x+(1+x)*log(x)))/(-1+x)^3+ALARMeps*eps;
id twint(1,1,-1)=(x*(-1+x^2-2*x*log(x)))/(2*(-1+x)^3)+ALARMeps*eps;
id twint(2,1,-2)=(x^2*(-5+4*x+x^2-2*(1+2*x)*log(x)))/(2*(-1+x)^4)+ALARMeps*eps;
id twint(3,1,-2)=(x^2*(17-9*x-9*x^2+x^3+6*(1+3*x)*log(x)))/(6*(-1+x)^5)+ALARMeps*eps;
id twint(4,0,-1)=(x*((-1+x)*(-25+x*(23+x*(-13+3*x)))+12*log(x)))/(12*(-1+x)^5)+ALARMeps*eps;
id twgint(0,0,1,-1)=(x^3*(-1+xi)^2*log(x)-(-1+x)*x*xi*((x-xi)*(-1+xi)+(x*(-2+xi)+xi)*log(xi)))/(2*(-1+x)*(x-xi)^2*(-1+xi)^2)+ALARMeps*eps;
id twgint(0,1,0,-1)=(x^3*(-1+xi)^2*log(x)-(-1+x)*x*(-(x-xi)*(-1+xi)+(-1+x)*xi^2*log(xi)))/(2*(-1+x)^2*(x-xi)*(-1+xi)^2)+ALARMeps*eps;
id twgint(1,0,1,-1)=-((x*(x^2*(-1+xi)^2*(x-3*xi+2*x*xi)*log(x)+(-1+x)*((-1+xi)*(-x+xi)*(x^2*(-1+xi)-xi^2+x*xi^2)-(-1+x)*xi^2*(xi+x*(-3+2*xi))*log(xi))))/(6*(-1+x)^2*(x-xi)^3*(-1+xi)^2))+ALARMeps*eps;
id twgint(1,0,1,-2)=-((x^2*(-x*(-1+xi)^2*(x^2+(-2+x)*xi)*log(x)+(-1+x)*((x-xi)*(-1+xi)*(-xi+x*(-1+2*xi))+(-1+x)*xi*(x*(-2+xi)+xi^2)*log(xi))))/(2*(-1+x)^2*(x-xi)^3*(-1+xi)^2))+ALARMeps*eps;
id twgint(1,1,0,-1)=(x*((-1+xi)*((-1+x)*(x-xi)*(x*(-1+x*(-1+xi))+xi)-x^2*(-1+xi)*(-3*xi+x*(2+xi))*log(x))+(-1+x)^3*xi^3*log(xi)))/(6*(-1+x)^3*(x-xi)^2*(-1+xi)^2)+ALARMeps*eps;
id twgint(1,1,0,-2)=(x^2*((-1+x)*(-1+xi)*(-x+xi)*(x*(-2+xi)+xi)+x*(x+x^2-2*xi)*(-1+xi)^2*log(x)-(-1+x)^3*xi^2*log(xi)))/(2*(-1+x)^3*(x-xi)^2*(-1+xi)^2)+ALARMeps*eps;
id twgint(2,0,1,-2)=(x^2*((-1+x)*(x-xi)*(-1+xi)*(-x^2*(1+x)+x*(5+(-2+x)*x)*xi+(2+x*(-9+5*x))*xi^2)-2*x*(-1+xi)^2*(x^3+2*(-2+x)*x^2*xi+(3+(-3+x)*x)*xi^2)*log(x)+2*(-1+x)^3*xi^2*(xi^2+x*(-3+2*xi))*log(xi)))/(6*(-1+x)^3*(x-xi)^4*(-1+xi)^2)+ALARMeps*eps;
id twgint(2,1,0,-2)=(x^2*((-1+x)*(x-xi)*(-1+xi)*(-x^2*(5+x)+x*(9+x*(2+x))*xi+(-2+(-5+x)*x)*xi^2)-2*x*(-1+xi)^2*(x^2*(1+2*x)+x*(-3+(-4+x)*x)*xi+3*xi^2)*log(x)+2*(-1+x)^4*xi^3*log(xi)))/(6*(-1+x)^4*(x-xi)^3*(-1+xi)^2)+ALARMeps*eps;
id twgint(3,0,0,-2)=(x^2*((-1+x)*(x-xi)*(-1+xi)*(x^4-11*xi^2-5*x^3*(1+xi)+7*x*xi*(1+xi)-2*x^2*(1+(-5+xi)*xi))+6*(-1+xi)*(x^4+(-4+x)*x^3*xi+x^2*(6+(-4+x)*x)*xi^2-xi^3)*log(x)-6*(-1+x)^4*xi^3*log(xi)))/(6*(-1+x)^4*(x-xi)^4*(-1+xi))+ALARMeps*eps;
id twgint(3,0,1,-2)=(x^2*(6*x*(-1+xi)^2*(2*x^2*(5-2*xi)*xi^2-4*xi^3+6*x*xi^3+x^3*xi*(-5-10*xi+xi^2)+x^4*(1+2*xi+3*xi^2))*log(x)+(-1+x)*(x^6*(-1+xi)^2-6*(-1+xi)*xi^4-x^5*(-1+xi)^2*(5+9*xi)+4*x*xi^3*(5-16*xi+11*xi^2)+x^2*xi^2*(-36+59*xi+26*xi^2-49*xi^3)+x^4*(-2+19*xi-35*xi^2+27*xi^3-9*xi^4)+x^3*xi*(12-3*xi-37*xi^2+11*xi^3+17*xi^4)-6*(-1+x)^3*xi^3*(xi^2+x*(-4+3*xi))*log(xi))))/(24*(-1+x)^4*(x-xi)^5*(-1+xi)^2)+ALARMeps*eps;
id twgint(3,0,1,-3)=(x^3*((-1+x)*(x-xi)*(-1+xi)*(2*x^4*(-1+xi)+xi^2*(-17+11*xi)+x*xi*(-8+(33-7*xi)*xi)+x^3*(-5+xi*(-9+20*xi))+x^2*(1+xi*(33+2*(-27+xi)*xi)))+6*(-1+xi)^2*(-x^5+(5-2*x)*x^4*xi+x*(3-2*x*(6+(-4+x)*x))*xi^2+xi^3)*log(x)+6*(-1+x)^4*xi^2*(x*(-3+2*xi)+xi*(-1+2*xi))*log(xi)))/(12*(-1+x)^4*(x-xi)^5*(-1+xi)^2)+ALARMeps*eps;
id twgint(3,1,0,-2)=(x^2*((-1+x)*(x-xi)*(-1+xi)*(x^3*(17-(-8+x)*x)+(-7+x)*x^2*(7+x*(4+x))*xi-(-4+x)*x*(11+x*(8+5*x))*xi^2-2*(3+x*(13+(-5+x)*x))*xi^3)+6*x*(-1+xi)^2*(6*x*xi^2-4*xi^3+2*x^2*xi*(-2+5*xi)+x^3*(1-5*xi*(2+xi))+x^4*(3+xi*(2+xi)))*log(x)-6*(-1+x)^5*xi^4*log(xi)))/(24*(-1+x)^5*(x-xi)^4*(-1+xi)^2)+ALARMeps*eps;
id twgint(3,1,0,-3)=(x^3*(6*(-1+xi)^2*(-2*x^4*(1+x)+x^3*(8-(-5+x)*x)*xi-12*x^2*xi^2+(1+3*x)*xi^3)*log(x)+(-1+x)*((-1+xi)*(-x+xi)*(2*x^2*(1+x*(10+x))-x*(7+x*(54+x*(9+2*x)))*xi+(11+x*(33+(33-5*x)*x))*xi^2+(-17+(-8+x)*x)*xi^3)+6*(-1+x)^4*xi^3*log(xi))))/(12*(-1+x)^5*(x-xi)^4*(-1+xi)^2)+ALARMeps*eps;
id twgint(4,0,0,-2)=(x^2*(-12*(-10*x^2*(-1+xi)*xi^3+(-1+xi)*xi^4+10*x^3*xi^2*(-1+xi^2)-5*x^4*xi*(-1+xi^3)+x^5*(-1+xi^4))*log(x)+(-1+x)*(x^7*(-1+xi)+25*(-1+xi)*xi^4+x*xi^3*(48-25*xi-23*xi^2)+x^6*(5+xi-6*xi^2)+x^2*xi^2*(-36+58*xi-35*xi^2+13*xi^3)+x^5*(-13-19*xi+14*xi^2+18*xi^3)+x^4*(-3+61*xi+14*xi^2-62*xi^3-10*xi^4)+x^3*xi*(16-106*xi+58*xi^2+35*xi^3-3*xi^4)+12*(-1+x)^4*xi^4*log(xi))))/(12*(-1+x)^5*(x-xi)^5*(-1+xi))+ALARMeps*eps;
id twgint(4,0,1,-3)=(x^3*(12*(-1+xi)^2*(4*x*xi^3-20*x^2*xi^3+20*x^3*xi^3+xi^4-5*x^4*xi^2*(-3+2*xi)+2*x^5*xi*(-3-6*xi+xi^2)+x^6*(1+2*xi+3*xi^2))*log(x)+(-1+x)*(x^7*(-1+xi)^2-x^6*(-1+xi)^2*(7+12*xi)+xi^4*(-37+62*xi-25*xi^2)+x*xi^3*(8+67*xi-98*xi^2+23*xi^3)+x^5*(-7+30*xi-63*xi^2+76*xi^3-36*xi^4)+x^2*xi^2*(36+16*xi-273*xi^2+234*xi^3-13*xi^4)+x^3*xi*(-8-188*xi+356*xi^2+7*xi^3-170*xi^4+3*xi^5)+x^4*(1+50*xi+17*xi^2-204*xi^3+92*xi^4+44*xi^5)-12*(-1+x)^4*xi^3*(xi*(-1+2*xi)+x*(-4+3*xi))*log(xi))))/(24*(-1+x)^5*(x-xi)^6*(-1+xi)^2)+ALARMeps*eps;
id twgint(4,1,0,-3)=(x^3*(12*(-1+xi)^2*(20*x^3*xi^2-20*x^2*xi^3+xi^4+4*x*xi^4+5*x^4*xi*(-2+3*xi)+x^6*(3+2*xi+xi^2)-2*x^5*(-1+6*xi+3*xi^2))*log(x)+(-1+x)*(x^7*(-1+xi)^2-x^6*(-1+xi)^2*(11+8*xi)+xi^4*(-25+62*xi-37*xi^2)+x*xi^3*(48-185*xi+166*xi^2-29*xi^3)+x^5*(-47+102*xi-75*xi^2+20*xi^3)+x^2*xi^2*(-36+380*xi-525*xi^2+174*xi^3+7*xi^4)-x^3*xi*(-16+440*xi-660*xi^2+185*xi^3+50*xi^4+xi^5)+x^4*(-3+230*xi-355*xi^2+100*xi^3+20*xi^4+8*xi^5)-12*(-1+x)^5*xi^4*log(xi))))/(24*(-1+x)^6*(x-xi)^5*(-1+xi)^2)+ALARMeps*eps;
id twgint(5,0,0,-2)=(x^2*(60*(15*x^2*(-1+xi)*xi^4-(-1+xi)*xi^5-20*x^3*xi^3*(-1+xi^2)+15*x^4*xi^2*(-1+xi^3)-6*x^5*xi*(-1+xi^4)+x^6*(-1+xi^5))*log(x)+(-1+x)*(3*x^9*(-1+xi)+137*(-1+xi)*xi^5+x*xi^4*(300-137*xi-163*xi^2)+x^8*(17+3*xi-20*xi^2)+x^7*(-43-72*xi+55*xi^2+60*xi^3)+x^2*xi^3*(-300+555*xi-392*xi^2+137*xi^3)+x^6*(77+228*xi+55*xi^2-240*xi^3-120*xi^4)+x^3*xi^2*(200-1240*xi+555*xi^2+548*xi^3-63*xi^4)+x^4*xi*(-75+1055*xi+260*xi^2-945*xi^3-307*xi^4+12*xi^5)+x^5*(12-447*xi-445*xi^2+260*xi^3+555*xi^4+65*xi^5)-60*(-1+x)^5*xi^5*log(xi))))/(60*(-1+x)^6*(x-xi)^6*(-1+xi))+ALARMeps*eps;
id twgint(5,0,1,-3)=(x^3*(-60*(-1+xi)^2*(-5*x*xi^4+30*x^2*xi^4-40*x^3*xi^4-xi^5+5*x^4*xi^3*(-7+6*xi)+3*x^5*xi^2*(7+14*xi-4*xi^2)+x^6*xi*(-7-14*xi-21*xi^2+2*xi^3)+x^7*(1+2*xi+3*xi^2+4*xi^3))*log(x)+(-1+x)*(2*x^9*(-1+xi)^2-x^8*(-1+xi)^2*(13+20*xi)+xi^5*(-197+334*xi-137*xi^2)+x^7*(-1+xi)^2*(47+145*xi+120*xi^2)+x*xi^4*(-25+573*xi-711*xi^2+163*xi^3)+x^2*xi^3*(300+425*xi-2787*xi^2+2199*xi^3-137*xi^4)+x^3*xi^2*(-100-1900*xi+3045*xi^2+1273*xi^3-2381*xi^4+63*xi^5)+x^6*(27-289*xi+77*xi^2+705*xi^3-680*xi^4+160*xi^5)+x^4*xi*(25+750*xi+205*xi^2-2455*xi^3+258*xi^4+1229*xi^5-12*xi^6)-x^5*(3+209*xi-627*xi^2+1295*xi^3-1670*xi^4+540*xi^5+250*xi^6)+60*(-1+x)^5*xi^4*(xi*(-1+2*xi)+x*(-5+4*xi))*log(xi))))/(120*(-1+x)^6*(x-xi)^7*(-1+xi)^2)+ALARMeps*eps;
id twgint(5,1,0,-3)=(x^3*(-60*(-1+xi)^2*(-40*x^3*xi^3+30*x^2*xi^4-xi^5-5*x*xi^5-5*x^4*xi^2*(-6+7*xi)+3*x^5*xi*(-4+14*xi+7*xi^2)+x^7*(4+3*xi+2*xi^2+xi^3)-x^6*(-2+21*xi+14*xi^2+7*xi^3))*log(x)+(-1+x)*(2*x^9*(-1+xi)^2-3*x^8*(-1+xi)^2*(6+5*xi)+xi^5*(-137+334*xi-197*xi^2)+6*x^7*(-1+xi)^2*(17+25*xi+10*xi^2)-6*x*xi^4*(-50+197*xi-184*xi^2+37*xi^3)+6*x^2*xi^3*(-50+535*xi-757*xi^2+259*xi^3+13*xi^4)+x^6*(262-854*xi+662*xi^2+230*xi^3-280*xi^4-20*xi^5)-2*x^3*xi^2*(-100+2440*xi-3635*xi^2+961*xi^3+323*xi^4+11*xi^5)+3*x^4*xi*(-25+1270*xi-1965*xi^2+590*xi^3+61*xi^4+68*xi^5+xi^6)-6*x^5*(-2+259*xi-502*xi^2+295*xi^3-45*xi^4-10*xi^5+5*xi^6)+60*(-1+x)^6*xi^5*log(xi))))/(120*(-1+x)^7*(x-xi)^6*(-1+xi)^2)+ALARMeps*eps;

.sort
****************
****expand euler gamma functions in eps, where egamp(n) = \Gamma(n+\epsilon) and egamm(n) = \Gamma(n-\epsilon)
***************
id egamp(4)=(3+eps)*egamp(3);
id egamp(3)=(2+eps)*egamp(2);
id egamp(2)=(1+eps)*egamp(1);
id egamp(1)=1;
id egamp(-2) = -(1+eps/2+eps^2*ALARMeps)/2 * egamp(-1);
id egamp(-1) = -(1+eps+eps^2*ALARMeps) * egamp(0);
id egamp(0) = 1/eps;

id egamm(4)=(3-eps)*egamm(3);
id egamm(3)=(2-eps)*egamm(2);
id egamm(2)=(1-eps)*egamm(1);
id egamm(1)=1;
id egamm(-2) = -(1-eps/2+eps^2*ALARMeps)/2 * egamm(-1);
id egamm(-1) = -(1-eps+eps^2*ALARMeps) * egamm(0);
id egamm(0) = -1/eps;

id D = 4-2*eps;
.sort

**********
***discard terms of order \epsilon^1 or (1/M^2)^2 or higher
***********
if ( count(eps,1) > 0 ) discard;
*Bracket dent,denw,deng,twgint,tgint,twint,eps;
*Print;
*.end
*if ( count(mw2,1) < -2 ) discard;
*if ( count(d6dum,1) != 1 ) discard;
*id d6dum=1;

id COUPv = 2*mw/COUPg;
id mw^2 = mw2;
id mz2 = mw2/COUPcosW^2;
id COUPg = COUPe/COUPsinW;
id COUPgp = COUPe/COUPcosW;

id oneoveroneminus(x?) = 1/(1-x);
.sort

************
***manual eom relations for dipole diagrams
************
**lepton side
*id g_(2,6_,p3)=0;
*id g_(2,6_,p4)=0;
*id g_(2,7_,p3)=0;
*id g_(2,7_,p4)=0;
*id g_(2,6_,p1)=0;
*id g_(2,6_,p2)=0;
*id g_(2,7_,p1)=0;
*id g_(2,7_,p2)=0;
id g_(2,6_,p3)=0;
id g_(2,6_,p4)=0;
id g_(2,7_,p3)=0;
id g_(2,7_,p4)=0;
*************
id p4=-(p1+p2+p3);

id g_(1,6_,gamma?,p2?,p1) = mb * g_(1,6_,gamma,p2);
id g_(1,6_,p2?,gamma?,p1) = mb * g_(1,6_,p2,gamma);
id g_(1,7_,gamma?,p2?,p1) = mb * g_(1,7_,gamma,p2);
id g_(1,7_,p2?,gamma?,p1) = mb * g_(1,7_,p2,gamma);
****two gammas (plus projection matrix)
id g_(1,6_,gamma?,p2) = 2 * p2(gamma) * g_(1,6_);
id g_(1,6_,p1,gamma?) = 2 * p1(gamma) * g_(1,6_) - mb * g_(1,6_,gamma);
id g_(1,6_,gamma?,p1) = mb * g_(1,6_,gamma);
id g_(1,6_,p2,gamma?) = 0;
id g_(1,7_,gamma?,p2) = 2 * p2(gamma) * g_(1,7_);
id g_(1,7_,p1,gamma?) = 2 * p1(gamma) * g_(1,7_) - mb * g_(1,7_,gamma);
id g_(1,7_,gamma?,p1) = mb * g_(1,7_,gamma);
id g_(1,7_,p2,gamma?) = 0;
****one gamma (plus projection matrix)
id g_(1,6_,p1) = mb * g_(1,6_);
id g_(1,6_,p2) = 0;
id g_(1,7_,p1) = mb * g_(1,7_);
id g_(1,7_,p2) = 0;

.sort

id g_(2,7_,p2) = -g_(2,7_,p1);
id g_(2,6_,p2) = -g_(2,6_,p1);

.sort
id p1.p1 = mb^2;
id p2.p2 = 0;
.sort
****************
***keep only first order in qsqr and mb^2
***************
id qsqr = lightdum^2*qsqr;
id mb = lightdum*mb;
if ( count(lightdum,1) > 2 ) discard;
id lightdum = 1;


*Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,COUPgp,COUPsinW,COUPcosW,oneoveroneminusx,xi,mt,pi_,mw2;
.sort
#ifdef `feyngauge'
  #write <bubbleZAout> "FaWZ=%e\nFaGZ=%e\nGg=%e\nGgW=%e\nGiW=%e\nGiG=%e\nFloopcqqZ=%e\nFaWZnu=%e\nFaGZnu=%e\nGgnu=%e\nGgWnu=%e\nGiWnu=%e\nGiGnu=%e\n",FaWZ,FaGZ,Gg,GgW,GiW,GiG,FloopcqqZ,FaWZnu,FaGZnu,Ggnu,GgWnu,GiWnu,GiGnu
**  #write <bubbleZAout> "FaWZ=%e\nFaGZ=%e\nGg=%e\nGgW=%e\nGiW=%e\nGiG=%e\nFloopcqqZ=%e",FaWZ,FaGZ,Gg,GgW,GiW,GiG,FloopcqqZ
#endif
#ifndef `feyngauge'
  #write <bubbleZAXIout> "FaWZ=%e\nFaGZ=%e\nGg=%e\nGgW=%e\nGiW=%e\nGiG=%e\nFloopcqqZ=%e\nFaWZnu=%e\nFaGZnu=%e\nGgnu=%e\nGgWnu=%e\nGiWnu=%e\nGiGnu=%e\n",FaWZ,FaGZ,Gg,GgW,GiW,GiG,FloopcqqZ,FaWZnu,FaGZnu,Ggnu,GgWnu,GiWnu,GiGnu
#endif
.sort
*if ( count(twint,1) == 0 && count(tgint,1) == 0 && count(twgint,1) == 0 ) discard;
Bracket twint,tgint,twgint,egamp,egamm;
Print;
.end
************
***final print
*************
*Bracket p1,p2,p3,M2,COUPA,delABdelCD,delADdelBC,sigACsigBD,delACdelBD;
Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,COUPgp,COUPsinW,COUPcosW,oneoveroneminusx,xi,mt,pi_,mw2;
Print;
.sort

.end

