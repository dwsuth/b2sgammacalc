(* ::Package:: *)

(* ::Input:: *)
(*$Assumptions=Element[g1,Reals]&&Element[g2,Reals];*)
(*FORMtoMatRules={oneoveroneminusx->1/(1-x),logx->Log[x],logxi->Log[xi],logxoverxi->Log[x/xi],logxxi->Log[x*xi],COUPe->g2 *COUPsinW,mw->mW,mw2->mW^2,COUPgp->g1,COUPg->g2,COUPcosW->g2/Sqrt[g1^2+g2^2],COUPsinW->g1/Sqrt[g1^2+g2^2],mz2->mW^2/COUPcosW^2,mt->Sqrt[\[Kappa]]*mW,COUPv->2*mW/g2,eps->-1/2/Log[mW],GF->Sqrt[2]*g2^2/8/mW^2};*)
(*Rules2={x->\[Kappa],xi->\[Xi],COUPsinW->g1/Sqrt[g1^2+g2^2],COUPcosW->g2/Sqrt[g1^2+g2^2]};*)
(*StringReplacementRules={"g_(1,6_,delta1)"->"2*quarkL","g_(1,6_,N1_?)"->"2*quarkL","g_(1,6_,alpha)"->"2*quarkL","g_(1,7_,delta1)"->"2*quarkR","g_(1,7_,N1_?)"->"2*quarkR","g_(1,7_,alpha)"->"2*quarkR","g_(2,6_,delta1)"->"2*lepL","g_(2,6_,N1_?)"->"2*lepL","g_(2,6_,alpha)"->"2*lepL","g_(2,7_,delta1)"->"2*lepR","g_(2,7_,N1_?)"->"2*lepR","g_(2,7_,alpha)"->"2*lepR","pi_"->"Pi","log(x)"->"logx","log(xi)"->"logxi","log(xi^-1*x)"->"logxoverxi","log(xi*x)"->"logxxi","g_(1,6_,epspol)"->"2*vectorshiz","g_(1,6_)"->"2*scalarshiz","g_(1,7_,epspol)"->"2*vectorshizR","g_(1,7_)"->"2*scalarshizR","p1.epspol"->"p1eps","p2.epspol"->"p2eps","sqrt_(2)"->"Sqrt[2]",";"->";\n","g_(2,6_,p1)"->"2*lepRp1","g_(2,7_,p1)"->"2*lepLp1"};*)
(*$Assumptions=\[Kappa]>1&&\[Xi]>0&&g1>0&&g2>0;*)


(* ::Input:: *)
(*(*only needed on sophies mac*)*)
(*If[$MachineName!="walnuts",SetDirectory["~/Work/b2sgammacalc"];]*)
(**)


(* ::Input:: *)
(*inputFORMfiles={"HRMEout","bubblegamAout","bubblegamBout","bubbleZAout","bubbleZBout","bsgammaout"};*)
(*resultstrs=Import/@inputFORMfiles;*)
(*sanitisedStrings=StringReplace[StringDelete[#,"\n"|"\r"|" "],StringReplacementRules]&/@resultstrs;*)
(*ToExpression/@sanitisedStrings*)


(Ke/.d6dum->0)mw2 Pi^2 COUPsinW^4/COUPe^2/lepL/quarkL/COUPVtb^2/COUPVts^2/COUPe^2//Simplify
1/8(Coefficient[Ke,COUPcll]) Pi^2 COUPsinW^2/lepL/quarkL/COUPVtb^2/COUPVts^2/COUPe^2/d6dum/newshiftdum//Simplify





(* ::Input:: *)
(*diagnamesbox={"Fbox","Gbox1","Gbox2"};*)
(*diagnamesnew={"Ga","Gb","Gc","Gd","Ge","Gf","Gg","GhW","GhG","GiW","GiG","Gj","Gk","GgW","GfW"};*)
(*diagnamesZ={"FaWZ","FaGZ","FbGZ","FbWZ","FcZ","FdZ","FeZ","FfZ","FgZ","FhZ","FloopcqqZ"};*)
(*diagnamesgam={"FaWgam","FaGgam","FbGgam","FbWgam","Fcgam","Fdgam","Fegam","Ffgam","Fggam","Fhgam","Gfgam","GfWgam"};*)
(*diagnamesglue={"Fcglue","Fdglue","Ggglue","FbGglue","Gfglue"};*)
(*diagnamesbub={"Floopcqq"};*)
(*diagnamesZnu={"FaWZnu","FaGZnu","FbGZnu","FbWZnu","FcZnu","FdZnu","FeZnu","FfZnu","FgZnu","FhZnu","FloopcqqZnu"};*)
(*diagnamesboxnu={"Fboxnu","Gbox1nu","Gbox2nu"};*)
(*diagnamesnewnu={"Ganu","Gbnu","Gcnu","Gdnu","Genu","Gfnu","Ggnu","GhWnu","GhGnu","GiWnu","GiGnu","Gjnu","Gknu","GgWnu","GfWnu"};*)
(*diagnamesmix={"Ha","Hb","Hc","Hd","He","Hf","Hg","Hh","Ka","Kb","Kc","Kd","Ke","Kf","Kg","Kh","Ki","Kj","Kk","Kl","Km","Kn","Ko","Kp"};*)
(*diagnamesmixall={"Ha","Hb","Hc", "Hd", "- He", "-Hf","-Hg","-Hh","Ka","Kb","Kc","Kd","Ke","Kf","Kg","Kh","-Ki","-Kj","-Kk","-Kl","-Km","-Kn","-Ko","-Kp"};*)
(*FORMmix=(16*Pi^2*{Ha,Hb,Hc, Hd, - He, -Hf,-Hg,-Hh,Ka,Kb,Kc,Kd,Ke,Kf,Kg,Kh,-Ki,-Kj,-Kk,-Kl,-Km,-Kn,-Ko,-Kp}/COUPVtb^2/ COUPVts^2/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMmixforGIM=(16*Pi^2*{Ha,Hb,Hc,Hd,Ka,Kb,Kc,Kd,Ke,Kf,Kg,Kh}/COUPVtb^2/ COUPVts^2/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMbox=(16*Pi^2*{Fbox,Gbox1,Gbox2}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMnew=(16*Pi^2*{Ga,Gb,Gc,Gd,Ge,Gf,Gg,GhW,GhG,GiW,GiG,Gj,Gk,GgW,GfW}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMZ=(16*Pi^2*{FaGZ,FaWZ,FbGZ,FbWZ,FcZ,FdZ,FeZ,FfZ,FgZ,FhZ,FloopcqqZ}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMgam=(16*Pi^2*{FaWgam,FaGgam,FbWgam,FbGgam,Fcgam,Fdgam,Fegam,Ffgam,Fggam,Fhgam,Gfgam,GfWgam}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)
(*(*FORMgam=(16*Pi^2*{0,0,0,0,Fcgam,Fdgam,Fegam,Ffgam,Fggam,Fhgam,Gfgam,GfWgam}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)*)
(*(*FORMgam=(16*Pi^2*{FaWgam,FaGgam,FbWgam,FbGgam,0,0,0,0,0,0,0,0}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)*)
(*FORMglue=(16*Pi^2*{Fcglue,Fdglue,Ggglue,FbGglue,Gfglue}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)
(*(*FORMglue=(16*Pi^2*{0,0,0,FbGglue,0}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)*)
(*(*FORMglue=(16*Pi^2*{Fcglue,Fdglue,Ggglue,0,Gfglue}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)*)
(*FORMbub=(16*Pi^2*{Floopcqq}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.{vectorshiz->0,scalarshiz->1}//Expand);*)
(*FORMZnu=(16*Pi^2*{FaWZnu,FaGZnu,FbGZnu,FbWZnu,FcZnu,FdZnu,FeZnu,FfZnu,FgZnu,FhZnu,FloopcqqZnu}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMboxnu=(16*Pi^2*{Fboxnu,Gbox1nu,Gbox2nu}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*FORMnewnu=(16*Pi^2*{Ganu,Gbnu,Gcnu,Gdnu,Genu,Gfnu,Ggnu,GhWnu,GhGnu,GiWnu,GiGnu,Gjnu,Gknu,GgWnu,GfWnu}/COUPVtb/ COUPVts/.FORMtoMatRules/.Rules2/.mb->0//Expand);*)
(*{FORMboxd4FeynGauge,FORMnewd4FeynGauge,FORMZd4FeynGauge,FORMgamd4FeynGauge,FORMglued4FeynGauge,FORMbubd4FeynGauge,FORMZnud4FeynGauge,FORMboxnud4FeynGauge,FORMnewnud4FeynGauge,FORMmixd4FeynGauge,FORMmixd4FeynGaugeforGIM}=({FORMbox,FORMnew,FORMZ,FORMgam,FORMglue,FORMbub,FORMZnu,FORMboxnu,FORMnewnu,FORMmix,FORMmixforGIM}/.d6dum->0)//Simplify//Expand;*)
(**)


(* ::Input:: *)
(*wilsonCoeffs={COUPcphil1,COUPcphil3,COUPcphie,COUPcphiq1,COUPcphiu,COUPcphiq3,COUPcphid,COUPclq1,COUPclq3,COUPceu,COUPclu,COUPcqe,COUPcll,COUPcW,COUPcHWB,COUPcHD,COUPced,COUPcld,COUPcqq,COUPcqqprime,COUPcuW,COUPcdW,COUPcuB,COUPcuG,COUPcdB,COUPcdG,COUPcud,deltag1new,deltag2new,deltavnew};*)
(*{FORMboxd6FeynGauge,FORMnewd6FeynGauge,FORMZd6FeynGauge,FORMgamd6FeynGauge,FORMglued6FeynGauge,FORMbubd6FeynGauge,FORMmixd6FeynGauge,FORMmixd6FeynGaugeforGIM,FORMZnud6FeynGauge,FORMboxnud6FeynGauge,FORMnewnud6FeynGauge}=(Coefficient[Total[#],d6dum]&/@{FORMbox,FORMnew,FORMZ,FORMgam,FORMglue,FORMbub,FORMmix,FORMmixforGIM,FORMZnu,FORMboxnu,FORMnewnu})//Simplify//Expand*)


Coefficient[FbGgam,COUPcdB]/(COUPcosW COUPVtb COUPVts)//Simplify


Coefficient[Gfgam,COUPcdB]/(COUPcosW COUPVtb COUPVts)//Simplify


Coefficient[FbGglue,COUPcdG]/(COUPe COUPVtb COUPVts)//Simplify


Coefficient[Gfglue,COUPcdG]/(COUPe COUPVtb COUPVts)//Simplify


Coefficient[FORMgam,COUPcdB]/(COUPcosW COUPVtb COUPVts)//Simplify


Coefficient[FORMglue,COUPcdG]/(COUPe COUPVtb COUPVts)//Simplify


FORMZd6FeynGaugebits=((Coefficient[FORMZd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMgamd6FeynGaugebits=((Coefficient[FORMgamd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMboxd6FeynGaugebits=((Coefficient[FORMboxd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMnewd6FeynGaugebits=((Coefficient[FORMnewd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMglued6FeynGaugebits=((Coefficient[FORMglued6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMbubd6FeynGaugebits=((Coefficient[FORMbubd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMmixd6FeynGaugebits=((Coefficient[FORMmixd6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMmixforGIMd6FeynGaugebits=((Coefficient[FORMmixd6FeynGaugeforGIM,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMZnud6FeynGaugebits=((Coefficient[FORMZnud6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMboxnud6FeynGaugebits=((Coefficient[FORMboxnud6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;
FORMnewnud6FeynGaugebits=((Coefficient[FORMnewnud6FeynGauge,#]&/@ wilsonCoeffs))//Simplify//Expand;


FORMgamd6FeynGaugebits[[-6]]//Simplify


FORMglued6FeynGaugebits[[-6]]//Simplify


FORMgamd4FeynGauge//Simplify


FORMglued6FeynGaugebits//Simplify


Length[FORMgamd6FeynGaugebits]


Coefficient[Gbox2,COUPcphil3]/COUPVtb/COUPVts/lepL/quarkL/COUPe^2/d6dum COUPsinW^2 Pi^2//Simplify


Coefficient[Gbox2nu,COUPcphil3]/COUPVtb/COUPVts/lepL/quarkL/COUPe^2/d6dum COUPsinW^2 Pi^2//Simplify


(* ::Input:: *)
(*(*Checking SM values of Wilson coeffs*)*)
(*TotalSMZ=Total[FORMZd4FeynGauge];*)
(*TotalSMZnu=Total[FORMZnud4FeynGauge];*)
(*TotalSMboxnu=Total[FORMboxnud4FeynGauge];*)
(*TotalSMbox=Total[FORMboxd4FeynGauge];*)
(*TotalSMgam=Total[FORMgamd4FeynGauge];*)
(*TotalSMglue=Total[FORMglued4FeynGauge];*)
(*TotalSMmix=Total[FORMmixd4FeynGauge];*)
(*TotalSMmixtobeGIMmed=Total[FORMmixd4FeynGaugeforGIM];*)
(*SMC10Zresult=((Coefficient[TotalSMZ,lepL]-Coefficient[TotalSMZ,lepR])*(-mW^2/g2^4/quarkL))//Expand//Simplify//Expand;*)
(*SMC10Znuresult=((Coefficient[TotalSMZnu,lepL])*(mW^2/g2^4/quarkL))//Expand//Simplify//Expand;*)
(*SMC10boxnuresult=((Coefficient[TotalSMboxnu,lepL])*(mW^2/g2^4/quarkL))//Expand//Simplify//Expand;*)
(*SMC10boxresult=((Coefficient[TotalSMbox,lepL]-Coefficient[TotalSMbox,lepR])*(-mW^2/g2^4/quarkL))//Expand//Simplify//Expand;*)
(*SMmixresult=-2(TotalSMmix+Limit[TotalSMmixtobeGIMmed,\[Kappa]->0])/g2^4 mW^2/quarkL/lepL//Expand//Simplify//Expand;*)
(*SMGIMMEDC10Zresult=SMC10Zresult-Limit[SMC10Zresult,\[Kappa]->0]//Simplify;*)
(*SMGIMMEDC10Znuresult=SMC10Znuresult-Limit[SMC10Znuresult,\[Kappa]->0]//Simplify;*)
(*SMGIMMEDC10boxresult=SMC10boxresult-Limit[SMC10boxresult,\[Kappa]->0]//Simplify*)
(*SMGIMMEDC10boxnuresult=SMC10boxnuresult-Limit[SMC10boxnuresult,\[Kappa]->0]//Simplify*)
(*SMGIMMEDC10totalresult=SMC10Zresult+SMC10boxresult-Limit[SMC10Zresult+SMC10boxresult,\[Kappa]->0]//Simplify*)
(*SMGIMMEDC10nuresult=SMGIMMEDC10Znuresult+SMGIMMEDC10boxnuresult//Simplify*)
(*SMC7result=1/2((Coefficient[TotalSMgam,p1eps]-Coefficient[TotalSMgam,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))//Simplify;*)
(*SMC8result=1/2((Coefficient[TotalSMglue,p1eps]-Coefficient[TotalSMglue,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))//Simplify;*)
(*SMGIMMEDC7result=SMC7result-Limit[SMC7result,\[Kappa]->0]//Simplify*)
(*SMGIMMEDC8result=SMC8result-Limit[SMC8result,\[Kappa]->0]//Simplify*)
(*(*Total C9 result = 1/s^2 (SMC9Zresult+SMC9boxresult) + SMC9gamresult *)*)
(*(*NB not yet gimmed because C9 gamma causes problems*)*)
(*SMC9gamresult=((Coefficient[TotalSMgam,p1eps]+Coefficient[TotalSMgam,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))//Simplify*)
(*SMC9Zresult=-((Coefficient[TotalSMZ,lepL]+Coefficient[TotalSMZ,lepR])*(-mW^2/g2^4/quarkL))//Expand//Simplify*)
(*SMC9boxresult=-((Coefficient[TotalSMbox,lepL]+Coefficient[TotalSMbox,lepR])*(-mW^2/g2^4/quarkL))//Expand//Simplify*)
(*SMC9gamresultnolog=SMC9gamresult-Coefficient[SMC9gamresult,Log[\[Kappa]]]*Log[\[Kappa]]//Simplify*)
(*SMC9gamGIMMEDresult=(SMC9gamresultnolog-Limit[SMC9gamresultnolog,\[Kappa]->0]+Coefficient[SMC9gamresult,Log[\[Kappa]]]*Log[\[Kappa]])//Simplify*)


SMGIMMEDC10nuresult


SMmixresult//Simplify


SMGIMMEDC7result


SMGIMMEDC8result


S0


C0//Simplify
B0//Simplify
C0-B0//Simplify
C0-4B0//Simplify


(* ::Input:: *)
(*(*now lets get it to calculate C7, C9 and C10 for me*)*)
(*deltaalphaover2alpha=(-e*s\[Theta]*Sqrt[1-s\[Theta]^2]*v^2*COUPcHWB+deltaenew*newshiftdum )/e;*)
(*deltaenew=(deltag2newexpr*s\[Theta]^3+deltag1newexpr*Sqrt[1-s\[Theta]^2]^3);*)
(*(*TODO: THESE SHOULD BE UNCOMMENTED FOR SCHEME 1*)*)
(*deltag2newexpr=g2*v^2*(-COUPcphil3+1/2*COUPcll);*)
(*deltag1newexpr=v^2*(g1*(-COUPcphil3+1/2*COUPcll)-1/4*1/g1*(g2^2+g1^2)*COUPcHD-g2*COUPcHWB);*)
(*(*deltag1newexpr=g1/(2*(1-2s\[Theta]^2))*(s\[Theta]^2*(1/2*v^2*COUPcHD+e*v^3*Sqrt[1-s\[Theta]^2]/mW*COUPcHWB)+2*(1-s\[Theta]^2)^(3/2)*s\[Theta]*v^2*COUPcHWB);*)
(*deltag2newexpr=-g2/(2*(1-2s\[Theta]^2))*((1-s\[Theta]^2)*(1/2*v^2*COUPcHD+e*v^3*Sqrt[1-s\[Theta]^2]/mW*COUPcHWB)+2*Sqrt[1-s\[Theta]^2]*s\[Theta]^3*v^2*COUPcHWB);*)*)
(*\[Delta]GF=1/(Sqrt[2]GF) (Sqrt[2]COUPcphil3-1/Sqrt[2]COUPcll);*)
(*\[Delta]mZ2=mZ^2((1/(2Sqrt[2]GF))COUPcHD+Sqrt[2]/GF mW/mZ Sqrt[1-mW^2/mZ^2]COUPcHWB);*)
(*mZ=1/Sqrt[1-s\[Theta]^2] mW;v=2 mW/g2; GF=Sqrt[2]g2^2/(8mW^2);*)
(*s\[Theta]=g1/Sqrt[g1^2+g2^2];*)
(*e=g1 g2/Sqrt[g1^2+g2^2];*)
(*C10Zresult=((Coefficient[FORMZd6FeynGaugebits,lepL]-Coefficient[FORMZd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C9Zresult=-((Coefficient[FORMZd6FeynGaugebits,lepL]+Coefficient[FORMZd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C10boxresult=((Coefficient[FORMboxd6FeynGaugebits,lepL]-Coefficient[FORMboxd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C9boxresult=-((Coefficient[FORMboxd6FeynGaugebits,lepL]+Coefficient[FORMboxd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C10newresult=((Coefficient[FORMnewd6FeynGaugebits,lepL]-Coefficient[FORMnewd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C10Znuresult=((Coefficient[FORMZnud6FeynGaugebits,lepL])*(1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C10boxnuresult=((Coefficient[FORMboxnud6FeynGaugebits,lepL])*(1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C10newnuresult=((Coefficient[FORMnewnud6FeynGaugebits,lepL])*(1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C9newresult=-((Coefficient[FORMnewd6FeynGaugebits,lepL]+Coefficient[FORMnewd6FeynGaugebits,lepR])*(-1/4/g2^2/quarkL))//Expand//Simplify//Expand;*)
(*C7result=1/2((Coefficient[FORMgamd6FeynGaugebits,p1eps]-Coefficient[FORMgamd6FeynGaugebits,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))(g2^2/4/mW^2)//Simplify*)
(*C7bubresult=1/2((Coefficient[FORMbubd6FeynGaugebits,p1eps]-Coefficient[FORMbubd6FeynGaugebits,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))(g2^2/4/mW^2)//Simplify*)
(*C8result=1/2((Coefficient[FORMglued6FeynGaugebits,p1eps]-Coefficient[FORMglued6FeynGaugebits,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))(g2^2/4/mW^2)//Simplify*)
(*C9gamresult=((Coefficient[FORMgamd6FeynGaugebits,p1eps]+Coefficient[FORMgamd6FeynGaugebits,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))(g2^2/4/mW^2)//Simplify*)
(*C9gambubresult=((Coefficient[FORMbubd6FeynGaugebits,p1eps]+Coefficient[FORMbubd6FeynGaugebits,p2eps])*(-mW^2/g2^2/mb/(g1 g2)Sqrt[g1^2+g2^2]))(g2^2/4/mW^2)//Simplify*)
(*C9gamresultdeltaalphasummed=SMC9gamGIMMEDresult*(g2^2/4/mW^2)*deltaalphaover2alpha//Simplify;*)
(*C9gamresultdeltaalpha={Coefficient[C9gamresultdeltaalphasummed,COUPcphil1],Coefficient[C9gamresultdeltaalphasummed,COUPcphil3],Coefficient[C9gamresultdeltaalphasummed,COUPcphie],Coefficient[C9gamresultdeltaalphasummed,COUPcphiq1],Coefficient[C9gamresultdeltaalphasummed,COUPcphiu],Coefficient[C9gamresultdeltaalphasummed,COUPcphiq3],Coefficient[C9gamresultdeltaalphasummed,COUPcphid],Coefficient[C9gamresultdeltaalphasummed,COUPclq1],Coefficient[C9gamresultdeltaalphasummed,COUPclq3],Coefficient[C9gamresultdeltaalphasummed,COUPceu],Coefficient[C9gamresultdeltaalphasummed,COUPclu],Coefficient[C9gamresultdeltaalphasummed,COUPcqe],Coefficient[C9gamresultdeltaalphasummed,COUPcll],Coefficient[C9gamresultdeltaalphasummed,COUPcW],Coefficient[C9gamresultdeltaalphasummed,COUPcHWB],Coefficient[C9gamresultdeltaalphasummed,COUPcHD],Coefficient[C9gamresultdeltaalphasummed,COUPced],Coefficient[C9gamresultdeltaalphasummed,COUPcld],Coefficient[C9gamresultdeltaalphasummed,COUPcqq],Coefficient[C9gamresultdeltaalphasummed,COUPcqqprime],Coefficient[C9gamresultdeltaalphasummed,COUPcuW],Coefficient[C9gamresultdeltaalphasummed,COUPcdW],Coefficient[C9gamresultdeltaalphasummed,COUPcuB],Coefficient[C9gamresultdeltaalphasummed,COUPcuG],Coefficient[C9gamresultdeltaalphasummed,COUPcdB],Coefficient[C9gamresultdeltaalphasummed,COUPcdG],Coefficient[C9gamresultdeltaalphasummed,COUPcud],Coefficient[C9gamresultdeltaalphasummed,deltag1new],Coefficient[C9gamresultdeltaalphasummed,deltag2new],Coefficient[C9gamresultdeltaalphasummed,deltavnew]};*)
(*GIMMEDC7result=C7result-Limit[C7result,\[Kappa]->0]+C7bubresult-Limit[C7bubresult,\[Kappa]->0];*)
(*GIMMEDC8result=C8result-Limit[C8result,\[Kappa]->0];*)
(*C9gamresultnolog=C9gamresult-Coefficient[C9gamresult,Log[\[Kappa]]]*Log[\[Kappa]];*)
(*GIMMEDC9gamresult=(C9gamresultnolog-Limit[C9gamresultnolog,\[Kappa]->0]+Coefficient[C9gamresult,Log[\[Kappa]]]*Log[\[Kappa]]);*)
(*GIMMEDC9gambubresult=C9gambubresult//Simplify*)
(*GIMMEDC9boxresult=C9boxresult-Limit[C9boxresult,\[Kappa]->0];*)
(*GIMMEDC9Zresult=C9Zresult-Limit[C9Zresult,\[Kappa]->0];*)
(*GIMMEDC9newresult=C9newresult-Limit[C9newresult,\[Kappa]->0];*)
(*GIMMEDC10Zresult=C10Zresult-Limit[C10Zresult,\[Kappa]->0];*)
(*GIMMEDC10boxresult=C10boxresult-Limit[C10boxresult,\[Kappa]->0];*)
(*GIMMEDC10newresult=C10newresult-Limit[C10newresult,\[Kappa]->0];*)
(*GIMMEDC10Znuresult=C10Znuresult-Limit[C10Znuresult,\[Kappa]->0];*)
(*GIMMEDC10boxnuresult=C10boxnuresult-Limit[C10boxnuresult,\[Kappa]->0];*)
(*GIMMEDC10newnuresult=C10newnuresult-Limit[C10newnuresult,\[Kappa]->0];*)
(*GIMMEDC10totalresult=C10Zresult+C10boxresult+C10newresult-Limit[C10Zresult+C10boxresult+C10newresult,\[Kappa]->0];*)
(*GIMMEDC10totalnuresult=C10Znuresult+C10boxnuresult+C10newnuresult-Limit[C10Znuresult+C10boxnuresult+C10newnuresult,\[Kappa]->0];*)
(*C1mixresult=-1/2(FORMmixd6FeynGaugebits+Limit[FORMmixforGIMd6FeynGaugebits,\[Kappa]->0])/g2^2/quarkL/lepL//Expand//Simplify//Expand;*)
(*C1mixresultnoGIM=-1/2(FORMmixd6FeynGaugebits)/g2^2/quarkL/lepL//Expand//Simplify//Expand;*)
(*Ix=-((x (7-8 x+x^2+4 (-1+x)^2 Log[mW]+2 (4-2 x+x^2) Log[x]))/(32 (-1+x)^2))//Simplify;*)
(**)
(**)


Length[C9gamresultdeltaalpha]


wilsonCoeffs


simpGIMMedC10Zresult=GIMMEDC10Zresult//Simplify;
simpGIMMedC10newresult=GIMMEDC10newresult//Simplify;
simpGIMMedC10boxresult=GIMMEDC10boxresult//Simplify;
simpGIMMedC10totalresult=GIMMEDC10totalresult//Simplify;
simpGIMMedC10Znuresult=GIMMEDC10Znuresult//Simplify;
simpGIMMedC10newnuresult=GIMMEDC10newnuresult//Simplify;
simpGIMMedC10boxnuresult=GIMMEDC10boxnuresult//Simplify;
simpGIMMedC10totalnuresult=GIMMEDC10totalnuresult//Simplify;
simpGIMMedC9Zresult=GIMMEDC9Zresult//Simplify;
simpGIMMedC9newresult=GIMMEDC9newresult//Simplify;
simpGIMMedC9boxresult=GIMMEDC9boxresult//Simplify; (*todo: check that this is consistent with setting shiftdum\[Rule]0 before this step!*)
simpGIMMedC7result=GIMMEDC7result//Simplify;
simpGIMMedC8result=GIMMEDC8result//Simplify;
simpGIMMedC9gamresult=GIMMEDC9gamresult+C9gamresultdeltaalpha+GIMMEDC9gambubresult//Simplify//Expand//Simplify;
simpGIMMedC9totalresult=GIMMEDC9gamresult+1/s\[Theta]^2(C9gamresultdeltaalpha+GIMMEDC9boxresult+GIMMEDC9newresult+GIMMEDC9Zresult)//Simplify//Expand//Simplify;
simpGIMMedC9hardresult=GIMMEDC9boxresult+GIMMEDC9newresult+GIMMEDC9Zresult//Simplify;
simpC9gamresult=C9gamresult+C9gamresultdeltaalpha//Simplify;


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMedC9Zresult,newshiftdum],Coefficient[simpGIMMedC9boxresult,newshiftdum],Coefficient[simpGIMMedC9gamresult,newshiftdum]}//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,Coefficient[C1mixresult,newshiftdum]}//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,C1mixresult,C1mixresultnoGIM}/.newshiftdum->0//Simplify],Frame->All]


(-1+\[Kappa]^2)/(-1+\[Kappa])^3//Simplify


Grid[Transpose[{wilsonCoeffs,simpGIMMedC9gamresult,simpGIMMedC9Zresult+simpGIMMedC9newresult,simpGIMMedC9boxresult,simpGIMMedC9Zresult+simpGIMMedC9newresult+simpGIMMedC9boxresult}/.newshiftdum->0//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC9gamresult,simpGIMMedC9Zresult+simpGIMMedC9newresult,simpGIMMedC9boxresult}/.newshiftdum->1//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMedC10Zresult+simpGIMMedC10newresult,newshiftdum],Coefficient[simpGIMMedC10boxresult,newshiftdum],Coefficient[simpGIMMedC10totalresult,newshiftdum]}//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,Coefficient[simpGIMMedC10Znuresult+simpGIMMedC10newnuresult,newshiftdum],Coefficient[simpGIMMedC10boxnuresult,newshiftdum],Coefficient[simpGIMMedC10totalnuresult,newshiftdum]}//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC10Znuresult+simpGIMMedC10newnuresult,simpGIMMedC10boxnuresult,simpGIMMedC10totalnuresult}/.newshiftdum->1//Simplify],Frame->All]


(-31+32 \[Kappa]-\[Kappa]^2)/(-1+\[Kappa])^2//Simplify


C0-4B0//Simplify


Grid[Transpose[{wilsonCoeffs,simpGIMMedC10Zresult+simpGIMMedC10newresult,simpGIMMedC10Znuresult+simpGIMMedC10newnuresult,simpGIMMedC10boxresult,simpGIMMedC10boxnuresult}/.newshiftdum->0//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC7result}/.newshiftdum->1//Simplify],Frame->All]


Grid[Transpose[{wilsonCoeffs,simpGIMMedC8result}/.newshiftdum->1]//Simplify,Frame->All]


(*this is the HWB non shift gamma piece for C9*)
HWBnonshiftgamC9=(g2 (-(-1+\[Kappa]) \[Kappa] (9 g2^2 (3-4 \[Kappa]+\[Kappa]^2)+g1^2 (27-61 \[Kappa]+28 \[Kappa]^2))+2 (9 g2^2 \[Kappa]^2 (2-3 \[Kappa]+\[Kappa]^2)+g1^2 (-8+32 \[Kappa]-36 \[Kappa]^2+3 \[Kappa]^3+6 \[Kappa]^4)) Log[\[Kappa]]))/(18 g1 (g1^2+g2^2) (-1+\[Kappa])^4);
g1sqpiece=Coefficient[HWBnonshiftgamC9*g1/g2*(g1^2+g2^2),g1^2]//Simplify;
g2sqpiece=Coefficient[HWBnonshiftgamC9*g1/g2*(g1^2+g2^2),g2^2]//Simplify;
g1sqpiece-g2sqpiece//Simplify
g1sqpiece+g2sqpiece//Simplify
g2/g1*1/2(g1sqpiece+g2sqpiece)+g2/g1 1/2 (g1^2-g2^2)/(g1^2+g2^2)(g1sqpiece-g2sqpiece)//Simplify
HWBshiftgamc9=-((g2^3 (\[Kappa]^2 (25-44 \[Kappa]+19 \[Kappa]^2)+2 (8-32 \[Kappa]+54 \[Kappa]^2-30 \[Kappa]^3+3 \[Kappa]^4) Log[\[Kappa]]))/(18 g1 (g1^2+g2^2) (-1+\[Kappa])^4))


(\[Kappa] (54-151 \[Kappa]+134 \[Kappa]^2-37 \[Kappa]^3)+2 (-8+32 \[Kappa]-18 \[Kappa]^2-24 \[Kappa]^3+15 \[Kappa]^4) Log[\[Kappa]])/(18 (-1+\[Kappa])^4)
(54-151 \[Kappa]+134 \[Kappa]^2-37 \[Kappa]^3)/(-1+\[Kappa])^4//Simplify
(-8+32 \[Kappa]-18 \[Kappa]^2-24 \[Kappa]^3+15 \[Kappa]^4)/(-1+\[Kappa])^4//Simplify


(*This is the HWB non-shift piece for C7*)
HWBnonshiftC7=(g2 \[Kappa] ((-1+\[Kappa]) (-12 g2^2 (-1+\[Kappa])+g1^2 (5-7 \[Kappa]+8 \[Kappa]^2))-6 \[Kappa] (g2^2 (3-4 \[Kappa]+\[Kappa]^2)+g1^2 (1-\[Kappa]+\[Kappa]^2)) Log[\[Kappa]]))/(24 g1 (g1^2+g2^2) (-1+\[Kappa])^4);
g1sqpiecec7=Coefficient[HWBnonshiftC7*g1/g2*(g1^2+g2^2),g1^2]//Simplify;
g2sqpiecec7=Coefficient[HWBnonshiftC7*g1/g2*(g1^2+g2^2),g2^2]//Simplify;
(g1sqpiecec7-g2sqpiecec7)/2//Simplify
(g1sqpiecec7+g2sqpiecec7)/2//Simplify
g2/g1*1/2(g1sqpiecec7+g2sqpiecec7)+g2/g1 1/2 (g1^2-g2^2)/(g1^2+g2^2)(g1sqpiecec7-g2sqpiecec7)//Simplify


-(17-36 \[Kappa]+27 \[Kappa]^2-8 \[Kappa]^3)/(48(-1+\[Kappa])^4)//Simplify


6 \[Kappa] (4-5 \[Kappa]+2 \[Kappa]^2) /(48(-1+\[Kappa])^4)//Simplify


(*Inami-Lim functions*)
B0=1/4(-\[Kappa]/(\[Kappa]-1)+\[Kappa]/(\[Kappa]-1)^2 Log[\[Kappa]])//Simplify
C0=\[Kappa]/8((\[Kappa]-6)/(\[Kappa]-1)+(3\[Kappa]+2)/(\[Kappa]-1)^2 Log[\[Kappa]])//Simplify
D0=-4/9 Log[\[Kappa]]+(-19\[Kappa]^3+25\[Kappa]^2)/(36(\[Kappa]-1)^3)+\[Kappa]^2(5\[Kappa]^2-2\[Kappa]-6)/(18(\[Kappa]-1)^4)Log[\[Kappa]]//Simplify
D0
D0prime=(8\[Kappa]^3+5\[Kappa]^2-7\[Kappa])/(12(\[Kappa]-1)^3)+\[Kappa]^2/2(2-3\[Kappa])/(1-\[Kappa])^4 Log[\[Kappa]]//Simplify
-((\[Kappa] (2+3 \[Kappa]-6 \[Kappa]^2+\[Kappa]^3+6 \[Kappa] Log[\[Kappa]]))/(12 (-1+\[Kappa])^4))
E0prime=-\[Kappa] (\[Kappa]^2-5\[Kappa]-2)/(4(1-\[Kappa])^3)+3/2 \[Kappa]^2/(\[Kappa]-1)^4 Log[\[Kappa]]//Simplify
IAebischer=x/16(-2Log[mW]+(x-7)/(2(1-x))-(x^2-2x+4)/(x-1)^2Log[x])//Simplify
S0=(4x-11x^2+x^3)/4/(1-x)^2-3x^3/2/(1-x)^3 Log[x]//Simplify;






