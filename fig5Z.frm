Dimension D;
Vectors p1,p2,p3,k,epsstar,vectorcurrentL,vectorcurrentR;
Symbols Qt,mt,mb,scalarchargeL,scalarchargeR,davedavedave,C00,C12,C11,C22,C1,C2,C0,d,yt,yb,q,g,gprime,LHcombo,RHcombo;
Indices mu1,mu2,mu3,mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3;
Functions it44,it43,it22,it23,it62,it61mu,it61nu,it60,it33,it32,it12,it11,it54,it53mu,it53nu,it52;

*hack to stop immediate contraction of indices
Index gamma1=0;
Index gamma2=0;
Index gamma3=0;
Index mu=0;
Index nu=0;
Index rho=0;
Index sigma=0;
Index alpha=0;
Index beta=0;
Index gamma=0;
Index mu1=0;
Index mu2=0;
Index mu3=0;


**A has the goldstone loop on the b leg, B has the goldstone loop on the s leg**
**for A and B, Cs actually mean Bs!
Local FA=C0*epsstar(gamma)*g_(1,gamma,7_)/2*(k(mu)*g_(1,mu)+mt)*(g_(1,7_)/2*LHcombo);
Local FB=C0*epsstar(gamma)*g_(1,6_)/2*(k(mu)*g_(1,mu)+mt)*g_(1,gamma,7_)/2*LHcombo;

**FC is a triangle (G-t-t) with the operator insertion on the s-t vertex
**FD is the same but with the operator insertion on the b-t vertex
**I think LHcombo and RHcombo mean diff things for C and D as for A and B
*Local FC=C0*epsstar(gamma)*g_(1,6_)/2*((-k(mu)-p2(mu))*g_(1,mu)+mt)*(LHcombo*g_(1,gamma,7_)/2+RHcombo*g_(1,gamma,6_)/2)*((-k(nu)+p1(nu))*g_(1,nu)+mt)*g_(1,7_)/2;
Local FD=C0*epsstar(gamma)*g_(1,6_)/2*((-k(mu)-p2(mu))*g_(1,mu)+mt)*(LHcombo*g_(1,gamma,7_)/2+RHcombo*g_(1,gamma,6_)/2)*((-k(nu)+p1(nu))*g_(1,nu)+mt)*k(sigma)*g_(1,sigma,7_)/2;
Local FC=C0*epsstar(gamma)*(k(sigma)*g_(1,sigma,7_)/2)*((-k(mu)-p2(mu))*g_(1,mu)+mt)*(LHcombo*g_(1,gamma,7_)/2+RHcombo*g_(1,gamma,6_)/2)*((-k(nu)+p1(nu))*g_(1,nu)+mt)*g_(1,7_)/2;



.sort
Print;
*id it44(mu?,alpha?,beta?,gamma?) = -i_ * (
*p3(gamma3) * ( p2(gamma2) + k(gamma2) ) * ( p1(gamma3) - k(gamma3) ) - 
*p3(gamma2) * ( p2(gamma3) + k(gamma3) ) * ( p1(gamma1) - k(gamma1) ) +
*d_(gamma1,gamma2) * ( p2(gamma3) + k(gamma3) ) * p3(sigma) * ( p1(sigma) - k(sigma) ) 
*d_(gamma1,gamma2) * ( p2(gamma3) + k(gamma3) ) * p3(sigma) * ( p1(sigma) - k(sigma) ) 
*
*);
id it54(mu?,nu?,alpha?,beta?) = i_ * (k(mu)-p1(mu)) * (k(nu) + p2(nu)) * d_(alpha,beta);
id it53mu(mu?,alpha?,beta?) = -i_ * (k(mu)-p1(mu)) * (mt) * d_(alpha,beta);
id it53nu(nu?,alpha?,beta?) = -i_ * (mt) * (k(nu) + p2(nu)) * d_(alpha,beta);
id it52(alpha?,beta?) = i_ * mt^2 * d_(alpha,beta);
*id it44(mu?,alpha?,beta?,gamma?) = -i_ * (
* d_(gamma1,gamma2) * ( 2*k(gamma) + p2(gamma) - p1(gamma) ) +
* d_(gamma1,gamma) * ( -k(gamma2) + 2*p1(gamma2) + p2(gamma2) ) +
* d_(gamma,gamma2) * ( -k(gamma1) - 2*p2(gamma1) - p1(gamma1) )
*) * k(mu) * d_(alpha,gamma1) * d_(beta,gamma2);
*id it43(alpha?,beta?,gamma?) = -i_ * (
* d_(gamma1,gamma2) * ( 2*k(gamma) + p2(gamma) - p1(gamma) ) +
* d_(gamma1,gamma) * ( -k(gamma2) + 2*p1(gamma2) + p2(gamma2) ) +
* d_(gamma,gamma2) * ( -k(gamma1) - 2*p2(gamma1) - p1(gamma1) )
*) * mt * d_(alpha,gamma1) * d_(beta,gamma2);
id it44(mu?,alpha?,beta?,gamma?) = -i_ * (
d_(gamma1,gamma2) * (2* k(gamma) + p2(gamma) - p1(gamma) ) +
d_(gamma1,gamma) * (-k(gamma2) + 2*p1(gamma2) + p2(gamma2) ) +
d_(gamma,gamma2) * (-k(gamma1) - 2*p2(gamma1) - p1(gamma1) )
) * k(mu) * d_(alpha,gamma1) * d_(beta,gamma2);
id it43(alpha?,beta?,gamma?) = -i_ * (
d_(gamma1,gamma2) * (2* k(gamma) + p2(gamma) - p1(gamma) ) +
d_(gamma1,gamma) * (-k(gamma2) + 2*p1(gamma2) + p2(gamma2) ) +
d_(gamma,gamma2) * (-k(gamma1) - 2*p2(gamma1) - p1(gamma1) )
) * mt * d_(alpha,gamma1) * d_(beta,gamma2);
id it23(mu?,beta?,gamma?) = i_ * k(mu) * d_(beta,gamma);
id it22(beta?,gamma?) = i_ * mt * d_(beta,gamma);
id it62(mu?,nu?) = -i_ * (k(mu) - p1(mu)) * (k(nu) + p2(nu));
id it61mu(mu?) = i_ * (k(mu) - p1(mu)) * mt;
id it61nu(nu?) = i_ * mt * (k(nu) + p2(nu));
id it60 = -i_ * mt^2;
id it33(mu?,alpha?,gamma?) = i_ * k(mu) * d_(alpha,gamma);
id it32(alpha?,gamma?) = i_ * mt * d_(alpha,gamma);
id it12(mu?,gamma?) = -i_ * (2 * k(gamma) + p2(gamma) - p1(gamma)) * k(mu);
id it11(gamma?) = -i_ * (2 * k(gamma) + p2(gamma) - p1(gamma)) * mt;

.sort
Print;
*.sort
*sum mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,mu1,mu2,mu3;
*sum gamma1,gamma2;
.sort
Print;
id k(mu?) * k(nu?) = 1/C0 * (C00 * d_(mu,nu));
id k(mu?) = 0;
Bracket C0,C1,C2,C00,C11,C12,C22;
*.sort
Print;
.sort
sum mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,mu1,mu2,mu3;
Print;
.sort


****these subs have to be done in a specific order!! all longer gamma matrix products must be eliminated first. Within that, subs with fewer wildcards come first
****five gammas (plus proj matrix)
id g_(1,6_,alpha?,mu?,gamma?,nu?,alpha?) = -2 * g_(1,6_,nu,gamma,mu) + (4-D)*g_(1,6_,mu,gamma,nu);
****three gammas (plus projection matrix)
id g_(1,6_,gamma?,p1,p1) = mb^2 * g_(1,6_,gamma);
id g_(1,6_,gamma?,p1,p2) = 2 * p1(mu) * p2(mu) * g_(1,6_,gamma) - 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,gamma?,p2,p1) = 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,gamma?,p2,p2) = 0;
id g_(1,6_,p1,p1,gamma?) = mb^2 * g_(1,6_,gamma);
id g_(1,6_,p1,p2,gamma?) = 2 * p1(mu) * p2(mu) * g_(1,6_,gamma); 
id g_(1,6_,p2,p1,gamma?) = 0;
id g_(1,6_,p2,p2,gamma?) = 0;
id g_(1,6_,p1,gamma?,p2) = -2 * p1(mu) * p2(mu) * g_(1,6_,gamma) + 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,p1,gamma?,p1) = - mb^2 * g_(1,6_,gamma) + 2 * mb * p1(gamma) * g_(1,6_);
id g_(1,6_,p2,gamma?,p2) = 0;
id g_(1,6_,p2,gamma?,p1) = 0;
id g_(1,6_,mu?,nu?,mu?) = -(D-2)*g_(1,6_,nu);
id g_(1,6_,mu?,mu?,nu?) = D*g_(1,6_,nu);
id g_(1,6_,nu?,mu?,mu?) = D*g_(1,6_,nu);
****two gammas (plus projection matrix)
id g_(1,6_,gamma?,p2) = 2 * p2(gamma) * g_(1,6_);
id g_(1,6_,p1,gamma?) = 2 * p1(gamma) * g_(1,6_) - mb * g_(1,6_,gamma);
id g_(1,6_,gamma?,p1) = mb * g_(1,6_,gamma);
id g_(1,6_,p2,gamma?) = 0;
id g_(1,6_,mu?,mu?) = D * g_(1,6_);
****one gamma (plus projection matrix)
id g_(1,6_,p1) = mb * g_(1,6_);
id g_(1,6_,p2) = 0;

sum mu;
Print;
.sort

id g_(1,6_,epsstar) = 2*vectorcurrentL;
id g_(1,7_,epsstar) = 2*vectorcurrentR;
id g_(1,6_) = 2*scalarchargeR;
id g_(1,7_) = 2*scalarchargeL;
id p1.p2 = (q^2-mb^2)/2;
id q^2=0;
id mb=0;
id yb=0;

Bracket scalarchargeR,vectorcurrentL,vectorcurrentR,scalarchargeL;
Print;
.end

