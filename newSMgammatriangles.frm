Dimension D;
Vectors q1,q2,q3,p1,p2,p3,p4,k,epsstar,vectorcurrentL,vectorcurrentR;
Symbols g,Kmu,sw2,mmu,mt,mb,scalarchargeL,scalarchargeR,scalarchargeLp1,scalarchargeLp2,scalarchargeRp1,scalarchargeRp2,davedavedave,C00,C12,C11,C22,C1,C2,C0,d,yt,yb,q,KD33,KD32,KD22,Kd33,Kd32,Kd22,KD33star,KD32star,Kd33star,Kd32star,KD22star,Kd22star,C0000,C1111,C2222,C1122,C1222,C2111,C111,C100,C200,C222,C122,C211,C1200,C1100,C2200,CeffGG,LHcombo,RHcombo;
Indices mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,delta,alpha1,beta1,mu1,nu1,sigma1,delta1;
Functions itA4,itA3mu,itA3nu,itA2,itB3,itB4,itC2,itC3,sig;

*hack to stop immediate contraction of indices
Index gamma1=0;
Index gamma2=0;
Index gamma3=0;
Index mu=0;
Index nu=0;
Index rho=0;
Index sigma=0;
Index alpha=0;
Index beta=0;
Index gamma=0;
Index delta=0;
Index delta1=0;
Index mu1=0;
Index mu2=0;
Index mu3=0;
Index alpha1=0;
Index sigma1=0;
Index beta1=0;
Index nu1=0;

Local FC=C0*epsstar(gamma)*g_(1,beta,7_)/2*(-(p2(mu)+k(mu))*g_(1,mu)+mt)*g_(1,gamma)*((p1(nu)-k(nu))*g_(1,nu)+mt)*g_(1,alpha,7_)/2*d_(alpha,beta);
Local FD=C0*epsstar(gamma)*yt*g_(1,6_)/2*(-(p2(mu)+k(mu))*g_(1,mu)+mt)*g_(1,gamma)*((p1(nu)-k(nu))*g_(1,nu)+mt)*(yt*g_(1,7_)/2-yb*g_(1,6_)/2);
Local FE=C0*epsstar(gamma)*g_(1,beta,7_)/2*(k(mu)*g_(1,mu)+mt)*g_(1,alpha,7_)/2*d_(gamma,mu1)*d_(alpha,mu3)*d_(beta,mu2)*(d_(mu1,mu2)*(q1(mu3)-q2(mu3))+d_(mu2,mu3)*(q2(mu1)-q3(mu1))+d_(mu3,mu1)*(q3(mu2)-q1(mu2)));
Local FF=C0*epsstar(gamma)*g_(1,beta,7_)/2*(k(mu)*g_(1,mu)+mt)*(yt*g_(1,7_)/2-yb*g_(1,6_)/2)*d_(mu2,gamma)*d_(mu2,beta);
Local FG=C0*epsstar(gamma)*yt*g_(1,6_)/2*(k(mu)*g_(1,mu)+mt)*g_(1,alpha,7_)/2*d_(mu2,gamma)*d_(mu2,alpha);
Local FH=C0*epsstar(gamma)*yt*g_(1,6_)/2*(k(mu)*g_(1,mu)+mt)*(yt*g_(1,7_)/2-yb*g_(1,6_)/2)*(2*k(gamma)+p2(gamma)-p1(gamma));


id q1=-(p1+p2);
id q2=k+p2;
id q3=p1-k;


Print;
.sort
id k(mu?)*k(nu?)*k(alpha?)*k(beta?)=1/C0*((d_(mu,nu)*d_(alpha,beta)+d_(mu,alpha)*d_(nu,beta)+d_(mu,beta)*d_(nu,alpha))*C0000
+(d_(mu,nu)*p1(alpha)*p1(beta)+d_(mu,alpha)*p1(nu)*p1(beta)+d_(mu,beta)*p1(nu)*p1(alpha)+d_(nu,alpha)*p1(mu)*p1(beta)+d_(nu,beta)*p1(mu)*p1(alpha)+d_(alpha,beta)*p1(nu)*p1(mu))*C1100
+(d_(mu,nu)*p2(alpha)*p2(beta)+d_(mu,alpha)*p2(nu)*p2(beta)+d_(mu,beta)*p2(nu)*p2(alpha)+d_(nu,alpha)*p2(mu)*p2(beta)+d_(nu,beta)*p2(mu)*p2(alpha)+d_(alpha,beta)*p2(nu)*p2(mu))*C2200
+(d_(mu,nu)*p2(alpha)*p1(beta)+d_(mu,alpha)*p2(nu)*p1(beta)+d_(mu,beta)*p2(nu)*p1(alpha)+d_(nu,alpha)*p2(mu)*p1(beta)+d_(nu,beta)*p2(mu)*p1(alpha)+d_(alpha,beta)*p2(nu)*p1(mu)+d_(mu,nu)*p1(alpha)*p2(beta)+d_(mu,alpha)*p1(nu)*p2(beta)+d_(mu,beta)*p1(nu)*p2(alpha)+d_(nu,alpha)*p1(mu)*p2(beta)+d_(nu,beta)*p1(mu)*p2(alpha)+d_(alpha,beta)*p1(nu)*p2(mu))*C1200
+(p1(mu)*p2(nu)*p2(alpha)*p2(beta)+p2(mu)*p1(nu)*p2(alpha)*p2(beta)+p2(mu)*p2(nu)*p1(alpha)*p2(beta)+p2(mu)*p2(nu)*p2(alpha)*p1(beta))*C1222
+(p2(mu)*p1(nu)*p1(alpha)*p1(beta)+p1(mu)*p2(nu)*p1(alpha)*p1(beta)+p1(mu)*p1(nu)*p2(alpha)*p1(beta)+p1(mu)*p1(nu)*p1(alpha)*p2(beta))*C2111
+(p2(mu)*p2(nu)*p1(alpha)*p1(beta)+p2(mu)*p2(alpha)*p1(nu)*p1(beta)+p2(mu)*p2(beta)*p1(nu)*p1(alpha)+p2(nu)*p2(alpha)*p1(mu)*p1(beta)+p2(nu)*p2(beta)*p1(mu)*p1(alpha)+p2(alpha)*p2(beta)*p1(nu)*p1(mu))*C1122
+p1(mu)*p1(nu)*p1(alpha)*p1(beta)*C1111
+p2(mu)*p2(nu)*p2(alpha)*p2(beta)*C2222);
id k(mu?)*k(nu?)*k(alpha?)=1/C0*((d_(mu,nu)*p1(alpha)+d_(mu,alpha)*p1(nu)+d_(nu,alpha)*p1(mu))*C100+(d_(mu,nu)*p2(alpha)+d_(mu,alpha)*p2(nu)+d_(nu,alpha)*p2(mu))*C200+(p1(mu)*p1(nu)*p2(alpha)+p1(nu)*p1(alpha)*p2(mu)+p1(mu)*p1(alpha)*p2(nu))*C211+(p2(mu)*p2(nu)*p1(alpha)+p2(nu)*p2(alpha)*p1(mu)+p2(mu)*p2(alpha)*p1(nu))*C122+p1(mu)*p1(nu)*p1(alpha)*C111+p2(mu)*p2(nu)*p2(alpha)*C222);
id k(mu?) * k(nu?) = 1/C0 * (C00 * d_(mu,nu) + C12 * (p1(mu)*p2(nu) + p1(nu)*p2(mu)) + C11 * p1(mu) * p1(nu) + C22 * p2(mu) * p2(nu) );
id k(mu?) = 1/C0 * (p1(mu) * C1 + p2(mu) * C2);
Bracket C0,C1,C2,C00,C11,C12,C22,C100,C200,C111,C222,C122,C211,C0000,C1111,C2222,C1100,C2200,C1200,C1222,C2111,C1122;
*.sort
Print;
.sort
sum mu,mu1,mu2,mu3,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,alpha1,beta1,delta1,nu1,sigma1;
Print;
.sort


***hey babes it's me. I've tried to add some general rules to help condense these gamma strings

***currently the program stops after this set of substitutions, and adds a dummy term davedavedave every time one is applied

***note that p1?,p2? etc should match any momentum, and mu?,nu? etc any index

***step one: if two adjacent gamma matrices are contracted by the same momentum or same index, replace
repeat;
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1);
endrepeat;


***step two: if two adjacent-but-one gamma matrices are contracted by the same momentum or same index, commute and then replace
repeat;
id g_(1,p1?)*g_(1,p2?)*g_(1,p1?)=(2*d_(p1,p2)-g_(1,p2,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,p2?)*g_(1,mu?)=(2*d_(mu,p2)-g_(1,p2,mu))*g_(1,mu);
id g_(1,mu?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,nu)-g_(1,nu,mu))*g_(1,mu);
id g_(1,p1?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,nu)-g_(1,nu,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1);
endrepeat;

***step three: if two adjacent-but-two gamma matrices are contracted by the same momentum or same index, commute and replace, then apply adjacent-but-one rules
repeat;
id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,p3,p1)-2*d_(p1,p3)*g_(1,p2,p1)+g_(1,p2,p3,p1,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,p3?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,p3,p1)-2*d_(p1,p3)*g_(1,nu,p1)+g_(1,nu,p3,p1,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,nu,p1)-2*d_(p1,nu)*g_(1,p2,p1)+g_(1,p2,nu,p1,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,rho?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,rho,p1)-2*d_(p1,rho)*g_(1,nu,p1)+g_(1,nu,rho,p1,p1));
id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,p3,mu)-2*d_(mu,p3)*g_(1,p2,mu)+g_(1,p2,p3,mu,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,p3,mu)-2*d_(mu,p3)*g_(1,nu,mu)+g_(1,nu,p3,mu,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,nu,mu)-2*d_(mu,nu)*g_(1,p2,mu)+g_(1,p2,nu,mu,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,rho?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,rho,mu)-2*d_(mu,rho)*g_(1,nu,mu)+g_(1,nu,rho,mu,mu));
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1);
id g_(1,p1?)*g_(1,p2?)*g_(1,p1?)=(2*d_(p1,p2)-g_(1,p2)*g_(1,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,p2?)*g_(1,mu?)=(2*d_(mu,p2)-g_(1,p2)*g_(1,mu))*g_(1,mu);
id g_(1,mu?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,nu)-g_(1,nu)*g_(1,mu))*g_(1,mu);
id g_(1,p1?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,nu)-g_(1,nu)*g_(1,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1);
endrepeat;

***step four: adjacent-but-three rules
***TODO
repeat;
id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,p4?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,p3,p4,p1)-g_(1,p2)*g_(1,p1,p3,p4,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,p3?)*g_(1,p4?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,p3,p4,p1)-g_(1,nu)*g_(1,p1,p3,p4,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,rho?)*g_(1,p4?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,rho,p4,p1)-g_(1,nu)*g_(1,p1,rho,p4,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,rho?)*g_(1,sigma?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,rho,sigma,p1)-g_(1,nu)*g_(1,p1,rho,sigma,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,rho?)*g_(1,sigma?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,rho,sigma,p1)-g_(1,p2)*g_(1,p1,rho,sigma,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,p3?)*g_(1,sigma?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,p3,sigma,p1)-g_(1,nu)*g_(1,p1,p3,sigma,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,sigma?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,p3,sigma,p1)-g_(1,p2)*g_(1,p1,p3,sigma,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,rho?)*g_(1,p4?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,rho,p4,p1)-g_(1,p2)*g_(1,p1,rho,p4,p1));
id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,p4?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,p3,p4,mu)-g_(1,p2)*g_(1,mu,p3,p4,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,p4?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,p3,p4,mu)-g_(1,nu)*g_(1,mu,p3,p4,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,rho?)*g_(1,p4?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,rho,p4,mu)-g_(1,nu)*g_(1,mu,rho,p4,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,rho?)*g_(1,sigma?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,rho,sigma,mu)-g_(1,nu)*g_(1,mu,rho,sigma,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,rho?)*g_(1,sigma?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,rho,sigma,mu)-g_(1,p2)*g_(1,mu,rho,sigma,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,sigma?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,p3,sigma,mu)-g_(1,nu)*g_(1,mu,p3,sigma,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,sigma?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,p3,sigma,mu)-g_(1,p2)*g_(1,mu,p3,sigma,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,rho?)*g_(1,p4?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,rho,p4,mu)-g_(1,p2)*g_(1,mu,rho,p4,mu));
id g_(1,p1?)*g_(1,p2?)*g_(1,p3?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,p3,p1)-2*d_(p1,p3)*g_(1,p2,p1)+g_(1,p2,p3,p1,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,p3?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,p3,p1)-2*d_(p1,p3)*g_(1,nu,p1)+g_(1,nu,p3,p1,p1));
id g_(1,p1?)*g_(1,p2?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,p2)*g_(1,nu,p1)-2*d_(p1,nu)*g_(1,p2,p1)+g_(1,p2,nu,p1,p1));
id g_(1,p1?)*g_(1,nu?)*g_(1,rho?)*g_(1,p1?)=(2*d_(p1,nu)*g_(1,rho,p1)-2*d_(p1,rho)*g_(1,nu,p1)+g_(1,nu,rho,p1,p1));
id g_(1,mu?)*g_(1,p2?)*g_(1,p3?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,p3,mu)-2*d_(mu,p3)*g_(1,p2,mu)+g_(1,p2,p3,mu,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,p3?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,p3,mu)-2*d_(mu,p3)*g_(1,nu,mu)+g_(1,nu,p3,mu,mu));
id g_(1,mu?)*g_(1,p2?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,p2)*g_(1,nu,mu)-2*d_(mu,nu)*g_(1,p2,mu)+g_(1,p2,nu,mu,mu));
id g_(1,mu?)*g_(1,nu?)*g_(1,rho?)*g_(1,mu?)=(2*d_(mu,nu)*g_(1,rho,mu)-2*d_(mu,rho)*g_(1,nu,mu)+g_(1,nu,rho,mu,mu));
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1);
id g_(1,p1?)*g_(1,p2?)*g_(1,p1?)=(2*d_(p1,p2)-g_(1,p2)*g_(1,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,p2?)*g_(1,mu?)=(2*d_(mu,p2)-g_(1,p2)*g_(1,mu))*g_(1,mu);
id g_(1,mu?)*g_(1,nu?)*g_(1,mu?)=(2*d_(mu,nu)-g_(1,nu)*g_(1,mu))*g_(1,mu);
id g_(1,p1?)*g_(1,nu?)*g_(1,p1?)=(2*d_(p1,nu)-g_(1,nu)*g_(1,p1))*g_(1,p1);
id g_(1,mu?)*g_(1,mu?)=d_(mu,mu);
id g_(1,p1?)*g_(1,p1?)=d_(p1,p1);
endrepeat;
******fill me in babes
***TODO
***

***when you're ready, you should remove the davedavedaves and three commands below. Hopefully this will render many of the specific rules further below redundant, and you will only have to deal with strings of gamma matrices each contracted into a distinct index/momentum. If you are going to reuse some of the specific rules further below, note that the spacetime dimension should be D, not d
***bye babes!
*Bracket davedavedave;
*Print;
*.end


****these subs have to be done in a specific order!! all longer gamma matrix products must be eliminated first. Within that, subs with fewer wildcards come first

****four gammas (plus proj matrix)
*id g_(1,6_,p1,p1,gamma?,p1)=mb^3*g_(1,6_,gamma);
*id g_(1,6_,p1,p1,gamma?,p2)=2*mb^2*p2(gamma)*g_(1,6_);
*id g_(1,6_,p1,p2,gamma?,p2)=4*p1(mu)*p2(mu)*p2(gamma)*g_(1,6_);
*id g_(1,6_,p1,p2,gamma?,p1)=2*p1(mu)*p2(mu)*mb*g_(1,6_,gamma);
*id g_(1,6_,p2,p1,gamma?,p1)=0;
*id g_(1,6_,p2,p1,gamma?,p2)=0;
*id g_(1,6_,p2,p2,gamma?,p1)=0;
*id g_(1,6_,p2,p2,gamma?,p2)=0;
*id g_(1,6_,p1,gamma?,p1,p1)=2*p1(gamma)*mb^2*g_(1,6_)-mb^3*g_(1,6_,gamma);
*id g_(1,6_,p1,gamma?,p2,p2)=0;
*id g_(1,6_,p1,gamma?,p1,p2)=(4*p1(mu)*p2(mu)*p1(gamma)-2*mb^2*p2(gamma))*g_(1,6_);
*id g_(1,6_,p1,gamma?,p2,p1)=2*mb^2*p2(gamma)*g_(1,6_)-2*p1(mu)*p2(mu)*mb*g_(1,6_,gamma);
*id g_(1,6_,p2,gamma?,p1,p1)=0;
*id g_(1,6_,p2,gamma?,p1,p2)=0;
*id g_(1,6_,p2,gamma?,p2,p1)=0;
*id g_(1,6_,p2,gamma?,p2,p2)=0;
*id g_(1,7_,p1,p1,gamma?,p1)=mb^3*g_(1,7_,gamma);
*id g_(1,7_,p1,p1,gamma?,p2)=2*mb^2*p2(gamma)*g_(1,7_);
*id g_(1,7_,p1,p2,gamma?,p2)=4*p1(mu)*p2(mu)*p2(gamma)*g_(1,7_);
*id g_(1,7_,p1,p2,gamma?,p1)=2*p1(mu)*p2(mu)*mb*g_(1,7_,gamma);
*id g_(1,7_,p2,p1,gamma?,p1)=0;
*id g_(1,7_,p2,p1,gamma?,p2)=0;
*id g_(1,7_,p2,p2,gamma?,p1)=0;
*id g_(1,7_,p2,p2,gamma?,p2)=0;
*id g_(1,7_,p1,gamma?,p1,p1)=2*p1(gamma)*mb^2*g_(1,7_)-mb^3*g_(1,7_,gamma);
*id g_(1,7_,p1,gamma?,p2,p2)=0;
*id g_(1,7_,p1,gamma?,p1,p2)=(4*p1(mu)*p2(mu)*p1(gamma)-2*mb^2*p2(gamma))*g_(1,7_);
*id g_(1,7_,p1,gamma?,p2,p1)=2*mb^2*p2(gamma)*g_(1,7_)-2*p1(mu)*p2(mu)*mb*g_(1,7_,gamma);
*id g_(1,7_,p2,gamma?,p1,p1)=0;
*id g_(1,7_,p2,gamma?,p1,p2)=0;
*id g_(1,7_,p2,gamma?,p2,p1)=0;
*id g_(1,7_,p2,gamma?,p2,p2)=0;
*id g_(1,6_,p2,mu?,nu?,gamma?)=0;
*id g_(1,7_,p2,mu?,nu?,gamma?)=0;
*id g_(1,6_,mu?,nu?,gamma?,p1)=mb*g_(1,6_,mu,nu,gamma);
*id g_(1,7_,mu?,nu?,gamma?,p1)=mb*g_(1,7_,mu,nu,gamma);
****three gammas (plus projection matrix)
id g_(1,6_,gamma?,p1,p1) = mb^2 * g_(1,6_,gamma);
id g_(1,6_,gamma?,p1,p2) = 2 * p1(mu) * p2(mu) * g_(1,6_,gamma) - 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,gamma?,p2,p1) = 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,gamma?,p2,p2) = 0;
id g_(1,6_,p1,p1,gamma?) = mb^2 * g_(1,7_,gamma);
id g_(1,6_,p1,p2,gamma?) = 2 * p1(mu) * p2(mu) * g_(1,6_,gamma);
id g_(1,6_,p2,p1,gamma?) = 0;
id g_(1,6_,p2,p2,gamma?) = 0;
id g_(1,6_,p1,gamma?,p2) = -2 * p1(mu) * p2(mu) * g_(1,6_,gamma) + 2 * mb * p2(gamma) * g_(1,6_);
id g_(1,6_,p1,gamma?,p1) = - mb^2 * g_(1,6_,gamma) + 2 * mb * p1(gamma) * g_(1,6_);
id g_(1,6_,p2,gamma?,p2) = 0;
id g_(1,6_,p2,gamma?,p1) = 0;
id g_(1,7_,gamma?,p1,p1) = mb^2 * g_(1,6_,gamma);
id g_(1,7_,gamma?,p1,p2) = 2 * p1(mu) * p2(mu) * g_(1,7_,gamma) - 2 * mb * p2(gamma) * g_(1,7_);
id g_(1,7_,gamma?,p2,p1) = 2 * mb * p2(gamma) * g_(1,7_);
id g_(1,7_,gamma?,p2,p2) = 0;
id g_(1,7_,p1,p1,gamma?) = mb^2 * g_(1,7_,gamma);
id g_(1,7_,p1,p2,gamma?) = 2 * p1(mu) * p2(mu) * g_(1,7_,gamma);
id g_(1,7_,p2,p1,gamma?) = 0;
id g_(1,7_,p2,p2,gamma?) = 0;
id g_(1,7_,p1,gamma?,p2) = -2 * p1(mu) * p2(mu) * g_(1,7_,gamma) + 2 * mb * p2(gamma) * g_(1,7_);
id g_(1,7_,p1,gamma?,p1) = - mb^2 * g_(1,7_,gamma) + 2 * mb * p1(gamma) * g_(1,7_);
id g_(1,7_,p2,gamma?,p2) = 0;
id g_(1,7_,p2,gamma?,p1) = 0;
id g_(1,7_,p1,mu?,nu?)=2*p1(mu)*g_(1,7_,nu)-2*p1(nu)*g_(1,7_,mu)+mb*g_(1,7_,mu,nu);
id g_(1,7_,mu?,nu?,p2)=2*p2(nu)*g_(1,7_,mu)-2*p2(mu)*g_(1,7_,nu);
id g_(1,6_,p1,mu?,nu?)=2*p1(mu)*g_(1,6_,nu)-2*p1(nu)*g_(1,6_,mu)+mb*g_(1,6_,mu,nu);
id g_(1,6_,mu?,nu?,p2)=2*p2(nu)*g_(1,6_,mu)-2*p2(mu)*g_(1,6_,nu);
id g_(1,7_,mu?,p1,nu?)=2*p1(nu)*g_(1,7_,mu)-mb*g_(1,7_,mu,nu);
id g_(1,6_,mu?,p2,nu?)=2*p2(mu)*g_(1,6_,nu);
id g_(1,6_,mu?,p1,nu?)=2*p1(nu)*g_(1,6_,mu)-mb*g_(1,6_,mu,nu);
id g_(1,7_,mu?,p2,nu?)=2*p2(mu)*g_(1,7_,nu);
id g_(1,7_,mu?,nu?,p1)=mb*g_(1,7_,mu,nu);
id g_(1,7_,p2,mu?,nu?)=0;
id g_(1,6_,mu?,nu?,p1)=mb*g_(1,6_,mu,nu);
*id g_(1,6_,p1,mu?,nu?)=2*p1(mu)*g_(1,6_,nu)-2*p1(nu)*g_(1,6_,mu)+mb*g_(1,6_,mu,nu);
*id g_(1,6_,mu?,nu?,p2)=2*p2(nu)*g_(1,6_,mu)-2*p2(mu)*g_(1,6_,nu);
*id g_(1,7_,mu?,p1,nu?)=2*p1(nu)*g_(1,7_,mu)-mb*g_(1,7_,mu,nu);
*id g_(1,6_,mu?,p2,nu?)=2*p2(mu)*g_(1,6_,nu);
*id g_(1,6_,mu?,p1,nu?)=2*p1(nu)*g_(1,6_,mu)-mb*g_(1,6_,mu,nu);
*id g_(1,7_,mu?,p2,nu?)=2*p2(mu)*g_(1,7_,nu);
*id g_(1,7_,mu?,nu?,p1)=mb*g_(1,7_,mu,nu);
*id g_(1,7_,p2,mu?,nu?)=0;
*id g_(1,6_,mu?,nu?,p1)=mb*g_(1,6_,mu,nu);
****two gammas (plus projection matrix)
id g_(1,6_,gamma?,p2) = 2 * p2(gamma) * g_(1,6_);
id g_(1,6_,p1,gamma?) = 2 * p1(gamma) * g_(1,6_) - mb * g_(1,6_,gamma);
id g_(1,6_,gamma?,p1) = mb * g_(1,6_,gamma);
id g_(1,6_,p2,gamma?) = 0;
id g_(1,7_,gamma?,p2) = 2 * p2(gamma) * g_(1,7_);
id g_(1,7_,p1,gamma?) = 2 * p1(gamma) * g_(1,7_) - mb * g_(1,7_,gamma);
id g_(1,7_,gamma?,p1) = mb * g_(1,7_,gamma);
id g_(1,7_,p2,gamma?) = 0;
****one gamma (plus projection matrix)
id g_(1,6_,p1) = mb * g_(1,6_);
id g_(1,6_,p2) = 0;
id g_(1,7_,p1) = mb * g_(1,7_);
id g_(1,7_,p2) = 0;

sum mu,nu,gamma;
*Contract 0;
Print;
.sort
.sort

id g_(1,6_,epsstar) = 2*vectorcurrentL;
id g_(1,7_,epsstar) = 2*vectorcurrentR;
id g_(1,6_)*p1.epsstar = 2*scalarchargeRp1;
id g_(1,6_)*p2.epsstar = 2*scalarchargeRp2;
id g_(1,7_)*p1.epsstar = 2*scalarchargeLp1;
id g_(1,7_)*p2.epsstar = 2*scalarchargeLp2;

*id g_(1,7_) = 2*scalarchargeL;
id p1.p2 = (q^2-mb^2)/2;
id p1.p1=mb^2;
id p2.p2=0;
*id q^2=0;
*id mb=0;
*id yb=0;
*id RHcombo=0;


Bracket C0,C1,C2,C00,C11,C12,C22,C100,C200,C111,C222,C122,C211,C0000,C1111,C2222,C1100,C2200,C1200,C1222,C2111,C1122;
Bracket scalarchargeLp1,scalarchargeLp2,vectorcurrentL,scalarchargeRp1,scalarchargeRp2,vectorcurrentR;
Print;
.end

