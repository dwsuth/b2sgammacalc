*SM vertices
id VHdagHB(p1?, p2?, p3?, mu?)=- i_/2*COUPgp*(p1(mu)-p2(mu));
id VHdagHBB(p1?,p2?,p3?,p4?,mu?,nu?)=i_/2*COUPgp^2*d_(mu,nu);
id VBBB(p1?,p2?,p3?,mu?,nu?,sigma?) = COUPg*( d_(mu,nu)*(p1(sigma)-p2(sigma)) + d_(nu,sigma)*(p2(mu)-p3(mu)) + d_(sigma,mu)*(p3(nu)-p1(nu)) ); *from sec 16.1 peskin schroeder
id VQbaruH(p1?,p2?,p3?)= -i_ *COUPyu*g_(1,6_)/2;
id VubarQH(p1?,p2?,p3?)= -i_*COUPyudag*g_(1,7_)/2;
id VQbardH(p1?,p2?,p3?)= -i_ *COUPyd*g_(1,6_)/2;
id VdbarQH(p1?,p2?,p3?)= -i_*COUPyddag*g_(1,7_)/2;

*ALP vertices
id VBBalp(p1?,p2?,p3?,nu?,sigma?)=-4*i_*COUPcBB*e_(alpha1,nu,beta1,sigma) * p1(alpha1) * p2(beta1);
id VBBalp2(p1?,p2?,p3?,nu?,sigma?)=-4*i_*COUPcBB*e_(gamma1,nu,delta1,sigma) * p1(gamma1) * p2(delta1);
id VBBBalp(p1?,p2?,p3?,p4?,mu?,nu?,rho?)=-4*COUPcBB*COUPg*e_(gamma1,mu,nu,rho) * p4(gamma1);
id VBBBLastBIsSpecialAlp(p1?,p2?,p3?,p4?,mu?,nu?,rho?)=4*COUPcBB*COUPg*e_(gamma1,mu,nu,rho) * (p1(gamma1)+p2(gamma1));
id VQbaruHa(p1?,p2?,p3?,p4?)= -i_ *COUPyutilde*g_(1,6_)/2;
id VubarQHa(p1?,p2?,p3?,p4?)= -i_*COUPyutildedag*g_(1,7_)/2;
id VQbardHa(p1?,p2?,p3?,p4?)= -i_ *COUPydtilde*g_(1,6_)/2;
id VdbarQHa(p1?,p2?,p3?,p4?)= -i_*COUPydtildedag*g_(1,7_)/2;

*Greens vertices
id VBBO2G(p1?,p2?,alpha?,beta?) = -i_*p1(alpha1)*p1(alpha1)*(p1(beta1)*p1(beta1)*d_(alpha,beta)-p1(alpha)*p1(beta));

id VBBBO2GUNPERM(p1?,p2?,p3?,alpha?,beta?,gamma?) = COUPg*(
 -p1(alpha1)*p1(alpha1)*d_(alpha,gamma)*p1(beta)
 +p1(alpha1)*p1(alpha1)*d_(alpha,gamma)*p3(beta)
 -p1(alpha1)*p1(alpha1)*d_(beta,gamma)*p3(alpha)
+p1(alpha)*p1(beta)*p1(gamma)
-p1(alpha)*p3(beta)*p1(gamma)
 +p1(alpha1)*p3(alpha1)*d_(beta,gamma)*p1(alpha) );

id VBBBO3G(p1?,p2?,p3?,alpha?,beta?,gamma?) =-6*(
(p1(mu1)*d_(alpha,nu1)-p1(nu1)*d_(alpha,mu1))*
(p2(alpha1)*d_(beta,beta1)-p2(beta1)*d_(beta,alpha1))*
(p3(gamma1)*d_(gamma,delta1)-p3(delta1)*d_(gamma,gamma1))*d_(nu1,alpha1)*d_(beta1,gamma1)*d_(delta1,mu1)
);


*propagators
id PRgauge(p1?,mu?,nu?,feynX?)=-i_*DEN*(d_(mu,nu)-(1-xi)*p1(mu)*p1(nu)*DEN*feynX);
id PRalp(p1?)=i_*DEN;
id PRh(p1?)=i_*DEN;
id PRf(p1?,mu?) = i_* (g_(1,mu) * p1(mu))*DEN;
