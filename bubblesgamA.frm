*Dimension D;
*Vectors p1,p2,p3,p4,p5,k,r1,r2,r3,kint,epspol;
*Symbols dent,denw,deng,xi,
*COUPe,COUPg,COUPVts,COUPVtb,
*COUPcHuprime,COUPcHu,COUPcHdprime,COUPcHd,COUPcHq1,COUPcHqprime1,COUPcHq3,COUPcHqprime3,COUPcboxH,COUPcH,COUPcR,COUPcT,
*COUPv,COUPcephistar,COUPcephi,COUPcphil1,COUPcphil3,COUPcphie,COUPcphiq1,COUPcphiu,COUPcphiq3,
*COUPclq1,COUPclq3,COUPceu,COUPclu,COUPcqe,
*COUPgp,COUPsinW,COUPcosW,
*intDUMMY,mw,mw2,mz2,mb,qsqr,x,logx,ALARMk,ALARMeps,MARK1,MARK2,MARK3,DEL,a,b,c,d,feynX,feynY,feynZ,n,ksqr,eps,mt,oneoveroneminusx,d6dum,dentdenw,lightdum,
*shiftdum,GF,deltagZ,deltamz2,deltag1,deltag2,deltasW2,deltaGF,deltaalphaover2alpha,deltakappaZ,deltag1Z,deltag1gamma,deltakappagamma,COUPcHD,COUPcll,COUPcHWB,COUPcW,COUPcphid;
*Indices mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,delta,alpha1,beta1,mu1,nu1,sigma1,delta1;
*Functions VGpwmgam,VwpGmgam,VGpGmgam,
*VsbartW,VtbarbW,Vwpwmgam,VGpGmlbarl,Vtbartgam,VsbartG,VtbarbG,VGpWmlbarl,VWpGmlbarl,Vtbartlbarl,
*VGpGmZ,VGpwmZ,VwpGmZ,VtbartZ,VwpwmZ,VlbarnuW,VnubarlW,VlbarlZ,VsbartGmZ,VsbarsZ,VbbarbZ,Vsbarsgam,Vlbarlgam,
*PRh,PRq,PRu,PRd,PRs,PRb,PRwplus,PRt,PRGplus,PRnu,PRz,
*MHHHHpartialb,
*MHHHHpartialw,
*scalarint,feyn2int,feyn3int,egamp,egamm,hypergeom,intdummy,log,oneoveroneminus,twint;

#include definitions.frm

**********
****define diagrams
***********

Local FaWgam = Vsbarsgam(p2,p1,p3,nu) * PRs(p1,mu)/mb^2 * VsbartW(-p1,k,p1-k,alpha) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta) * epspol(nu) * dentdenw;
Local FaGgam = Vsbarsgam(p2,p1,p3,nu) * PRs(p1,mu)/mb^2 * VsbartG(-p1,k,p1-k) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * epspol(nu) * dentdenw;
Local Gggam = VsbartGmgam(p2,k,p1-k,p3,nu) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * epspol(nu) * dentdenw;
Local GgWgam = VsbartWgam(p2,k,p1-k,alpha,nu) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta)  * epspol(nu)  * dentdenw;
Local Ggglue = VsbartGmglue(p2,k,p1-k,p3,nu) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * epspol(nu) * dentdenw;

*********
**apply feynman rules for vertices (V) and propagators (P)
*********

#include feynRules.frm
.sort
*******
**remove dim 8 and higher pieces
*******

if ( count(d6dum,1) > 1 ) discard;
.sort
********
***shift integration momentum
*******
id k(mu?) = kint(mu) + feynY*p1(mu);

Print;
.sort
*******
***reduce tensors of loop momenta to products of k-squared(ksqr) and metric factors (d_(??,??))
*******
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)*kint(beta?)=
ksqr^3*(
d_(mu,nu)*(d_(rho,sigma)*d_(alpha,beta)+d_(rho,alpha)*d_(sigma,beta)+d_(rho,beta)*d_(alpha,sigma))
+d_(mu,rho)*(d_(nu,sigma)*d_(alpha,beta)+d_(nu,alpha)*d_(sigma,beta)+d_(nu,beta)*d_(alpha,sigma))
+d_(mu,sigma)*(d_(rho,nu)*d_(alpha,beta)+d_(rho,alpha)*d_(nu,beta)+d_(rho,beta)*d_(alpha,nu))
+d_(mu,alpha)*(d_(rho,sigma)*d_(nu,beta)+d_(rho,nu)*d_(sigma,beta)+d_(rho,beta)*d_(nu,sigma))
+d_(mu,beta)*(d_(rho,sigma)*d_(alpha,nu)+d_(rho,alpha)*d_(sigma,nu)+d_(rho,nu)*d_(alpha,sigma))
) * (1+eps/2)/4 * (1+eps/3)/6 * (1+eps/4)/8 + eps^2*ALARMeps;

id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)=0;
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)=ksqr^2*
( d_(mu,nu)*d_(rho,sigma)+d_(mu,rho)*d_(nu,sigma)+d_(mu,sigma)*d_(nu,rho) )*
(1+eps/2)/4 * (1+eps/3)/6 + eps^2*ALARMeps;
id kint(mu?)*kint(nu?)*kint(rho?)=0;
id kint(mu?)*kint(nu?)=ksqr*d_(mu,nu) * (1+eps/2)/4 + eps^2*ALARMeps;
id kint(mu?)=0;

Bracket k,ALARMk,ALARMeps,MARK1,MARK2;
Print;
.sort

*********feynman gauge
id deng = denw;
id xi=1;

**********remove SM piece
*if ( count(d6dum,1) != 1 ) discard;
*id d6dum=1;
********************

**************
***contract and sum over lorentz indices
************
sum mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,gamma4,alpha1,beta1,delta1,mu1,nu1,sigma1;
Bracket mw2;
Print;
.sort

**********
***apply momentum cons 
**********

id sqrt_(2)^2 = 2;
id p3=-p1-p2;

*Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,xi,mt,pi_;
*Print;
*.end

**********
***gamma idents 1
******
#include gammaidents.frm
#include gammaidents2.frm

.sort


*****TODO check!!!
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,alpha?,beta?,rho?) = g_(1,6_,mu,nu,rho)*(2*d_(beta,rho)*g_(2,6_,alpha) - g_(2,6_,alpha,rho,beta));
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,alpha?,rho?,beta?) = g_(1,6_,mu,nu,rho)*(2*d_(alpha,rho)*g_(2,6_,beta) - g_(2,6_,rho,alpha,beta));
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,rho?,mu?,nu?) = g_(1,6_,mu,nu,rho)*(2*d_(mu,nu)*g_(2,6_,rho) - g_(2,6_,rho,nu,mu));

#include gammaidents.frm

id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,rho?,nu?,mu?) = 4*g_(1,6_,delta1) * g_(2,6_,delta1) + ALARMeps*eps;
*id eps=0;
*id D=4;
*Bracket COUPVts,COUPVtb,COUPg,COUPv,dent,denw,deng,xi,ksqr,mt;
*Print;
*.end

**********
***do scalar integrals
**********
*id ksqr^d? = scalarint(d,2) * ( DEL^(2+d-2) );
id ksqr^d? = scalarint(d,2) * ( DEL^(2+d-2) - (feynX*feynY*mb^2)*(2+d-eps-2)*DEL^(2+d-3)/mw2/x );
*Bracket intdummy;
*Print;
.sort

id scalarint(a?,b?) = (-1)^(a-b)/16/pi_^2 * egamm(a+2) * egamp(b-a-2) / fac_(b-1) * (1+eps+eps^2*ALARMeps) * (mw2*x)^(2+a-b) * (1-eps*log(x));
.sort

id feynX^a?*feynY^b?*DEL^d?*dentdenw = twint(a,b,d); 

.sort
****************
***twint sub
****************
*id twint(0,1,0)=1/2+ALARMeps*eps;
*id twint(1,2,-1)=(x*(1-6*x+3*x^2+2*x^3-6*x^2*log(x)))/(6*(-1+x)^4)+ALARMeps*eps;

id twint(0,1,0)=1/2+((1-4*x+3*x^2+2*log(x)-4*x*log(x))*eps)/(4*(-1+x)^2)+ALARMeps*eps^2;
id twint(1,2,-1)=(x-6*x^2+3*x^3+2*x^4-6*x^3*log(x))/(6*(-1+x)^4)+((5*x-54*x^2+27*x^3+22*x^4+6*x*log(x)-36*x^2*log(x)-36*x^3*log(x)-18*x^3*log(x)^2)*eps)/(36*(-1+x)^4)+ALARMeps*eps^2;
id twint(0,0,0)=1+((-1+x-log(x))*eps)/(-1+x)+ALARMeps*eps^2;
id twint(1,1,-1)=(x*(-1+x^2-2*x*log(x)))/(2*(-1+x)^3)+(x*(-3+3*x^2-2*log(x)-4*x*log(x)-2*x*log(x)^2)*eps)/(4*(-1+x)^3)+ALARMeps*eps^2;

id twint(0,2,0)=1/3+((-2+9*x-18*x^2+11*x^3-6*log(x)+18*x*log(x)-18*x^2*log(x))*eps)/(18*(-1+x)^3)+ALARMeps*eps^2;
id twint(1,1,0)=1/6+((-5+27*x-27*x^2+5*x^3-6*log(x)+18*x*log(x))*eps)/(36*(-1+x)^3)+ALARMeps*eps^2;
id twint(0,0,1)=(1+x)/(2*x)+((-1+x^2-2*log(x))*eps)/(4*(-1+x)*x)+ALARMeps*eps^2;

.sort
****************
****expand euler gamma functions in eps, where egamp(n) = \Gamma(n+\epsilon) and egamm(n) = \Gamma(n-\epsilon)
***************
id egamp(4)=(3+eps)*egamp(3);
id egamp(3)=(2+eps)*egamp(2);
id egamp(2)=(1+eps)*egamp(1);
id egamp(1)=1;
id egamp(-2) = -(1+eps/2+eps^2*ALARMeps)/2 * egamp(-1);
id egamp(-1) = -(1+eps+eps^2*ALARMeps) * egamp(0);
id egamp(0) = 1/eps;

id egamm(4)=(3-eps)*egamm(3);
id egamm(3)=(2-eps)*egamm(2);
id egamm(2)=(1-eps)*egamm(1);
id egamm(1)=1;
id egamm(-2) = -(1-eps/2+eps^2*ALARMeps)/2 * egamm(-1);
id egamm(-1) = -(1-eps+eps^2*ALARMeps) * egamm(0);
id egamm(0) = -1/eps;

id D = 4-2*eps;
.sort

**********
***discard terms of order \epsilon^1 or (1/M^2)^2 or higher
***********
if ( count(eps,1) > 0 ) discard;
*if ( count(mw2,1) < -2 ) discard;
*if ( count(d6dum,1) != 1 ) discard;
*id d6dum=1;

id COUPv = 2*mw/COUPg;
id mw^2 = mw2;
id mz2 = mw2/COUPcosW^2;
id COUPg = COUPe/COUPsinW;
id COUPgp = COUPe/COUPcosW;

id oneoveroneminus(x?) = 1/(1-x);
.sort

************
***manual eom relations for dipole diagrams
************

#include bsgammaEOMS.frm

.sort
****************
***keep only first order in qsqr and mb^2
***************
id qsqr = lightdum^2*qsqr;
id mb = lightdum*mb;
if ( count(lightdum,1) > 2 ) discard;
id lightdum = 1;


*Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,COUPgp,COUPsinW,COUPcosW,oneoveroneminusx,xi,mt,pi_,mw2;
Print;
.sort
#write <bubblegamAout> "FaWgam=%e\nFaGgam=%e\nGggam=%e\nGgWgam=%e\nGgglue=%e",FaWgam,FaGgam,Gggam,GgWgam,Ggglue
*#write <bubblegamAout> "FaWgam=%e\nFaGgam=%e",FaWgam,FaGgam
.end
************
***final print
*************
*Bracket p1,p2,p3,M2,COUPA,delABdelCD,delADdelBC,sigACsigBD,delACdelBD;
Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,COUPgp,COUPsinW,COUPcosW,oneoveroneminusx,xi,mt,pi_,mw2;
Print;
.sort

.end

