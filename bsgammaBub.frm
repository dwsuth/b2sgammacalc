*Dimension D;
*Vectors p1,p2,p3,p4,p5,k,r1,r2,r3,kint,epspol;
*Symbols dent,denw,deng,xi,
*COUPe,COUPg,COUPVts,COUPVtb,
*COUPcHuprime,COUPcHu,COUPcHdprime,COUPcHd,COUPcHq1,COUPcHqprime1,COUPcHq3,COUPcHqprime3,COUPcboxH,COUPcH,COUPcR,COUPcT,
*COUPv,COUPcephistar,COUPcephi,COUPcphil1,COUPcphil3,COUPcphie,COUPcphiq1,COUPcphiu,COUPcphiq3,
*COUPclq1,COUPclq3,COUPceu,COUPclu,COUPcqe,
*COUPgp,COUPsinW,COUPcosW,
*intDUMMY,mw,mw2,mz2,mb,qsqr,x,logx,ALARMk,ALARMeps,MARK1,MARK2,MARK3,DEL,a,b,c,d,feynX,feynY,feynZ,n,ksqr,eps,mt,oneoveroneminusx,d6dum,denwdentdent,dentdenwdenw,lightdum,
*shiftdum,GF,deltagZ,deltamz2,deltag1,deltag2,deltasW2,deltaGF,deltaalphaover2alpha,deltakappaZ,deltag1Z,deltag1gamma,deltakappagamma,COUPcHD,COUPcll,COUPcHWB,COUPcW,COUPcphid;
*Indices mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,delta,alpha1,beta1,mu1,nu1,sigma1,delta1;
*Functions VGpwmgam,VwpGmgam,VGpGmgam,
*VsbartW,VtbarbW,Vwpwmgam,VGpGmlbarl,Vtbartgam,VsbartG,VtbarbG,VGpWmlbarl,VWpGmlbarl,Vtbartlbarl,
*VGpGmZ,VGpwmZ,VwpGmZ,VtbartZ,VwpwmZ,VlbarnuW,VnubarlW,VlbarlZ,VsbartGmZ,VsbarsZ,VbbarbZ,Vsbarsgam,Vlbarlgam,
*PRh,PRq,PRu,PRd,PRs,PRb,PRwplus,PRt,PRGplus,PRnu,PRz,
*MHHHHpartialb,
*MHHHHpartialw,
*scalarint,feyn2int,feyn3int,egamp,egamm,hypergeom,intdummy,log,oneoveroneminus,twwint,wttint;

#include definitions.frm

Functions PRwplus0,PRwplus1,PRwplus2,PRGplus0,PRGplus1,PRGplus2;

**********
****define diagrams
***********

*Local FaWgam = Vsbarsgam(p2,p1,p3,nu) * PRs(p1,mu) * VsbartW(-p1,k,p1-k,alpha) * PRt(k,rho) * VtbarbW(-k,p1,k-p1,beta) * PRwplus(k-p1,alpha,beta) * epspol(nu) * dent * denW1(-p1);
*Local FaGgam = Vsbarsgam(p2,p1,p3,nu) * PRs(p1,mu) * VsbartG(-p1,k,p1-k) * PRt(k,rho) * VtbarbG(-k,p1,k-p1) * PRGplus(k-p1) * epspol(nu) * dent * denG1(-p1);
*Local Fcgam = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * Vtbartgam(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbW(k-p1,p1,-k,sigma) * PRwplus0(-k,sigma,nu) * epspol(alpha) * denwdentdent;
*Local Fdgam = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * Vtbartgam(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbG(k-p1,p1,-k) * PRGplus0(-k) * epspol(alpha) * denwdentdent;
*Local Fegam = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,rho) * PRwplus2(k+p2,nu,alpha) * PRwplus1(k-p1,rho,beta) * Vwpwmgam(k+p2,p1-k,p3,alpha,beta,gamma) * epspol(gamma) * dentdenwdenw;
*Local Ffgam = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRwplus2(k+p2,nu,alpha) * PRGplus1(k-p1) * VwpGmgam(k+p2,p1-k,p3,alpha,gamma) * epspol(gamma) * dentdenwdenw;
*Local Fggam = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,rho) * PRGplus2(k+p2) * PRwplus1(k-p1,rho,beta) * VGpwmgam(k+p2,p1-k,p3,beta,gamma) * epspol(gamma) * dentdenwdenw;
*Local Fhgam = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus2(k+p2) * PRGplus1(k-p1) * VGpGmgam(k+p2,p1-k,p3,gamma) * epspol(gamma) * dentdenwdenw;
*Local Fcglue = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * Vtbartglue(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbW(k-p1,p1,-k,sigma) * PRwplus0(-k,sigma,nu) * epspol(alpha) * denwdentdent;
*Local Fdglue = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * Vtbartglue(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbG(k-p1,p1,-k) * PRGplus0(-k) * epspol(alpha) * denwdentdent;

Local Floopcqq = Vsbartnil(p2,k-p1-p2,nu) * PRt(k-p1-p2,mu)* Vtbartgam(-k+p1+p2,k,p3,alpha)*PRt(k,rho)*Vtbarbnil(-k,p1,nu)*epspol(alpha)*dentdent;


*Local Fc = VsbartW(p2,-k-p2,k,nu) * PRt(-k-p2,mu) * Vtbartgam(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbW(k-p1,p1,-k,sigma) * PRwplus(-k,sigma,nu) * epspol(alpha) * denw * denT1(p2) * denT2(-p1);
*Local Fd = VsbartG(p2,-k-p2,k) * PRt(-k-p2,mu) * Vtbartgam(k+p2,p1-k,p3,alpha) * PRt(p1-k,rho) * VtbarbG(k-p1,p1,-k) * PRGplus(-k) * epspol(alpha) * denw * denT1(p2) * denT2(-p1);
*Local Fe = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,rho) * PRwplus(k+p2,nu,alpha) * PRwplus(k-p1,rho,beta) * Vwpwmgam(k+p2,p1-k,p3,alpha,beta,gamma) * epspol(gamma) * dent * denW1(p2) * denW2(-p1);
*Local Ff = VsbartW(p2,k,-k-p2,nu) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRwplus(k+p2,nu,alpha) * PRGplus(k-p1) * VwpGmgam(k+p2,p1-k,p3,alpha,gamma) * epspol(gamma) * dent * denW1(p2) * denW2(-p1);
*Local Fg = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbW(-k,p1,k-p1,rho) * PRGplus(k+p2) * PRwplus(k-p1,rho,beta) * VGpwmgam(k+p2,p1-k,p3,beta,gamma) * epspol(gamma) * dent * denW1(p2) * denW2(-p1);
*Local Fh = VsbartG(p2,k,-k-p2) * PRt(k,mu) * VtbarbG(-k,p1,k-p1) * PRGplus(k+p2) * PRGplus(k-p1) * VGpGmgam(k+p2,p1-k,p3,gamma) * epspol(gamma) * dent * denW1(p2) * denW2(-p1);

*Local testint = dent * denW1(p2) * denW2(-p1);

*********
**apply feynman rules for vertices (V) and propagators (P)
*********

*the following only work in feynman gauge
id PRwplus0(p1?,mu?,nu?) = -i_ * (1+d6dum*deltamW2new*denw*(1-feynY-feynZ)) * (d_(mu,nu) - (1-xi) * p1(mu) * p1(nu) * deng *(1+xi*d6dum*deltamW2new*deng) );
id PRwplus1(p1?,mu?,nu?) = -i_ * (1+d6dum*deltamW2new*denw*feynZ) * (d_(mu,nu) - (1-xi) * p1(mu) * p1(nu) * deng *(1+xi*d6dum*deltamW2new*deng) );
id PRwplus2(p1?,mu?,nu?) = -i_ * (1+d6dum*deltamW2new*denw*feynY) * (d_(mu,nu) - (1-xi) * p1(mu) * p1(nu) * deng *(1+xi*d6dum*deltamW2new*deng) );
id PRGplus0(p1?) = i_ * (1+xi*d6dum*deltamW2new*deng*(1-feynY-feynZ));
id PRGplus1(p1?) = i_ * (1+xi*d6dum*deltamW2new*deng*feynZ);
id PRGplus2(p1?) = i_ * (1+xi*d6dum*deltamW2new*deng*feynY);

#include feynRules.frm
.sort
*******
**remove dim 8 and higher pieces
*******

if ( count(d6dum,1) > 1 ) discard;
.sort

********
***shift integration momentum
*******
id k(mu?) = kint(mu) + feynY*p3(mu);

.sort
*******
***reduce tensors of loop momenta to products of k-squared(ksqr) and metric factors (d_(??,??))
*******
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)*kint(beta?)=
ksqr^3*(
d_(mu,nu)*(d_(rho,sigma)*d_(alpha,beta)+d_(rho,alpha)*d_(sigma,beta)+d_(rho,beta)*d_(alpha,sigma))
+d_(mu,rho)*(d_(nu,sigma)*d_(alpha,beta)+d_(nu,alpha)*d_(sigma,beta)+d_(nu,beta)*d_(alpha,sigma))
+d_(mu,sigma)*(d_(rho,nu)*d_(alpha,beta)+d_(rho,alpha)*d_(nu,beta)+d_(rho,beta)*d_(alpha,nu))
+d_(mu,alpha)*(d_(rho,sigma)*d_(nu,beta)+d_(rho,nu)*d_(sigma,beta)+d_(rho,beta)*d_(nu,sigma))
+d_(mu,beta)*(d_(rho,sigma)*d_(alpha,nu)+d_(rho,alpha)*d_(sigma,nu)+d_(rho,nu)*d_(alpha,sigma))
) * (1+eps/2)/4 * (1+eps/3)/6 * (1+eps/4)/8 + eps^2*ALARMeps;

id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)*kint(alpha?)=0;
id kint(mu?)*kint(nu?)*kint(rho?)*kint(sigma?)=ksqr^2*
( d_(mu,nu)*d_(rho,sigma)+d_(mu,rho)*d_(nu,sigma)+d_(mu,sigma)*d_(nu,rho) )*
(1+eps/2)/4 * (1+eps/3)/6 + eps^2*ALARMeps;
id kint(mu?)*kint(nu?)*kint(rho?)=0;
id kint(mu?)*kint(nu?)=ksqr*d_(mu,nu) * (1+eps/2)/4 + eps^2*ALARMeps;
id kint(mu?)=0;

.sort

*********feynman gauge
id deng = denw;
id xi=1;

**********remove SM piece
*if ( count(d6dum,1) != 1 ) discard;
*id d6dum=1;
********************

**************
***contract and sum over lorentz indices
************
sum mu,nu,rho,sigma,alpha,beta,gamma,gamma1,gamma2,gamma3,gamma4,alpha1,beta1,delta1,mu1,nu1,sigma1;
.sort

**********
***apply momentum cons 
**********

id sqrt_(2)^2 = 2;
id p3=-p1-p2;

*Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,xi,mt,pi_;
*Print;
*.end

**********
***gamma idents 1
******
#include gammaidents.frm
#include gammaidents2.frm

.sort


*****TODO check!!!
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,alpha?,beta?,rho?) = g_(1,6_,mu,nu,rho)*(2*d_(beta,rho)*g_(2,6_,alpha) - g_(2,6_,alpha,rho,beta));
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,alpha?,rho?,beta?) = g_(1,6_,mu,nu,rho)*(2*d_(alpha,rho)*g_(2,6_,beta) - g_(2,6_,rho,alpha,beta));
id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,rho?,mu?,nu?) = g_(1,6_,mu,nu,rho)*(2*d_(mu,nu)*g_(2,6_,rho) - g_(2,6_,rho,nu,mu));

#include gammaidents.frm

id g_(1,6_,mu?,nu?,rho?)*g_(2,6_,rho?,nu?,mu?) = 4*g_(1,6_,delta1) * g_(2,6_,delta1) + ALARMeps*eps;
*id eps=0;
*id D=4;
*Bracket COUPVts,COUPVtb,COUPg,COUPv,dent,denw,deng,xi,ksqr,mt;
*Print;
*.end

**********
***do scalar integrals
**********
id ksqr^d? = scalarint(d,2) * ( DEL^(2+d-2) - (feynX*feynY*qsqr)*(2+d-eps-2)*DEL^(2+d-3)/mw2/x );
*Bracket intdummy;
*Print;
.sort

id scalarint(a?,b?) = (-1)^(a-b)/16/pi_^2 * egamm(a+2) * egamp(b-a-2) / fac_(b-1) * (1+eps+eps^2*ALARMeps) * (mw2*x)^(2+a-b) * (1-eps*log(x));
.sort

id feynX^a?*feynY^b?*dentdent = fac_(a) * fac_(b) / fac_(a+b+1);
id DEL=1;
id DEL^-1=1;
.sort

****************
****expand euler gamma functions in eps, where egamp(n) = \Gamma(n+\epsilon) and egamm(n) = \Gamma(n-\epsilon)
***************
id egamp(4)=(3+eps)*egamp(3);
id egamp(3)=(2+eps)*egamp(2);
id egamp(2)=(1+eps)*egamp(1);
id egamp(1)=1;
id egamp(-2) = -(1+eps/2+eps^2*ALARMeps)/2 * egamp(-1);
id egamp(-1) = -(1+eps+eps^2*ALARMeps) * egamp(0);
id egamp(0) = 1/eps;

id egamm(4)=(3-eps)*egamm(3);
id egamm(3)=(2-eps)*egamm(2);
id egamm(2)=(1-eps)*egamm(1);
id egamm(1)=1;
id egamm(-2) = -(1-eps/2+eps^2*ALARMeps)/2 * egamm(-1);
id egamm(-1) = -(1-eps+eps^2*ALARMeps) * egamm(0);
id egamm(0) = -1/eps;

id D = 4-2*eps;
.sort

**********
***discard terms of order \epsilon^1 or (1/M^2)^2 or higher
***********
if ( count(eps,1) > 0 ) discard;
*if ( count(mw2,1) < -2 ) discard;
*if ( count(d6dum,1) != 1 ) discard;
*id d6dum=1;

id COUPv = 2*mw/COUPg;
id mw^2 = mw2;
id mz2 = mw2/COUPcosW^2;
id COUPg = COUPe/COUPsinW;
id COUPgp = COUPe/COUPcosW;

id oneoveroneminus(x?) = 1/(1-x);
.sort

************
***manual eom relations for dipole diagrams
************
#include bsgammaEOMS.frm

.sort
****************
***keep only first order in qsqr and mb^2
***************
id qsqr = lightdum^2*qsqr;
id mb = lightdum*mb;
if ( count(lightdum,1) > 2 ) discard;
id lightdum = 1;


*Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,COUPgp,COUPsinW,COUPcosW,oneoveroneminusx,xi,mt,pi_,mw2;
*Print;
.sort
#write <bsgammabubout> "Floopcqq=%e\n",Floopcqq
*#write <bsgammaout> "Fcgam=%e\nFdgam=%e\nFegam=%e\nFfgam=%e\nFggam=%e\nFhgam=%e",Fcgam,Fdgam,Fegam,Ffgam,Fggam,Fhgam

Bracket g_;
Print;
.end
************
***final print
*************
*Bracket p1,p2,p3,M2,COUPA,delABdelCD,delADdelBC,sigACsigBD,delACdelBD;
Bracket g_,hypergeom,COUPVts,COUPVtb,COUPg,COUPv,COUPe,COUPgp,COUPsinW,COUPcosW,oneoveroneminusx,xi,mt,pi_,mw2;
Print;
.sort

.end

